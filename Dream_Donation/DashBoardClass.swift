//
//  DashBoardClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/6/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications


class DashBoardClass: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var btnDonate_TransportNow: TransitionButton!
    @IBOutlet weak var viewGredient: GradientView!
    @IBOutlet weak var lblHeadertitle: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var heightImageView: NSLayoutConstraint!

    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        globleLogInData = NSMutableDictionary()
        globleLogInData = (nsud.value(forKey: "Dream_Donation_LoginData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
        globleRoleUser = "\(globleLogInData.value(forKey: "Role")!)"
        globleSubRoleUser = "\(globleLogInData.value(forKey: "UserType")!)"
        let aryContainData = getDataFromCoreDataBase(strEntity: "AllContain", strkey: "contain")
        
        if(globleRoleUser == "Donor" || globleRoleUser == "Manager"){
            self.view.tag = 1
        }else if(globleRoleUser == "Transport"){
            self.view.tag = 2
        }else if(globleRoleUser == "Receiver"){
            self.view.tag = 3
        }
        
        var strHeader = "" , strTitle = "" , strSubTitle = ""
        //---------For Donor------------
        if(self.view.tag == 1){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ButtongradientLayer(btnArray: [self.btnDonate_TransportNow])

            }

            let dict = OtherVC().filterArray(ary: aryContainData, type: "DonateDashboard")
            strHeader = "Donate"
            strTitle = "\(dict.value(forKey: "ContentTitle")!)"
            strSubTitle = "\(dict.value(forKey: "HtmlContent")!)".withoutHtml
            self.btnDonate_TransportNow.setTitle("Donate Now", for: .normal)
            //Preload Data
             callgetUnitAPI()
        }
        //---------For Transport------------
  
        else if(self.view.tag == 2){
           DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                           ButtongradientLayer(btnArray: [self.btnDonate_TransportNow])

                       }

            let dict = OtherVC().filterArray(ary: aryContainData, type: "TransportDashboard")
            strHeader = "Transport"
            strTitle = "\(dict.value(forKey: "ContentTitle")!)"
            strSubTitle = "\(dict.value(forKey: "HtmlContent")!)".withoutHtml
            self.btnDonate_TransportNow.setTitle("Transport Now", for: .normal)
        }
            //---------For Reciver------------
        else if(self.view.tag == 3){
            let dict = OtherVC().filterArray(ary: aryContainData, type: "ReceiverDashboard")
            strHeader = "Receiver"
            strTitle = "\(dict.value(forKey: "ContentTitle")!)"
            strSubTitle = "\(dict.value(forKey: "HtmlContent")!)".withoutHtml
            
            let IsAvailable = "\(globleLogInData.value(forKey: "IsAvailable")!)"
            if(IsAvailable == "1"){

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                    self.btnDonate_TransportNow.setTitle("Available for Receiving", for: .normal)
                    self.btnDonate_TransportNow.setTitleColor(UIColor.white, for: .normal)
                   ButtongradientLayer(btnArray: [self.btnDonate_TransportNow])

                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                    self.btnDonate_TransportNow.setTitle("Not Available for Receiving", for: .normal)
                    self.btnDonate_TransportNow.setTitleColor(UIColor.darkGray, for: .normal)

                    self.ButtongradientLayerByTransport(btnArray: [self.btnDonate_TransportNow])

                }
            }
           
        }
        self.lblHeadertitle.text = strHeader
        self.lbltitle.text = strTitle
        self.lblDetail.text = strSubTitle
        lbltitle.textColor = hexStringToUIColor(hex: primaryColor)
        
        heightImageView.constant = self.view.frame.height / 2 - 84
        if(nsud.value(forKey: "refreshAllcontain") != nil){
            if("\(nsud.value(forKey: "refreshAllcontain")!)" != "\(Date().string(format: "dd"))"){
                self.callGetAllContainAPI()
            }
        }else{
            self.callGetAllContainAPI()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(globleRoleUser == "Donor" && globleSubRoleUser == "Corporate" ){
                   callgetManagerListAPI()
               }
    }
    @IBAction func actionOnDonateTransport(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
        }, completion: { finish in
            sender.transform = CGAffineTransform.identity
            if(self.view.tag == 1){ // For Donate
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "DonateNowClass")as! DonateNowClass
                self.navigationController?.pushViewController(testController, animated: true)
            }else if(self.view.tag == 1){ // For Transport
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "TransportNowClass")as!TransportNowClass
                self.navigationController?.pushViewController(testController, animated: true)
            }else if(self.view.tag == 3){ // For Reciving
                let IsAvailable = "\(globleLogInData.value(forKey: "IsAvailable")!)"
                IsAvailable == "1" ? self.callUpdateAvailableStatusAPI(strStatus: "False") : self.callUpdateAvailableStatusAPI(strStatus: "True")
            }
        })
    }
    
    @IBAction func actionOnMenu(_ sender: UIButton) {
        let vc: MenuVC = self.storyboard!.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDrawerView = self
        self.present(vc, animated: false, completion: {})
    }
    
    @IBAction func actionOnNotification(_ sender: UIButton) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func ButtongradientLayerByTransport(btnArray:[TransitionButton]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
             for btn in btnArray {
             //     btn.titleLabel?.font = ButtonMainFont
                   for item in btn.layer.sublayers! {
                                if item is CAGradientLayer {
                                    item.removeFromSuperlayer()
                                }
                            }
                btn.layer.insertSublayer(self.getgradientLayerByTransport(bounds: btn.bounds), at: 0)
                 }
        }
    }
    func getgradientLayerByTransport(bounds : CGRect) -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [hexStringToUIColor(hex: "F2F1F2").cgColor, hexStringToUIColor(hex: "D6D6D6").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.locations = [0.0, 1.0]
        return gradientLayer
    }
}


   
// MARK:
// MARK:- DrawerScreenDelegate
extension DashBoardClass: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if(tag == 99){
                let alert = UIAlertController(title: alertMessage, message: alertLogout, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                    print("Yay! You brought your towel!")
                }))
                alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
                    
                    deleteAllRecords(strEntity:"AllContain")
                    deleteAllRecords(strEntity:"Location")
                    deleteAllRecords(strEntity:"LocationAdmin")
                    deleteAllRecords(strEntity:"Manager")
                    deleteAllRecords(strEntity:"Unit")
                    
                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    nsud.setValue(true, forKey: "DreamDonation_Permission")

                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "TutorialVC")as!TutorialVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }))
                self.present(alert, animated: true)
            }
                
            else if (strType == "Setting"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtherVC") as? OtherVC
                self.navigationController?.pushViewController(vc!, animated: true)
                
                
            }  else if (strType == "Add Locations"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LocationListClass") as? LocationListClass
                vc!.strComeFrom = "Navigation"
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Add Manager"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ManagerListClass") as? ManagerListClass
                vc!.strComeFrom = "Navigation"
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Assigned Locations"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ManagerAssignLocationClass") as? ManagerAssignLocationClass
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "History"){
                
                if(globleRoleUser == "Receiver"){
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryReciverVC") as? HistoryReciverVC
                                  self.navigationController?.pushViewController(vc!, animated: true)
                }else{
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryClass") as? HistoryClass
                                  self.navigationController?.pushViewController(vc!, animated: true)
                }
              
            }
            else if (strType == "Feedback"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "CreatFeedBackVC") as? CreatFeedBackVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
                
            else if (strType == "My Profile"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetUPProfileClass") as? SetUPProfileClass
                vc?.strComeFrom = "Profile"
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Notifications"){
                           let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
                           self.navigationController?.pushViewController(vc!, animated: true)
                       }
            
            else if (strType == "Verification"){
                                   let vc = mainStoryboard.instantiateViewController(withIdentifier: "VerificationVC") as? VerificationVC
                                   self.navigationController?.pushViewController(vc!, animated: true)
                               }
        }
    }
}


// MARK: -
// MARK: --API Calling

extension DashBoardClass {
    
    func callgetUnitAPI() {
        //  let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodUnit xmlns='\(BaseURL)'></GetFoodUnit></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetFoodUnitResult", responcetype: "GetFoodUnitResponse") { (responce, status) in
            // loader.dismiss(animated: false) {
            if status == "Suceess"{
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                // let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    deleteAllRecords(strEntity:"Unit")
                    saveDataInLocalArray(strEntity: "Unit", strKey: "unit", data: (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray)
                    // self.aryUnit = NSMutableArray()
                    // self.aryUnit = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                    //  self.goOnSelectionView(tag: 4, ary:  self.aryUnit)
                    
                }else{
                    //  CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                    
                }
            }else{
                //  CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                
            }
        }
        
        
        
    }
    func callgetManagerListAPI() {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("0", forKey: "DonorVId")
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "ManagerOwnerDonorVId")
        dictJson.setValue("", forKey: "Name")
        dictJson.setValue("", forKey: "Role")
        
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictJson) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetUserRegistration xmlns='\(BaseURL)'><RegistrationSearchMdl>\(jsonString)</RegistrationSearchMdl></GetUserRegistration></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetUserRegistrationResult", responcetype: "GetUserRegistrationResponse") { (responce, status) in
            
            if status == "Suceess"{
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                // let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    deleteAllRecords(strEntity:"Manager")
                    saveDataInLocalArray(strEntity: "Manager", strKey: "manager", data: (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray)
                }
            }
        }
    }
    
    func callUpdateAvailableStatusAPI(strStatus : String) {


            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UpdateAvailableStatus xmlns='\(BaseURL)'><DonorVId>\(globleLogInData.value(forKey: "DonorVId")!)</DonorVId><IsAvailable>\(strStatus)</IsAvailable></UpdateAvailableStatus></soap12:Body></soap12:Envelope>"
            self.btnDonate_TransportNow.startAnimation()
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "UpdateAvailableStatusResult", responcetype: "UpdateAvailableStatusResponse") { (responce, status) in
                
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        

                        self.callGetProfileDataAPI()
                        
                    }else{
                        self.btnDonate_TransportNow.stopAnimation()

                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                    }
                }else{
                    self.btnDonate_TransportNow.stopAnimation()

                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    
                }
            }
        }

    func callGetProfileDataAPI() {


             let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetProfileData xmlns='\(BaseURL)'><DonorVId>\(globleLogInData.value(forKey: "DonorVId")!)</DonorVId></GetProfileData></soap12:Body></soap12:Envelope>"
             WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetProfileDataResult", responcetype: "GetProfileDataResponse") { (responce, status) in
                 self.btnDonate_TransportNow.stopAnimation()

                 if status == "Suceess"{
                     let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                     //let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                     
                     if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                         let dictLoginData = ((dictTemp.value(forKey: "DTList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        nsud.set(dictLoginData.removeNullFromDict(), forKey: "Dream_Donation_LoginData")
                        nsud.synchronize()
                        globleLogInData = NSMutableDictionary()
                        globleLogInData = dictLoginData
                        self.setUpUI()
                         
                     }else{
                         //CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                     }
                 }else{
                     
                    // CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                     
                 }
             }
         }

    
    func callGetAllContainAPI() {
        
      
               let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetContent xmlns='\(BaseURL)'><country_id>2</country_id><ContentType></ContentType></GetContent></soap12:Body></soap12:Envelope>"
               WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetContentResult", responcetype: "GetContentResponse") { (responce, status) in
                   if status == "Suceess"{
                       let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                       if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        nsud.set("\(Date().string(format: "dd"))", forKey: "refreshAllcontain")
                        self.setUpUI()

                           deleteAllRecords(strEntity:"AllContain")
                           saveDataInLocalArray(strEntity: "AllContain", strKey: "contain", data: (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray)
                          
                       }else{
                       }
                   }else{
                       
                   }
               }
           }
}




