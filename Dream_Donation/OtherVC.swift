//
//  OtherVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/24/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class OtherVC: UIViewController {
    // MARK: -
    // MARK: - IBOutlet
    @IBOutlet weak var tvList: UITableView!
    // MARK: -
    // MARK: - Var
    var aryProfile = NSMutableArray()
    var aryPolicy = NSMutableArray()
    var aryOther = NSMutableArray()
    var aryPayment = NSMutableArray()
    
    
    // MARK: -
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        aryProfile = [["title":"Edit Profile"],["title":"Change Password"]]
        aryOther = [["title":"Terms & Conditions"],["title":"Disclaimer"],["title":"About App"],["title":"Contact Us"],["title":"Share App"],["title":"Version"]]
        
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardClass.self) {
                _ =  self.navigationController!.popToViewController(controller, animated:true)
                break
            }
        }
    }
    func filterArray(ary: NSMutableArray , type : String) -> NSMutableDictionary {
        for item in ary {
            if((item as AnyObject).value(forKey: "ContentType") as! String == type){
                return ((item as AnyObject)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            }
        }
        return NSMutableDictionary()
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension OtherVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return aryProfile.count
        }else if(section == 1){
            return aryOther.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath as IndexPath) as! OtherCell
        if(indexPath.section == 0){
            let strTitle = (aryProfile[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.lblTitle.text = strTitle
        }else if(indexPath.section == 1){
            let strTitle = (aryOther[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.lblTitle.text = strTitle
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tvList.cellForRow(at: indexPath)as! OtherCell
        let title = "\(cell.lblTitle.text!)"
        
        if(title == "Edit Profile"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetUPProfileClass") as? SetUPProfileClass
                                vc?.strComeFrom = "Profile"
                                self.navigationController?.pushViewController(vc!, animated: true)
        }
            
        else if(title == "Change Password"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordClass")as! ChangePasswordClass
            self.navigationController?.pushViewController(testController, animated: true)
        }
            
        else if(title == "About App"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ContainClass")as! ContainClass
            let dict = filterArray(ary: getDataFromCoreDataBase(strEntity: "AllContain", strkey: "contain"), type: "AboutApp")
            testController.dictContainData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }
            
        else if(title == "Contact Us"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ContainClass")as! ContainClass
            let dict = filterArray(ary: getDataFromCoreDataBase(strEntity: "AllContain", strkey: "contain"), type: "ContactUs")
            testController.dictContainData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }
            
            else if(title == "Disclaimer"){
                       let testController = mainStoryboard.instantiateViewController(withIdentifier: "ContainClass")as! ContainClass
                       let dict = filterArray(ary: getDataFromCoreDataBase(strEntity: "AllContain", strkey: "contain"), type: "Disclaimer")
                       testController.dictContainData = dict
                       self.navigationController?.pushViewController(testController, animated: true)
                   }
                       
            
        else if(title == "Share App"){
            let text = app_Name
            let myWebsite = NSURL(string:application_ID)
            let shareAll = [text , myWebsite ?? 0] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
            
        else if(title == "Terms & Conditions"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ContainClass")as! ContainClass
            let dict = filterArray(ary: getDataFromCoreDataBase(strEntity: "AllContain", strkey: "contain"), type: "TermsConditions")
            testController.dictContainData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }
            
        else if(title == "Version"){
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:"App Version : \(app_Version)\nDate : \(app_VersionDate)\n\(app_VersionSupport)" , viewcontrol: self)
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 45
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "Profile"
        }else if(section == 1){
            return "Other"
        }
        return ""
    }
}

// MARK: - ----------------VendorDiscount Cell
// MARK: -
class OtherCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
