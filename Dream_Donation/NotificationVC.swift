//
//  NotificationVC.swift
//  AaharFoeUS
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    // MARK:
       // MARK:- IBOutlet
    
    // MARK:
       // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callgetNotificationListAPI()
        // Do any additional setup after loading the view.
    }
    
    // MARK:
          // MARK:- IBAction
   @IBAction func actionOnBack(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
          
      }

}


// MARK:
// MARK:- Call API
extension NotificationVC {
    
    func callgetNotificationListAPI() {

           let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetNotification xmlns='\(BaseURL)'><DonorVId>\(globleLogInData.value(forKey: "DonorVId")!)</DonorVId></GetNotification></soap12:Body></soap12:Envelope>"
           WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetNotificationResult", responcetype: "GetNotificationResponse") { (responce, status) in
//               self.loadingPlaceholderView.uncover(animated: false)
//               self.refreshControl.endRefreshing()
               if status == "Suceess"{
                   let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                   let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                  if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                   
//                    self.aryList = NSMutableArray()
//                    self.aryList =  NSMutableArray(array: (dictTemp.value(forKey: "DTList")as! NSArray).reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
//                    self.arySelectedList = NSMutableArray()
//                    self.arySelectedList = NSMutableArray(array: (dictTemp.value(forKey: "DTList")as! NSArray).reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
//
//                    self.strErrorMessage = ""
                   }else{
                   // self.strErrorMessage = apiError
                  }
              }else{
                //   self.strErrorMessage = alertSomeError
              }
              // self.numberOfRows =  self.aryList.count
              // self.numberOfSections = 1
             //  self.tableView.reloadData()
          }
            }
}




