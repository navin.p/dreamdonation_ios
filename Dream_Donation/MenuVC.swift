//
//  MenuVC.swift
//  AaharFoeUS
//
//  Created by Navin Patidar on 2/11/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var btnTransprant: UIButton!
    
    weak var handleDrawerView: DrawerScreenDelegate?
    var aryMenu = NSMutableArray()
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTransprant.isHidden = true
      
         if("\(globleLogInData.value(forKey: "Role")!)" == "Donor") && ("\(globleLogInData.value(forKey: "UserType")!)" != "Individual"){
               aryMenu = [["title":"My Profile","image":"DeveloperN"],["title":"Add Locations","image":"languageN"],["title":"Add Manager","image":"DisclaimerN"],["title":"History","image":"video"],["title":"Feedback","image":"shake_to_sendN"],["title":"Setting","image":"ShareN"]]
        }
         else  if("\(globleLogInData.value(forKey: "Role")!)" == "Manager"){
                    
              aryMenu = [["title":"My Profile","image":"DeveloperN"],["title":"Assigned Locations","image":"languageN"],["title":"History","image":"video"],["title":"Feedback","image":"shake_to_sendN"],["title":"Setting","image":"ShareN"]]
            
                }
                 
         
         else  if("\(globleLogInData.value(forKey: "Role")!)" == "Receiver"){
                             aryMenu = [["title":"My Profile","image":"DeveloperN"],["title":"Notifications","image":"languageN"],["title":"Verification","image":"video"],["title":"History","image":"shake_to_sendN"],["title":"Setting","image":"ShareN"]]
                                      
                        }
                         
         
         else{
                 aryMenu = [["title":"My Profile","image":"DeveloperN"],["title":"History","image":"video"],["title":"Feedback","image":"shake_to_sendN"],["title":"Setting","image":"ShareN"]]
        }
      
   
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tvList.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        viewLogout.frame = CGRect(x: -self.view.frame.width, y: tvList.frame.maxY, width: self.view.frame.width, height: 44)
        UIView.animate(withDuration: 0.5) {
            self.tvList.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.viewLogout.frame = CGRect(x: 0, y: self.tvList.frame.maxY, width: self.view.frame.width, height: 40)
            self.btnTransprant.isHidden = false
        }
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
        handleDrawerView?.refreshDrawerScreen(strType: "", tag: 0)
        UIView.animate(withDuration: 0.5) {
            self.tvList.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    @IBAction func actionOnLogout(_ sender: UIButton) {
            self.dismiss(animated: false) {
                self.handleDrawerView?.refreshDrawerScreen(strType: "Log Out", tag: 99)
            }
    }
}
// MARK:
// MARK:- UITableViewDataSource
extension MenuVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  section == 0 ? 1 : aryMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath as IndexPath) as! ProfileCell
            cell.simpleLabel.text = "\(globleLogInData.value(forKey: "Name")!) " + "\(globleLogInData.value(forKey: "MiddleName")!)"
            cell.simpleRole.text = "\(globleLogInData.value(forKey: "Role")!) \(globleLogInData.value(forKey: "UserType")!)"
            let imgName = "\(globleLogInData.value(forKey: "ProfileImage")!)"
            cell.avatarImageView.contentMode = .center
            cell.avatarImageView.setImageWith(URL(string: ProfileImageDownload + imgName), placeholderImage: UIImage(named: "add_photo"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
                if(image != nil){
                    cell.avatarImageView.contentMode = .scaleAspectFill
                }
            }, usingActivityIndicatorStyle: .gray)
            return cell
        }else{
            let cell = tableView
                .dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! MenuCell
            let dict = aryMenu.object(at: indexPath.row)as! NSDictionary
            cell.lblMenu.text = "\(dict.value(forKey: "title")!)"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 230 : 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryMenu.object(at: indexPath.row)as! NSDictionary
        self.dismiss(animated: false) {
            self.handleDrawerView?.refreshDrawerScreen(strType: "\(dict.value(forKey: "title")!)", tag: indexPath.row)
        }
    }
    
}
// MARK:
// MARK:- ProfileCell
class ProfileCell: UITableViewCell {
    @IBOutlet weak var simpleLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var simpleRole: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
        avatarImageView.layer.borderColor = UIColor.white.cgColor
        avatarImageView.layer.borderWidth = 1.0
    }
    
}
// MARK:
// MARK:- MenuCell
class MenuCell: UITableViewCell {
    
    
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!
    
    
}
//MARK:
//MARK: ---------------Protocol-----------------
protocol DrawerScreenDelegate : class{
    func refreshDrawerScreen(strType : String ,tag : Int)
}
