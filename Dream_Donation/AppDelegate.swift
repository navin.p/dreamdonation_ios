//
//  AppDelegate.swift
//  DreamDonation
//
//  Created by Navin Patidar on 3/4/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CoreData

import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    let center = UNUserNotificationCenter.current()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if DeviceType.IS_IPAD {
                         mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                     }else{
                         mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                     }
      
        // Override point for customization after application launch.
        if #available(iOS 13.0, *) {
                        window?.overrideUserInterfaceStyle = .light

              }else{
              }

        gotoViewController()

        
        return true
    }
    func gotoViewController()  {
        
        if (nsud.value(forKey: "Dream_Donation_KeepLogin") != nil) {
           
            let obj_ISVC : LoginClass = mainStoryboard.instantiateViewController(withIdentifier: "LoginClass") as! LoginClass
                      
                      let navigationController = UINavigationController(rootViewController: obj_ISVC)
                      rootViewController(nav: navigationController)
            
        }else{
            let obj_ISVC : TutorialVC = mainStoryboard.instantiateViewController(withIdentifier: "TutorialVC") as! TutorialVC
            
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            rootViewController(nav: navigationController)
        }
        
    }
    func rootViewController(nav : UINavigationController)  {
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: true)
    }
     // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
        func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
            // Called when a new scene session is being created.
            // Use this method to select a configuration to create the new scene with.
            return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
        }
    @available(iOS 13.0, *)
        func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
            // Called when the user discards a scene session.
            // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
            // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
        }
       // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
     
        let container = NSPersistentContainer(name: "Dream_Donation")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
              
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
              
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    class func getContext () -> NSManagedObjectContext {
           let appDelegate = UIApplication.shared.delegate as! AppDelegate
           return appDelegate.persistentContainer.viewContext
       }
       
       
}

