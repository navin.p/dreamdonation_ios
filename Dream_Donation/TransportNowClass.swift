//
//  TransportNowClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/17/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import UserNotifications
import TransitionButton
class TransportNowClass: UIViewController {
    
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var viewHold: UIView!
    @IBOutlet weak var txtSearch: UISearchBar!
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strErrorMessage  = ""
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.coverableCellsIdentifiers = cellsIdentifiers
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    private var numberOfSections = 0
    private var numberOfRows = 0
    private var loadingPlaceholderView = LoadingPlaceholderView(){
        didSet{
            do {
                loadingPlaceholderView.gradientColor = .white
                loadingPlaceholderView.backgroundColor = .white
            }
        }
    }
    var refreshControl = UIRefreshControl()
    
    private var cellsIdentifiers = [
        "TransportCell",
        "TransportCell","TransportCell",
        "TransportCell","TransportCell",
        "TransportCell"
    ]
    
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        txtSearch.layer.cornerRadius = 10
        txtSearch.tintColor = hexStringToUIColor(hex: primaryColor)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.txtSearch.delegate = self
            self.refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            if(isInternetAvailable()){
               // self.loadingPlaceholderView.cover(self.viewHold, animated: true)
              //  self.getTransportList(id: "3", tag: 0)
            }else{
                self.aryList = NSMutableArray()
                self.strErrorMessage = alertInternet
                self.tableView.reloadData()
            }
            self.tableView.addSubview(self.refreshControl)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @objc func refresh(sender:AnyObject) {
        txtSearch.text = ""
    }
    
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnFilter(_ sender: UIButton) {
          
      }
}
// MARK: -
// MARK: - UISearchBarDelegate


extension TransportNowClass: UISearchBarDelegate  {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        if(self.aryList.count != 0){
            self.searchAutocomplete(Searching: txtAfterUpdate)
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
        self.view.endEditing(true)
    }
    
    func searchAutocomplete(Searching: NSString) -> Void {
        if(self.aryList.count != 0){
            
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        self.strErrorMessage = ""
        let resultPredicate = NSPredicate(format: "Name contains[c] %@ OR Name contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = nsMutableArray.mutableCopy() as! NSMutableArray
        }
        else{
            self.arySelectedList = NSMutableArray()
            self.arySelectedList = self.aryList.mutableCopy() as! NSMutableArray
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(arySelectedList.count == 0){
            self.strErrorMessage = alertDataNotFound
            
        }
        DispatchQueue.main.async {
            self.numberOfSections = 1
            self.numberOfRows = self.arySelectedList.count
            self.tableView.reloadData()
        }
       }
    }
}

// MARK:
   // MARK:- Call API
extension TransportNowClass {
    
      func getTransportList(id : String , tag : Int) {
        
    }
}


// MARK: -
// MARK: -UITableViewDelegate

extension TransportNowClass : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "TransportCell", for: indexPath as IndexPath) as! TransportCell
        if(aryList.count == 0){
            if(indexPath.row % 2 == 0){
                ButtongradientLayerByTransport(btnArray: [cell.btnTransportNow])
                cell.btnTransportNow.setTitleColor(UIColor.darkGray, for: .normal)
            }else{
                ButtongradientLayer(btnArray: [cell.btnTransportNow])
                cell.btnTransportNow.setTitleColor(UIColor.white, for: .normal)
            }
            cell.ConfigureDataOnCell(dict: NSDictionary())
        

        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        
        print("Button tapped")
        
    }
    
    @objc func buttonCalling(_ sender: UIButton!) {
        
        if !(callingFunction(number: (sender.titleLabel?.text!)! as NSString)){
            CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
        }
    }
    func ButtongradientLayerByTransport(btnArray:[TransitionButton]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
             for btn in btnArray {
             //     btn.titleLabel?.font = ButtonMainFont
                   for item in btn.layer.sublayers! {
                                if item is CAGradientLayer {
                                    item.removeFromSuperlayer()
                                }
                            }
                btn.layer.insertSublayer(self.getgradientLayerByTransport(bounds: btn.bounds), at: 0)
                 }
              
        }
      
       
    }
    func getgradientLayerByTransport(bounds : CGRect) -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [hexStringToUIColor(hex: "F2F1F2").cgColor, hexStringToUIColor(hex: "D6D6D6").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.locations = [0.0, 1.0]
        return gradientLayer
    }
}
// MARK: -
// MARK: -TransportCell
class TransportCell: UITableViewCell {
    @IBOutlet weak var btnTransportNow: TransitionButton!
    @IBOutlet weak var lblDonorName: UILabel!
    @IBOutlet weak var lblDonorAddress: UILabel!
    @IBOutlet weak var lblDonorContactPerson: UILabel!
    @IBOutlet weak var lblPickUpFromTime: UILabel!
    @IBOutlet weak var lblPickUpTOTime: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    override func awakeFromNib() {
           super.awakeFromNib()
           lblPickUpFromTime.layer.cornerRadius = lblPickUpFromTime.frame.height/2
        lblPickUpFromTime.layer.borderWidth = 1.0
        lblPickUpFromTime.layer.borderColor = UIColor.lightGray.cgColor
        
        lblPickUpTOTime.layer.cornerRadius = lblPickUpTOTime.frame.height/2
        lblPickUpTOTime.layer.borderWidth = 1.0
        lblPickUpTOTime.layer.borderColor = UIColor.lightGray.cgColor
       }
    
    
    func ConfigureDataOnCell(dict : NSDictionary)  {
        lblDonorName.text = "Navin Patidar"
        lblDonorAddress.text = "11802 Warfield St, San Antonio, TX 78216, United States"
        lblDonorContactPerson.text = "Saavan Patidar - +919685730669"
        lblPickUpFromTime.text = "12/123/2020 12:00 AM"
        lblPickUpTOTime.text = "12/123/2020 12:00 AM"
    }
}
