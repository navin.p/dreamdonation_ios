//
//  VerificationVC.swift
//  Dream_Donation
//
//  Created by NavinPatidar on 6/3/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class VerificationVC: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var lblMsg1: UILabel!
    @IBOutlet weak var lblMsg2: UILabel!
    @IBOutlet weak var viewReUpload: UIView!
    @IBOutlet weak var lblUploadFileName: UILabel!
    @IBOutlet weak var btnUpload: TransitionButton!
    @IBOutlet weak var txtWebsite: ACFloatingTextfield!
    @IBOutlet weak var txthoursOFOpration: ACFloatingTextfield!
    @IBOutlet weak var txtOrganizationName: ACFloatingTextfield!
    @IBOutlet weak var lblDocument: UILabel!
    @IBOutlet weak var btnBrowse: UIButton!

    var documentImage = UIImage()
    var imagePicker = UIImagePickerController()
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callVerificationAPI()
        self.setUpUI()
    }
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        lblMsg1.textColor = hexStringToUIColor(hex: primaryColor)
        ButtongradientLayer(btnArray: [btnUpload])
        viewReUpload.isHidden = true
        lblMsg1.text = ""
        lblMsg2.text = ""
    }
    
    func setUpUIAccordingToCondition(dictVerificationData : NSMutableDictionary) {
        print(dictVerificationData)
        if(dictVerificationData.value(forKey: "DTList") is NSArray){
            let aryTemp = dictVerificationData.value(forKey: "DTList") as! NSArray
            if(aryTemp.count != 0){
             //  let strStatus = "\((aryTemp.object(at: 0)as! NSDictionary).value(forKey: "VerificationStatus")!)"
            let strStatus = "Reject"
                if(strStatus == "Approve"){
                    viewReUpload.isHidden = false
                    btnUpload.isHidden = true
                    btnBrowse.isHidden = true

                    txtOrganizationName.text = "\(globleLogInData.value(forKey: "InstitutionName")!)"
                    txtWebsite.text = "\(globleLogInData.value(forKey: "Website")!)"
                    txthoursOFOpration.text = "\(globleLogInData.value(forKey: "HoursOfOperation")!)"
                    lblUploadFileName.text = "\(globleLogInData.value(forKey: "AttachDocument")!)"
                    lblDocument.text = "Uploaded Document"
                }else if(strStatus == "Reject"){
                    viewReUpload.isHidden = false
                    btnUpload.isHidden = false
                    btnBrowse.isHidden = false

                    txtOrganizationName.isUserInteractionEnabled = true
                    txtWebsite.isUserInteractionEnabled = true
                    txthoursOFOpration.isUserInteractionEnabled = true
                    lblDocument.text = "Upload Document"

                }else if(strStatus == "Pending"){
                    viewReUpload.isHidden = false
                    btnUpload.isHidden = true
                    btnBrowse.isHidden = true

                    txtOrganizationName.text = "\(globleLogInData.value(forKey: "InstitutionName")!)"
                    txtWebsite.text = "\(globleLogInData.value(forKey: "Website")!)"
                    txthoursOFOpration.text = "\(globleLogInData.value(forKey: "HoursOfOperation")!)"
                    lblUploadFileName.text = "\(globleLogInData.value(forKey: "AttachDocument")!)"
                    lblDocument.text = "Uploaded Document"
                    txtOrganizationName.isUserInteractionEnabled = false
                    txtWebsite.isUserInteractionEnabled = false
                    txthoursOFOpration.isUserInteractionEnabled = false
                }
                
                lblMsg1.text = "Welcome Back!"
                lblMsg2.text = "\(dictVerificationData.value(forKey: "Success")!)"
            }
        }
    }
    
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnUpload(_ sender: Any) {
        self.view.endEditing(true)
        if(validationForVerification()){
            self.APICalling()
        }
    }
    
    @IBAction func actionOnBrowse(_ sender: Any) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController.init(title: alertPhotoSource, message: alertChooseSource, preferredStyle: .actionSheet)
        let actioncamera = UIAlertAction(title: alertCamera, style: .default) { (action: UIAlertAction!) in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
            }
        }
        actioncamera.setValue(UIImage(named: "camera-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        alertController.addAction(actioncamera)
        
        let actionGallery = UIAlertAction(title: alertPhotoLibrary, style: .default) { (action: UIAlertAction!) in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
            }
        }
        actionGallery.setValue(UIImage(named: "gallery-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        alertController.addAction(actionGallery)
        let actionDismiss = UIAlertAction(title: "Dismiss", style: .destructive) { (action: UIAlertAction!) in
            
        }
        alertController.addAction(actionDismiss)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}
// MARK: -
// MARK: --API Calling
extension VerificationVC {
    func callVerificationAPI() {
        let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetVerificationStatus xmlns='\(BaseURL)'><DonorVId>\(globleLogInData.value(forKey: "DonorVId")!)</DonorVId></GetVerificationStatus></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetVerificationStatusResult", responcetype: "GetVerificationStatusResponse") { (responce, status) in
            loader.dismiss(animated: false) {
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.setUpUIAccordingToCondition(dictVerificationData: dictTemp)
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                    }
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                }
            }
        }
    }
    
    func APICalling()  {
        if !(isInternetAvailable()){
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }else{
            
            self.uploadDocumentImage(strImageNAme: self.lblUploadFileName.text!, image: documentImage)
            
        }
    }
    func callUpdateAPI() {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
        dictJson.setValue("\(txtWebsite.text!)", forKey: "Website")
        dictJson.setValue("\(txtOrganizationName.text!)", forKey: "InstitutionName")
        dictJson.setValue("\(txthoursOFOpration.text!)", forKey: "HoursOfOperation")
        dictJson.setValue("\(self.lblUploadFileName.text!)", forKey: "AttachDocument")
       
        
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictJson) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UploadAttachment xmlns='\(BaseURL)'><DreamDonationUploadAttachmentMdl>\(jsonString)</DreamDonationUploadAttachmentMdl></UploadAttachment></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "UploadAttachmentResult", responcetype: "UploadAttachmentResponse") { (responce, status) in
            self.btnUpload.stopAnimation()
            
            if status == "Suceess"{
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    
//                    let dictLoginData = ((dictTemp.value(forKey: "DTList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
//                    nsud.set(dictLoginData.removeNullFromDict(), forKey: "Dream_Donation_LoginData")
//                    nsud.synchronize()
//                    globleLogInData = NSMutableDictionary()
//                    globleLogInData = dictLoginData
                    CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 3)
                    self.navigationController?.popViewController(animated: true)
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                }
            }else{
                
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                
            }
        }
    }
    func uploadDocumentImage(strImageNAme : String , image : UIImage)  {
        btnUpload.startAnimation()
        WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProfile, image: image, fileName: strImageNAme, withName: "userfile") { (responce, status) in
            if(status == "Suceess"){
                self.callUpdateAPI()
            }else{
                self.btnUpload.stopAnimation()
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
            }
        }
    }
    
}

// MARK:
// MARK:- Validation

extension VerificationVC {
    func validationForVerification() -> Bool {
        if(txtOrganizationName.text?.count == 0){
            addErrorMessage(textView: txtOrganizationName, message: alert_required)
            
            return false
        }else if(txtWebsite.text!.count == 0){
            addErrorMessage(textView: txtWebsite, message: alert_required)
            return false
        }else if(txthoursOFOpration.text!.count == 0){
            addErrorMessage(textView: txthoursOFOpration, message: alert_required)
            return false
        }
        else if (Int(txthoursOFOpration.text!)! > 24){
            addErrorMessage(textView: txthoursOFOpration, message: alert_operation)
            return false
        }
        else if (self.lblUploadFileName.text!.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Document is required!", viewcontrol: self)
            return false
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension VerificationVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txthoursOFOpration){
            return ACFloatingTextValidation(textField: txthoursOFOpration, string: string, returnOnly: "NUMBER", limitValue: 1)
        }
        return true
    }
}

// MARK: -
// MARK: -UINavigationControllerDelegate
extension VerificationVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.lblUploadFileName.text = "Document\(getUniqueString())"
        self.documentImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
    }
}
