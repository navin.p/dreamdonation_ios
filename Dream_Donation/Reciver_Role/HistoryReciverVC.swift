//
//  HistoryReciverVC.swift
//  Dream_Donation
//
//  Created by NavinPatidar on 6/9/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import UserNotifications
import TransitionButton

class HistoryReciverVC: UIViewController {
    // MARK:
       // MARK:- IBOutlet
       @IBOutlet weak var viewHold: UIView!
       @IBOutlet weak var txtSearch: UISearchBar!
     
       @IBOutlet private weak var tableView: UITableView! {
           didSet {
               tableView.coverableCellsIdentifiers = cellsIdentifiers
               tableView.tableFooterView = UIView()
               tableView.estimatedRowHeight = 160
           }
       }
       private var numberOfSections = 1
       private var numberOfRows = 8
       private var loadingPlaceholderView = LoadingPlaceholderView(){
           didSet{
               do {
                   loadingPlaceholderView.gradientColor = .white
                   loadingPlaceholderView.backgroundColor = .white
               }
           }
       }
       var refreshControl = UIRefreshControl()
       var aryList = NSMutableArray()
       var arySelectedList = NSMutableArray()
       var strErrorMessage  = ""
       private var cellsIdentifiers = [
           "HistoryReceiverCell",
           "HistoryReceiverCell","HistoryReceiverCell",
           "HistoryReceiverCell","HistoryReceiverCell",
           "HistoryReceiverCell"
       ]

    
     // MARK:
        // MARK:- LifeCycle
        override func viewDidLoad() {
            super.viewDidLoad()
            self.setUpUI()
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.view.endEditing(true)
        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
        }
        
        // MARK:
        // MARK:- IBAction
        @IBAction func actionOnBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
        @IBAction func actionOnFilter(_ sender: UIButton) {
            
        }
        // MARK:
        // MARK:- Extra Function
        func setUpUI() {
            txtSearch.layer.cornerRadius = 10
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.txtSearch.delegate = self
                self.refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
                self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
                if(isInternetAvailable()){
                    //self.loadingPlaceholderView.cover(self.viewHold, animated: true)
                   // self.callgetHistoryDonateListAPI()
                }else{
                    self.aryList = NSMutableArray()
                    self.strErrorMessage = alertInternet
                    self.tableView.reloadData()
                }
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.addSubview(self.refreshControl)
            }
        }
        
        @objc func refresh(sender:AnyObject) {
            txtSearch.text = ""
          //  self.callgetHistoryDonateListAPI()
        }
    }
    // MARK: -
    // MARK: -UITableViewDelegate

    extension HistoryReciverVC : UITableViewDelegate, UITableViewDataSource{
        func numberOfSections(in tableView: UITableView) -> Int {
            return numberOfSections
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return numberOfRows
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView
                .dequeueReusableCell(withIdentifier: "HistoryReceiverCell", for: indexPath as IndexPath) as! HistoryReceiverCell
            let number = formattedNumber(number: "1685730669")
                      
            cell.lblContactName.text = "Navin Patidar - \(number)"
            cell.lblDate.text = "Date&Time 08/11/20 10:00 AM"
            if(arySelectedList.count != 0){
                cell.ConfigureDataOnCell(dict: (arySelectedList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
              

            }
            return cell
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if(aryList.count != 0){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryDetailVC") as? HistoryDetailVC
                self.navigationController?.pushViewController(vc!, animated: true)

            }
        }
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
            customView.backgroundColor = UIColor.clear
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
            button.setTitle("\(strErrorMessage)", for: .normal)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            
            button.setTitleColor(UIColor.lightGray, for: .normal)
            customView.addSubview(button)
            return customView
        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return strErrorMessage == "" ? 0 : tableView.frame.height
        }
        
        @objc func buttonAction(_ sender: UIButton!) {
            print("Button tapped")
        }
       
        
    }
    // MARK: -
    // MARK: -TransportCell
    class HistoryReceiverCell: UITableViewCell {
        @IBOutlet weak var lblDate: UILabel!
        @IBOutlet weak var lblContactName: UILabel!

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            self.selectionStyle = .none
        }
        override func awakeFromNib() {
               super.awakeFromNib()
             
           }
       
        
        func ConfigureDataOnCell(dict : NSDictionary)  {
            let number = formattedNumber(number: "9685730669")
            
            lblContactName.text = "Navin Patidar -\(number)"
            lblDate.text = "Date & Time 08/11/20 10:00 AM"
          
        }
    }
    // MARK: -
    // MARK: - UISearchBarDelegate


    extension HistoryReciverVC: UISearchBarDelegate  {
        func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            
            var txtAfterUpdate:NSString = searchBar.text! as NSString
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
            if(self.aryList.count != 0){
                self.searchAutocomplete(Searching: txtAfterUpdate)
            }
            return true
        }
        
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.view.endEditing(true)
            self.searchAutocomplete(Searching: "")
            txtSearch.text = ""
        }
        
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            
            self.searchAutocomplete(Searching: searchBar.text! as NSString)
            self.view.endEditing(true)
        }
        
        func searchAutocomplete(Searching: NSString) -> Void {
            if(self.aryList.count != 0){
                
            // let resultPredicate = NSPredicate(format: "name like %@", Searching)
            self.strErrorMessage = ""
            let resultPredicate = NSPredicate(format: "BookedStatus contains[c] %@ OR Address contains[c] %@ OR DonorName contains[c] %@ OR ManagerName contains[c] %@", argumentArray: [Searching, Searching , Searching, Searching])
            if !(Searching.length == 0) {
                let arrayfilter = (self.aryList ).filtered(using: resultPredicate)
                let nsMutableArray = NSMutableArray(array: arrayfilter)
                self.arySelectedList = NSMutableArray()
                self.arySelectedList = nsMutableArray.mutableCopy() as! NSMutableArray
            }
            else{
                self.arySelectedList = NSMutableArray()
                self.arySelectedList = self.aryList.mutableCopy() as! NSMutableArray
                self.view.endEditing(true)
                txtSearch.text = ""
            }
            if(arySelectedList.count == 0){
                self.strErrorMessage = alertDataNotFound
            }
            DispatchQueue.main.async {
                self.numberOfSections = 1
                self.numberOfRows = self.arySelectedList.count
                self.tableView.reloadData()
            }
           }
        }
    }

    // MARK:
       // MARK:- Call API
    extension HistoryReciverVC {
        
        func callgetHistoryReceiverListAPI() {
         
            let dictJson = NSMutableDictionary()
            dictJson.setValue("0", forKey: "FoodDonetId")
            dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
            dictJson.setValue("0", forKey: "state_id")
            dictJson.setValue("0", forKey: "city_id")
            dictJson.setValue("", forKey: "BookedStatus")
            dictJson.setValue("", forKey: "FromDate")
            dictJson.setValue("", forKey: "ToDate")

            var json = Data()
            var jsonString = NSString()
               if JSONSerialization.isValidJSONObject(dictJson) {
                   // Serialize the dictionary
                   json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
                   jsonString = String(data: json, encoding: .utf8)! as NSString
                   print("UpdateLeadinfo JSON: \(jsonString)")
               }
               let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodDonate xmlns='\(BaseURL)'><DreamDonationSearchDonateMdl>\(jsonString)</DreamDonationSearchDonateMdl></GetFoodDonate></soap12:Body></soap12:Envelope>"
               WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetFoodDonateResult", responcetype: "GetFoodDonateResponse") { (responce, status) in
                   self.loadingPlaceholderView.uncover(animated: false)
                   self.refreshControl.endRefreshing()
                   if status == "Suceess"{
                       let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                       let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                      if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                       
                        self.aryList = NSMutableArray()
                        self.aryList =  NSMutableArray(array: (dictTemp.value(forKey: "DTList")as! NSArray).reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
                        self.arySelectedList = NSMutableArray()
                        self.arySelectedList = NSMutableArray(array: (dictTemp.value(forKey: "DTList")as! NSArray).reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray

                        self.strErrorMessage = ""
                       }else{
                        self.strErrorMessage = apiError
                      }
                  }else{
                       self.strErrorMessage = alertSomeError
                  }
                   self.numberOfRows =  self.aryList.count
                   self.numberOfSections = 1
                   self.tableView.reloadData()
              }
                }
    }

