//
//  ManagerDonateClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/16/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class ManagerDonateClass: UIViewController {
    @IBOutlet weak var btnContinue: TransitionButton!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtContactPerson: ACFloatingTextfield!
    @IBOutlet weak var txtMealDetail: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!

    @IBOutlet weak var txtAddress: ACFloatingTextfield!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnSelectAddress: UIButton!
    
    @IBOutlet weak var viewForMAil: UIView!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var btnEmailYes: TransitionButton!
    @IBOutlet weak var btnEmailNo: TransitionButton!
    
    var aryLocationAdmin = NSMutableArray(),aryLocationManager = NSMutableArray() , aryItemDetail = NSMutableArray()
    var strLat = "" , strLONG = ""
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
       ButtongradientLayer(btnArray: [btnContinue , btnEmailYes])
             btnEmailNo.layer.borderColor = UIColor.black.cgColor
             btnEmailNo.layer.borderWidth = 1.0
        txtFrom.layer.cornerRadius = 14.0
        txtFrom.layer.borderWidth = 1.0
        txtFrom.layer.borderColor = UIColor.lightGray.cgColor
        txtFrom.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtFrom.frame.height))
        txtFrom.leftViewMode = .always
        txtTo.layer.cornerRadius = 14.0
        txtTo.layer.borderWidth = 1.0
        txtTo.layer.borderColor = UIColor.lightGray.cgColor
        txtTo.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtTo.frame.height))
        txtTo.leftViewMode = .always
       
        btnNo.setImage(UIImage(named: "uncheck"), for: .normal)
        btnYes.setImage(UIImage(named: "uncheck"), for: .normal)
        txtMealDetail.isHidden = true
        
   txtMobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        txtContactPerson.text = "\(globleLogInData.value(forKey: "Name")!)"
        txtContactPerson.tag = Int("\(globleLogInData.value(forKey: "DonorVId")!)")!
        txtMobileNumber.text = formattedNumber(number: "\(globleLogInData.value(forKey: "MobileNo")!)")

        
        txtEmail.text = "\(globleLogInData.value(forKey: "EmailId")!)"
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        callgetAdminLocationAPI()
        
    
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
             let text = textField.text ?? ""
             var trimmedText = text.trimmingCharacters(in: .whitespaces)
             trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
             textField.text = trimmedText
              if(txtMobileNumber == textField){
                                txtMobileNumber.text = formattedNumber(number: textField.text!)
                            }
         }
    func goOnSelectionView(tag : Int , ary : NSMutableArray) {
        self.view.endEditing(true)
        if(ary.count != 0){
            let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
            vc.strTag = tag
            vc.aryList = ary
            vc.aryForSelectedListData = self.aryLocationManager
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            self.present(vc, animated: false, completion: {})
        }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertDataNotFound, dismissDelay: 3)
            
        }
        
    }
    
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actionOnHideEmailView(_ sender: UIButton) {
        self.viewForMAil.removeFromSuperview()
    }
    
    @IBAction func actionOnEmailYes(_ sender: TransitionButton) {
        if(txtEmail.text?.count == 0){
                   addErrorMessage(textView: txtEmail, message: alert_Email)
            
        }else if !((txtEmail.text?.isValidEmailAddress())!){
                  addErrorMessage(textView: txtEmail, message: alert_EmailValid)
        }else{
            self.callDonateItemAPI(isMail: "True")

        }
        
    }
    @IBAction func actionOnEmailNo(_ sender: TransitionButton) {
        self.callDonateItemAPI(isMail: "False")

    }
    @IBAction func actionOnContinue(_ sender: TransitionButton) {
        self.view.endEditing(true)
        
        if(self.validationForDonate()){
            
            self.viewForMAil.frame = self.view.frame
            self.view.addSubview(self.viewForMAil)
        }
    }
    @IBAction func actionOnYes(_ sender: UIButton) {
        self.view.endEditing(true)
        btnNo.setImage(UIImage(named: "uncheck"), for: .normal)
        btnYes.setImage(UIImage(named: "redio_button"), for: .normal)
        txtMealDetail.isHidden = false
    }
    @IBAction func actionOnNo(_ sender: UIButton) {
        self.view.endEditing(true)
        btnYes.setImage(UIImage(named: "uncheck"), for: .normal)
        btnNo.setImage(UIImage(named: "redio_button"), for: .normal)
        txtMealDetail.isHidden = true
    }
    @IBAction func actionOnAddress(_ sender: UIButton) {
        self.view.endEditing(true)
        if(txtContactPerson.text == "" || txtMobileNumber.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: "Please add first contact person information like name and mobile number.", viewcontrol: self)
        }else{
            if(self.aryLocationManager.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: "Location not assigned. Please contact to admin.", viewcontrol: self)

            }else{
                self.goOnSelectionView(tag: 6, ary:  self.aryLocationAdmin)
            }
        }
        
    }
    @IBAction func actionOnContactPerson(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnTimeFrom(_ sender: UIButton) {
        self.view.endEditing(true)
                 let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
                 vc.strMode = "DateTime"
                 vc.strTag = 1
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                 vc.modalTransitionStyle = .coverVertical
                 vc.delegateDateTime = self
                 self.present(vc, animated: true, completion: {})
      }
    
    @IBAction func actionOnTimeTo(_ sender: UIButton) {
        self.view.endEditing(true)
                 let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
                 vc.strMode = "DateTime"
                 vc.strTag = 2
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                 vc.modalTransitionStyle = .coverVertical
                 vc.delegateDateTime = self
                 self.present(vc, animated: true, completion: {})
         }
       
  
    
}
//MARK:-
//MARK:- PopUpDelegate
extension ManagerDonateClass : PopUpDelegate{
    func getDataFromPopupDelegateArray(aryData: NSArray, tag: Int) {
        
    }
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
          if (tag == 6){
            txtAddress.text = "\(dictData.value(forKey: "LocationName")!),\(dictData.value(forKey: "Address")!)"
            txtAddress.tag = Int("\(dictData.value(forKey: "LocationId")!)")!
        }
    }
}
//MARK:-
//MARK:- DateTimeDelegate
extension ManagerDonateClass : DateTimeDelegate{
    func getDataFromDelegate(time: Date, tag: Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
              if(tag == 1){
                  self.txtFrom.text = "\(dateFormatter.string(from: time))"
              }else if (tag == 2){
                  self.txtTo.text = "\(dateFormatter.string(from: time))"
              }
    }
  
}
// MARK: -
  // MARK: --API Calling

  extension ManagerDonateClass {
   
    func callgetAdminLocationAPI() {
              let dictJson = NSMutableDictionary()
              dictJson.setValue("\(globleLogInData.value(forKey: "ManagerOwnerDonorVId")!)", forKey: "DonorVId")
              dictJson.setValue("0", forKey: "ManagerDonorVId")
              dictJson.setValue("0", forKey: "LocationId")
              dictJson.setValue("0", forKey: "LocationName")
          var json = Data()
          var jsonString = NSString()
          if JSONSerialization.isValidJSONObject(dictJson) {
              // Serialize the dictionary
              json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
              jsonString = String(data: json, encoding: .utf8)! as NSString
              print("UpdateLeadinfo JSON: \(jsonString)")
          }
          
          let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocation xmlns='\(BaseURL)'><LocationSearchMdl>\(jsonString)</LocationSearchMdl></GetLocation></soap12:Body></soap12:Envelope>"
          WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetLocationResult", responcetype: "GetLocationResponse") { (responce, status) in
   
              if status == "Suceess"{
                  self.callgetManagerLocationAPI()
                  let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                  let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                  
                 if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                  self.aryLocationAdmin = NSMutableArray()
                  self.aryLocationAdmin = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
             
                  }else{
                 }
             }else{
             }
         }
           }
          func callgetManagerLocationAPI() {
                    let dictJson = NSMutableDictionary()
                    dictJson.setValue("0", forKey: "DonorVId")
                    dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "ManagerDonorVId")
                    dictJson.setValue("0", forKey: "LocationId")
                    dictJson.setValue("0", forKey: "LocationName")
                var json = Data()
                var jsonString = NSString()
                if JSONSerialization.isValidJSONObject(dictJson) {
                    // Serialize the dictionary
                    json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
                    jsonString = String(data: json, encoding: .utf8)! as NSString
                    print("UpdateLeadinfo JSON: \(jsonString)")
                }
                
                let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocation xmlns='\(BaseURL)'><LocationSearchMdl>\(jsonString)</LocationSearchMdl></GetLocation></soap12:Body></soap12:Envelope>"
                WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetLocationResult", responcetype: "GetLocationResponse") { (responce, status) in
                   
                    if status == "Suceess"{
                        let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                   //     let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"

                       if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.aryLocationManager = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                         
                        }else{
                          
                       }
                   }else{
                   }
               }
          }

    
    func callDonateItemAPI(isMail : String) {
        
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
        dictJson.setValue("\(globleLogInData.value(forKey: "state_id")!)", forKey: "state_id")
        dictJson.setValue("\(globleLogInData.value(forKey: "city_id")!)", forKey: "city_id")
        dictJson.setValue("\(globleLogInData.value(forKey: "city_name")!)", forKey: "city_name")

        dictJson.setValue("", forKey: "FoodDescription")
        dictJson.setValue("\(self.strLat)", forKey: "Lat")
        dictJson.setValue("\(self.strLONG)", forKey: "Long")
        dictJson.setValue("\(self.txtAddress.text!)", forKey: "Address")
        dictJson.setValue("\(txtFrom.text!)", forKey: "FoodCollectStartTime")
        dictJson.setValue("\(txtTo.text!)", forKey: "FoodCollectEndTime")
        dictJson.setValue(Status.Donate, forKey: "BookedStatus")
        dictJson.setValue("\(txtContactPerson.tag)", forKey: "ManagerDonorVId")
        dictJson.setValue(btnYes.currentImage == UIImage(named: "redio_button") ? "Yes" : "No", forKey: "IsProvideDriverFreeMeal")
        dictJson.setValue("\(txtMealDetail.text!)", forKey: "FreeMealDescription")
        dictJson.setValue("\(txtAddress.tag)", forKey: "LocationId")
        dictJson.setValue(aryItemDetail, forKey: "FoodArray")
        dictJson.setValue(isMail, forKey: "IsMailSend")
        dictJson.setValue(txtEmail.text, forKey: "EmailId")

      var json = Data()
      var jsonString = NSString()
      if JSONSerialization.isValidJSONObject(dictJson) {
          // Serialize the dictionary
          json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
          jsonString = String(data: json, encoding: .utf8)! as NSString
          print("UpdateLeadinfo JSON: \(jsonString)")
      }
      let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)

      let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddDonateFood xmlns='\(BaseURL)'><DreamDonationDonateMdl>\(jsonString)</DreamDonationDonateMdl></AddDonateFood></soap12:Body></soap12:Envelope>"
      WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "AddDonateFoodResult", responcetype: "AddDonateFoodResponse") { (responce, status) in
         loader.dismiss(animated: false, completion: nil)
          if status == "Suceess"{
              let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
              let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
             if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                               CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 3)
                   for controller in self.navigationController!.viewControllers as Array {
                       if controller.isKind(of: DashBoardClass.self) {
                           _ =  self.navigationController!.popToViewController(controller, animated: true)
                           break
                       }
                   }

              }else{
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)

             }
         }else{
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)

         }
         
     }
       }
  }
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension ManagerDonateClass : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtMobileNumber){
           return ACFloatingTextValidation(textField: txtMobileNumber, string: string, returnOnly: "NUMBER", limitValue: 20)
        }
        if(textField == txtContactPerson){
                 return ACFloatingTextValidation(textField: txtContactPerson, string: string, returnOnly: "All", limitValue: 35)
              }
        return true
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}

// MARK:
// MARK:- Validation

extension ManagerDonateClass {
    
    func validationForDonate() -> Bool {
        
        if(txtFrom.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage:alert_FromTime, viewcontrol: self)
            return false
        }
            
        else if(txtTo.text?.count == 0){
           showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_ToTime, viewcontrol: self)
            return false
        }

        else if(txtContactPerson.text?.count == 0){
            addErrorMessage(textView: txtContactPerson, message: alert_required)
            return false
        }
            
        else if((txtMobileNumber.text!.count == 0)){
            addErrorMessage(textView: txtMobileNumber, message: alert_required)
            return false
        }
            
        else if(txtMobileNumber.text!.count < 10){
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumberValid)
            return false
        }
        else if(txtAddress.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_Address, viewcontrol: self)
            return false
        }
        
        return true
    }
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
}
                 
