//
//  individualDonateClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/16/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications
import MapKit
import GooglePlacesSearchController
class DonateIndividualClass: UIViewController {
    // MARK:
    // MARK:-IBOutlet
    @IBOutlet weak var btnContinue: TransitionButton!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtContactPerson: ACFloatingTextfield!
    @IBOutlet weak var txtMealDetail: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    @IBOutlet weak var viewForMAil: UIView!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var btnEmailYes: TransitionButton!
    @IBOutlet weak var btnEmailNo: TransitionButton!
    @IBOutlet weak var txtAddress: ACFloatingTextfield!

    var aryLocationAdmin = NSMutableArray(),aryLocationManager = NSMutableArray() , aryItemDetail = NSMutableArray()
    var strLat = "" , strLONG = "" ,strCountryName = ""
    // MARK: - ----------- Google Address Code ----------------
       // MARK: -
          @IBOutlet weak var scrollView: UIScrollView!
          @IBOutlet weak var tableView: UITableView!
          private var apiKey: String = GoogleMapsAPIServerKey
          private var placeType: PlaceType = .all
          private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
          private var radius: Double = 0.0
          private var strictBounds: Bool = false
          private var places = [Place]() {
              didSet { tableView.reloadData() }
          }
    // MARK:
    // MARK:-LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        ButtongradientLayer(btnArray: [btnContinue , btnEmailYes])
             btnEmailNo.layer.borderColor = UIColor.black.cgColor
        
             btnEmailNo.layer.borderWidth = 1.0
        txtFrom.layer.cornerRadius = 14.0
        txtFrom.layer.borderWidth = 1.0
        txtFrom.layer.borderColor = UIColor.lightGray.cgColor
        txtFrom.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtFrom.frame.height))
        txtFrom.leftViewMode = .always
        txtTo.layer.cornerRadius = 14.0
        txtTo.layer.borderWidth = 1.0
        txtTo.layer.borderColor = UIColor.lightGray.cgColor
        txtTo.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtTo.frame.height))
        txtTo.leftViewMode = .always
 
        btnNo.setImage(UIImage(named: "uncheck"), for: .normal)
        btnYes.setImage(UIImage(named: "uncheck"), for: .normal)
        txtMealDetail.isHidden = true
        
        txtMobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        txtContactPerson.text = "\(globleLogInData.value(forKey: "Name")!)"
        txtContactPerson.tag = Int("\(globleLogInData.value(forKey: "DonorVId")!)")!
        txtMobileNumber.text = formattedNumber(number: "\(globleLogInData.value(forKey: "MobileNo")!)")

        txtAddress.text = "\(globleLogInData.value(forKey: "Address")!)"
        self.strLat = "\(globleLogInData.value(forKey: "Lat")!)"
        self.strLONG = "\(globleLogInData.value(forKey: "Long")!)"
        txtEmail.text = "\(globleLogInData.value(forKey: "EmailId")!)"
   txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
             let text = textField.text ?? ""
             var trimmedText = text.trimmingCharacters(in: .whitespaces)
             trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
             textField.text = trimmedText
            if(txtMobileNumber == textField){
                                          txtMobileNumber.text = formattedNumber(number: textField.text!)
                                      }
         }
    // MARK:
    // MARK:- IBAction
        @IBAction func actionOnHideEmailView(_ sender: UIButton) {
        self.viewForMAil.removeFromSuperview()
    }
    
    @IBAction func actionOnEmailYes(_ sender: TransitionButton) {
        if(txtEmail.text?.count == 0){
                   addErrorMessage(textView: txtEmail, message: alert_Email)
            
        }else if !((txtEmail.text?.isValidEmailAddress())!){
                  addErrorMessage(textView: txtEmail, message: alert_EmailValid)
        }else{
            self.callDonateItemAPI(isMail: "True")

        }
        
    }
    @IBAction func actionOnEmailNo(_ sender: TransitionButton) {
        self.callDonateItemAPI(isMail: "False")

    }
    @IBAction func actionOnContinue(_ sender: TransitionButton) {
        self.view.endEditing(true)
        
        if(self.validationForDonate()){
            
            self.viewForMAil.frame = self.view.frame
            self.view.addSubview(self.viewForMAil)
        }
    }
    
    @IBAction func actionOnYes(_ sender: UIButton) {
        self.view.endEditing(true)
        btnNo.setImage(UIImage(named: "uncheck"), for: .normal)
        btnYes.setImage(UIImage(named: "redio_button"), for: .normal)
        txtMealDetail.isHidden = false
    }
    @IBAction func actionOnNo(_ sender: UIButton) {
        self.view.endEditing(true)
        btnYes.setImage(UIImage(named: "uncheck"), for: .normal)
        btnNo.setImage(UIImage(named: "redio_button"), for: .normal)
        txtMealDetail.isHidden = true
    }
    @IBAction func actionOnAddress(_ sender: UIButton) {
        self.view.endEditing(true)

    }
    @IBAction func actionOnContactPerson(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnTimeFrom(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
        vc.strMode = "DateTime"
        vc.strTag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegateDateTime = self
        self.present(vc, animated: true, completion: {})
    }
    
       @IBAction func actionOnTimeTo(_ sender: UIButton) {
           self.view.endEditing(true)
                    let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
                    vc.strMode = "DateTime"
                    vc.strTag = 2
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    vc.modalTransitionStyle = .coverVertical
                    vc.delegateDateTime = self
                    self.present(vc, animated: true, completion: {})
            }
          
}

//MARK:-
//MARK:- DateTimeDelegate
extension DonateIndividualClass : DateTimeDelegate{
    func getDataFromDelegate(time: Date, tag: Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
              if(tag == 1){
                  self.txtFrom.text = "\(dateFormatter.string(from: time))"
              }else if (tag == 2){
                  self.txtTo.text = "\(dateFormatter.string(from: time))"
              }
    }
  
}
// MARK:
// MARK:- Validation

extension DonateIndividualClass {
    
    func validationForDonate() -> Bool {
        
        if(txtFrom.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage:alert_FromTime, viewcontrol: self)
          
            return false
        }
        else if(txtTo.text?.count == 0){
           showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_ToTime, viewcontrol: self)
            return false
        }

        else if(txtContactPerson.text?.count == 0){
            addErrorMessage(textView: txtContactPerson, message: alert_required)
            return false
        }
        else if((txtMobileNumber.text!.count == 0)){
            addErrorMessage(textView: txtMobileNumber, message: alert_required)
            return false
            
        }
            
        else if(txtMobileNumber.text!.count < 10){
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumberValid)
            return false
        }
        else if (txtAddress.text == ""){
               addErrorMessage(textView: txtAddress, message: alert_Address)
            return false
        }
        else if(strLat == "" || strLat == "0"){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_AddressProper, viewcontrol: self)
                      return false
        }
        
        return true
    }
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
}


// MARK: -
// MARK: --API Calling

extension DonateIndividualClass {
    func callDonateItemAPI(isMail : String) {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
        dictJson.setValue("\(globleLogInData.value(forKey: "state_id")!)", forKey: "state_id")
        dictJson.setValue("\(globleLogInData.value(forKey: "city_id")!)", forKey: "city_id")
        dictJson.setValue("\(globleLogInData.value(forKey: "city_name")!)", forKey: "city_name")

        dictJson.setValue("", forKey: "FoodDescription")
        dictJson.setValue("\(self.strLat)", forKey: "Lat")
        dictJson.setValue("\(self.strLONG)", forKey: "Long")
        dictJson.setValue("\(self.txtAddress.text!)", forKey: "Address")
        dictJson.setValue("\(txtFrom.text!)", forKey: "FoodCollectStartTime")
        dictJson.setValue("\(txtTo.text!)", forKey: "FoodCollectEndTime")
        dictJson.setValue(Status.Donate, forKey: "BookedStatus")
        
        dictJson.setValue("\(txtContactPerson.tag)", forKey: "ManagerDonorVId")
        dictJson.setValue(btnYes.currentImage == UIImage(named: "redio_button") ? "Yes" : "No", forKey: "IsProvideDriverFreeMeal")
        dictJson.setValue("\(txtMealDetail.text!)", forKey: "FreeMealDescription")
        dictJson.setValue("\(txtAddress.tag)", forKey: "LocationId")
        dictJson.setValue(aryItemDetail, forKey: "FoodArray")
        dictJson.setValue(isMail, forKey: "IsMailSend")
        dictJson.setValue(txtEmail.text, forKey: "EmailId")

        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictJson) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddDonateFood xmlns='\(BaseURL)'><DreamDonationDonateMdl>\(jsonString)</DreamDonationDonateMdl></AddDonateFood></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "AddDonateFoodResult", responcetype: "AddDonateFoodResponse") { (responce, status) in
            loader.dismiss(animated: false, completion: nil)
            if status == "Suceess"{
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 3)
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: DashBoardClass.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                    
                }
            }else{
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                
            }
            
        }
    }
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension DonateIndividualClass : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
         
         if(textField == txtAddress){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
               self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 260), animated: false)
                         self.scrollView.isScrollEnabled = false
            }
          

         }
     }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

      if(textField == txtMobileNumber){
              return ACFloatingTextValidation(textField: txtMobileNumber, string: string, returnOnly: "NUMBER", limitValue: 20)
           }
           if(textField == txtContactPerson){
                    return ACFloatingTextValidation(textField: txtContactPerson, string: string, returnOnly: "All", limitValue: 35)
                 }
        if(textField == txtAddress){
            if (range.location == 0) && (string == " ")
            {
                return false
            }
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.tableView.removeFromSuperview()
                    return true
                }
            }
            var txtAfterUpdate:NSString = txtAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
               if(textField == txtAddress){

                    let txtAfterUpdate:NSString = txtAddress.text! as NSString
                    if(txtAfterUpdate == ""){
                        places = [];
                        return true
                    }
                    let parameters = getParameters(for: txtAfterUpdate as String)

                    return true
                }
        return true
    }
    
       func textFieldDidEndEditing(_ textField: UITextField) {
           if(textField == txtAddress){
               self.scrollView.isScrollEnabled = true
               self.tableView.removeFromSuperview()
           }
           
       }
       private func getParameters(for text: String) -> [String: String] {
             var params = [
                 "input": text,
                 "types": placeType.rawValue,
                 "key": apiKey
             ]
             
             if CLLocationCoordinate2DIsValid(coordinate) {
                 params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
                 
                 if radius > 0 {
                     params["radius"] = "\(radius)"
                 }
                 
                 if strictBounds {
                     params["strictbounds"] = "true"
                 }
             }
             
             return params
         }
         
}

extension DonateIndividualClass {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.tableView.removeFromSuperview()
                }
                else
                {
               
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                 
                    self.tableView.layer.cornerRadius = 8.0
                    
                    self.tableView.frame = CGRect(x: Int(self.txtAddress.frame.origin.x), y: 320, width: Int(self.txtAddress.frame.width), height:200)
                    self.scrollView.addSubview(self.tableView)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension DonateIndividualClass : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            self.txtAddress.text = value.formattedAddress
            self.strLat = "\(value.coordinate?.latitude ?? 0.0)"
            self.strLONG = "\(value.coordinate?.longitude ?? 0.0)"

            self.places = [];
            
            if("\(value.countryCode!)" != "US"){
                self.strLat = "0"
                self.strLONG = "0"
                self.txtAddress.text = ""
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLocationCountry, viewcontrol: self)
            }
        }
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
