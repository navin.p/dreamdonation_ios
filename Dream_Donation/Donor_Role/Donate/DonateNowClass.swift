//
//  DonateNowClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/17/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class DonateNowClass: UIViewController {
    
    @IBOutlet weak var txtItemName: ACFloatingTextfield!
    @IBOutlet weak var txtItemDescription: ACFloatingTextfield!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var txtUnit: UITextField!
    @IBOutlet weak var txtDays: UITextField!
    @IBOutlet weak var txtHours: UITextField!
    
    @IBOutlet weak var btnClose: TransitionButton!
    @IBOutlet weak var btnAdd: TransitionButton!
    @IBOutlet weak var btnContinue: TransitionButton!
    @IBOutlet weak var lblErrorMsg: UILabel!
    @IBOutlet weak var viewheader: GradientView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var btnNow: UIButton!

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    var aryList = NSMutableArray() , aryUnit = NSMutableArray()
    var tagForShowHide = ""
    var editTag = 0
     
    
    
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        if(aryList.count == 0){
            lblErrorMsg.text = "Click on + botton and add item."
            btnAdd.setTitle("Add", for: .normal)
            scroll.isHidden = false
            btnNow.isHidden = true
            btnContinue.isHidden = true
        }
        ButtongradientLayer(btnArray: [btnClose,btnAdd,btnContinue])
        
        txtHours.layer.cornerRadius = 14.0
        txtDays.layer.cornerRadius = 14.0
        txtQuantity.layer.cornerRadius = 14.0
        txtUnit.layer.cornerRadius = 14.0
       
        txtHours.layer.borderWidth = 1.0
        txtDays.layer.borderWidth = 1.0
        txtQuantity.layer.borderWidth = 1.0
        txtUnit.layer.borderWidth = 1.0

        txtHours.layer.borderColor = UIColor.lightGray.cgColor
        txtDays.layer.borderColor = UIColor.lightGray.cgColor
        txtQuantity.layer.borderColor = UIColor.lightGray.cgColor
        txtUnit.layer.borderColor = UIColor.lightGray.cgColor
      
        txtHours.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtHours.frame.height))
        txtHours.leftViewMode = .always
        txtDays.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtDays.frame.height))
        txtDays.leftViewMode = .always
        txtQuantity.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtDays.frame.height))
        txtQuantity.leftViewMode = .always
        txtUnit.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtUnit.frame.height))
        txtUnit.leftViewMode = .always
        
        aryUnit = getDataFromCoreDataBase(strEntity: "Unit", strkey: "unit")

    }
    func goOnSelectionView(tag : Int , ary : NSMutableArray) {
              self.view.endEditing(true)
              let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
              vc.strTag = tag
              vc.aryList = ary
              vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
              vc.modalTransitionStyle = .coverVertical
              vc.delegate = self
              self.present(vc, animated: false, completion: {})
          }
         
    
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actionOnContinue(_ sender: TransitionButton) {
        self.view.endEditing(true)
        if(aryList.count == 0){
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: "Click on + botton and add item.", dismissDelay: 3)

        }else{
            
            if(globleRoleUser == "Manager"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ManagerDonateClass") as? ManagerDonateClass
                vc?.aryItemDetail = self.aryList
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }else if (globleRoleUser == "Donor" && globleSubRoleUser == "Individual"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DonateIndividualClass") as? DonateIndividualClass
                vc?.aryItemDetail = self.aryList
                self.navigationController?.pushViewController(vc!, animated: true)
            }else if (globleRoleUser == "Donor" && globleSubRoleUser == "Corporate"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DonateCorporateClass") as? DonateCorporateClass
                vc?.aryItemDetail = self.aryList
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        }
    }
    
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnPluse(_ sender: UIButton) {
        txtItemName.text = ""
              txtItemDescription.text = ""
        txtUnit.text = ""
        txtQuantity.text = ""
        txtDays.text = ""
        txtHours.text = ""
        scroll.isHidden = false
        btnNow.isHidden = true
        btnContinue.isHidden = true
      
    }
    
    @IBAction func actionOnUnit(_ sender: UIButton) {
        self.view.endEditing(true)
        self.aryUnit.count == 0 ? self.callgetUnitAPI(): self.goOnSelectionView(tag: 4, ary: self.aryUnit)

    }
    
    @IBAction func actionOnClose(_ sender: UIButton) {
        self.view.endEditing(true)

        if(aryList.count == 0){
            self.navigationController?.popViewController(animated: true)

        }else{
            scroll.isHidden = true
            btnNow.isHidden = false
            btnContinue.isHidden = false

            self.tableView.reloadData()
        }
        
    }
    
    @IBAction func actionOnAdd(_ sender: TransitionButton) {
        self.view.endEditing(true)

        if(validation()){
            let dict = NSMutableDictionary()
                  dict.setValue("\(txtItemName.text!)", forKey: "FoodItem")
                  dict.setValue("\(txtItemDescription.text!)", forKey: "OtherFoodDetail")
                  dict.setValue("\(txtQuantity.text!)", forKey: "NoOfPerson")
                  dict.setValue("\(txtUnit.text!)", forKey: "Unit")
                  dict.setValue("\(txtDays.text!)", forKey: "SelfLifeDays")
                  dict.setValue("\(txtHours.text!)", forKey: "SelfLifeHours")
                  dict.setValue("", forKey: "SelfLifeStartDate")
                  dict.setValue("", forKey: "SelfLifeEndDate")
                    
                  if("Edit" == btnAdd.titleLabel?.text!){
                          aryList.replaceObject(at: editTag, with: dict)
                      }else{
                          aryList.add(dict)
                      }
                      scroll.isHidden = true
                      btnNow.isHidden = false
                      btnContinue.isHidden = false

                      self.tagForShowHide = "\(aryList.count - 1)"

                      self.tableView.reloadData()
              
                  if(aryList.count == 0){
                      lblErrorMsg.text = "Click on + botton and add item."
                  }else{
                      lblErrorMsg.text = ""
                  }
        }
            
 
      
    }
    
    func validation() -> Bool {
     if(txtItemName.text?.count == 0){
            txtItemName.shakeLineWithError = true
            txtItemName.showErrorWithText(errorText: alert_required)
        return false
        }

        else if (txtQuantity.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Quantity is required!", viewcontrol: self)
         return false

        }
        else if (txtUnit.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Unit is required!", viewcontrol: self)
            return false

        }
        else if (txtDays.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Days is required!", viewcontrol: self)
        return false

        }
        else if (txtHours.text?.count != 0 ){
        if(Int(txtHours.text!)! >= 24){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Hours should be minimum 24.", viewcontrol: self)
            return false


        }
        }
       return true
    }
}
// MARK: -
// MARK: -UITableViewDelegate

extension DonateNowClass : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagForShowHide == "\(section)" ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath as IndexPath) as! ItemCell
        cell.ConfigureDataOnCell(dict: aryList.object(at: indexPath.section)as! NSDictionary)
        cell.btnEdit.tag = indexPath.section
        cell.btnEdit.addTarget(self, action: #selector(buttonActionEdit(_:)), for: .touchDown)
        cell.btnDelete.tag = indexPath.section
        cell.btnDelete.addTarget(self, action: #selector(buttonActionDelete(_:)), for: .touchDown)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
            
            
        }
    }
  
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let dict = aryList.object(at: section)as! NSDictionary
        let strItemName = "\(dict.value(forKey: "FoodItem")!)"
        // var strItemQuantity = "\(dict.value(forKey: "itemQuantity")!)"
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.white
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.width - 20, height: 50))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        button.tag = section
        button.addTarget(self, action: #selector(buttonActionHeader(_:)), for: .touchDown)
        let arrowImage = UIImageView(frame: CGRect(x: tableView.frame.width - 58, y: 12, width: 25, height: 25))
        arrowImage.contentMode = .scaleAspectFit
        tagForShowHide == "\(section)" ? arrowImage.image = UIImage(named: "dropUp") : (arrowImage.image = UIImage(named: "dropdown"))
        lbl.text = "\(section + 1). Item: \(strItemName)"
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(arrowImage)
        customView.addSubview(lbl)
        customView.addSubview(button)
        
        let lblUnder = UILabel(frame: CGRect(x: 0, y: 49, width: tableView.frame.width, height: 1))
        lblUnder.backgroundColor = UIColor.lightGray
        customView.addSubview(lblUnder)

        
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
        
    }
    @objc func buttonActionHeader(_ sender: UIButton!) {
        print("Button tapped")
        tagForShowHide == "\(sender.tag)" ? tagForShowHide = "" : (tagForShowHide = "\(sender.tag)")
        self.tableView.reloadData()
    }
    @objc func buttonActionEdit(_ sender: UIButton!) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        
        txtItemName.text = "\(dict.value(forKey: "FoodItem")!)"
        txtItemDescription.text = "\(dict.value(forKey: "OtherFoodDetail")!)"
        txtQuantity.text = "\(dict.value(forKey: "Unit")!)"
        txtUnit.text = "\(dict.value(forKey: "NoOfPerson")!)"
        txtDays.text = "\(dict.value(forKey: "SelfLifeDays")!)"
        txtHours.text = "\(dict.value(forKey: "SelfLifeHours")!)"
        
        editTag = sender.tag
        btnAdd.setTitle("Edit", for: .normal)
        scroll.isHidden = false
        btnNow.isHidden = true
        btnContinue.isHidden = true
        
    }
    @objc func buttonActionDelete(_ sender: UIButton!) {
        let alert = UIAlertController.init(title: alertMsg, message: alertDelete, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.aryList.remove(sender.tag)
            self.tableView.reloadData()
            print("Yay! You brought your towel!")
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
            print("Yay! You brought your towel!")
        }))
        self.present(alert, animated: true)

    }
}
// MARK: -
// MARK: -TransportCell
class ItemCell: UITableViewCell {
    @IBOutlet weak var lblItemDetail: UILabel!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblHrs: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func ConfigureDataOnCell(dict : NSDictionary)  {
 
        lblItemDetail.text = "\(dict.value(forKey: "OtherFoodDetail")!)" == "" ? "-" : "\(dict.value(forKey: "OtherFoodDetail")!)"
        lblUnit.text = "\(dict.value(forKey: "Unit")!)" == "" ? "-" : "\(dict.value(forKey: "Unit")!)"
        lblQuantity.text = "\(dict.value(forKey: "NoOfPerson")!)" == "" ? "-" : "\(dict.value(forKey: "NoOfPerson")!)"
        lblDay.text = "\(dict.value(forKey: "SelfLifeDays")!)" == "" ? "-" : "\(dict.value(forKey: "SelfLifeDays")!)"
        lblHrs.text = "\(dict.value(forKey: "SelfLifeHours")!)" == "" ? "-" : "\(dict.value(forKey: "SelfLifeHours")!)"
    }
}

//MARK:-
//MARK:- ---------UITextFieldDelegate

extension DonateNowClass : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtDays){
           return txtFiledValidation(textField: txtDays, string: string, returnOnly: "NUMBER", limitValue: 1)
        }
        if(textField == txtHours){
           return txtFiledValidation(textField: txtHours, string: string, returnOnly: "NUMBER", limitValue: 1)
        }
        if(textField == txtQuantity){
                  return txtFiledValidation(textField: txtQuantity, string: string, returnOnly: "NUMBER", limitValue: 2)
               }
        return true
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
// MARK: -
// MARK: --API Calling

extension DonateNowClass {
    func callgetUnitAPI() {
  let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)

    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodUnit xmlns='\(BaseURL)'></GetFoodUnit></soap12:Body></soap12:Envelope>"
    WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetFoodUnitResult", responcetype: "GetFoodUnitResponse") { (responce, status) in
        loader.dismiss(animated: false) {
            if status == "Suceess"{
                      let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                      let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                      
                     if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                      self.aryUnit = NSMutableArray()
                      self.aryUnit = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                      self.goOnSelectionView(tag: 4, ary:  self.aryUnit)

                      }else{
                       CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)

                     }
                 }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)

                 }
        }
      
       
        
   }
     }
}
//MARK:-
//MARK:- PopUpDelegate
extension DonateNowClass : PopUpDelegate{
    func getDataFromPopupDelegateArray(aryData: NSArray, tag: Int) {
        
    }
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
            txtUnit.text = "\(dictData.value(forKey: "Unit")!)"
            //txtUnit.tag = Int("\(dictData.value(forKey: "state_id")!)")!
    }
}
