//
//  HistoryDetailVC.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/20/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import UserNotifications
import TransitionButton

class HistoryDetailVC: UIViewController {
   
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var viewHold: UIView!
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.coverableCellsIdentifiers = cellsIdentifiers
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 160
        }
    }
    private var numberOfSections = 0
    private var numberOfRows = 0
    private var loadingPlaceholderView = LoadingPlaceholderView(){
        didSet{
            do {
                loadingPlaceholderView.gradientColor = .white
                loadingPlaceholderView.backgroundColor = .white
            }
        }
    }
    var refreshControl = UIRefreshControl()
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strErrorMessage  = ""
    private var cellsIdentifiers = [
        "HistoryDetailCell1",
        "HistoryDetailCell2","HistoryDetailCell2",
        "HistoryDetailCell2","HistoryDetailCell2",
        "HistoryDetailCell2"
    ]
    var dictItemData = NSMutableDictionary()
    // MARK:
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
              self.refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
              self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
              if(isInternetAvailable()){
                  self.loadingPlaceholderView.cover(self.viewHold, animated: true)
                  self.callgetHistoryDetailDonateListAPI()
              }else{
                  self.aryList = NSMutableArray()
                  self.strErrorMessage = alertInternet
                  self.tableView.reloadData()
              }
              self.tableView.delegate = self
              self.tableView.dataSource = self
              self.tableView.addSubview(self.refreshControl)
          }
      }
      
      @objc func refresh(sender:AnyObject) {
          self.callgetHistoryDetailDonateListAPI()
      }
}

// MARK: -
// MARK: -UITableViewDelegate

extension HistoryDetailVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView
                .dequeueReusableCell(withIdentifier: "HistoryDetailCell1", for: indexPath as IndexPath) as! HistoryDetailCell
            cell.lblDate.text = "Date & Time \(self.dictItemData.value(forKey: "CreatedDate")!)"
            cell.lblAddress.text = "\(self.dictItemData.value(forKey: "Address")!)"
//            let ManagerMobileNo = formattedNumber(number: "\(dict.value(forKey: "ManagerMobileNo")!)")
//            lblName.text = "\(dict.value(forKey: "DonorName")!)  \(number)"
            
            return cell
        }else{
            let cell = tableView
                .dequeueReusableCell(withIdentifier: "HistoryDetailCell2", for: indexPath as IndexPath) as! HistoryDetailCell
            let dict = ((aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary).removeNullFromDict()
            
            cell.lblItemName.text = "\(indexPath.row + 1). \(dict.value(forKey: "FoodItem")!)"
            cell.lblDetail.text = "\(dict.value(forKey: "OtherFoodDetail")!)"
            let strQty = "\(dict.value(forKey: "NoOfPerson")!)"
            let strUnit = "\(dict.value(forKey: "Unit")!)"

            cell.lblQty.text = strQty + " " + strUnit
            
            var strDay = "\(dict.value(forKey: "SelfLifeDays")!)"
            var strHr = "\(dict.value(forKey: "SelfLifeHours")!)"
            if(strDay == ""){
               strDay = "0"
            }
            if(strHr == ""){
                          strHr = "0"
            }
            cell.lblShelfLife.text = strDay + " Day " + strHr + " Hrs "

            return cell
        }
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
            
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
  
}
// MARK: -
// MARK: -TransportCell
class HistoryDetailCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTransportedBy: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblShelfLife: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    override func awakeFromNib() {
           super.awakeFromNib()
         
       }
   
    
 
}



// MARK:
   // MARK:- Call API
extension HistoryDetailVC {
    
    func callgetHistoryDetailDonateListAPI() {
     
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(self.dictItemData.value(forKey: "FoodDonetId")!)", forKey: "FoodDonetId")
        dictJson.setValue("0", forKey: "DonorVId")
        dictJson.setValue("0", forKey: "state_id")
        dictJson.setValue("0", forKey: "city_id")
        dictJson.setValue("", forKey: "BookedStatus")
        dictJson.setValue("", forKey: "FromDate")
        dictJson.setValue("", forKey: "ToDate")

        var json = Data()
        var jsonString = NSString()
           if JSONSerialization.isValidJSONObject(dictJson) {
               // Serialize the dictionary
               json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
               jsonString = String(data: json, encoding: .utf8)! as NSString
               print("UpdateLeadinfo JSON: \(jsonString)")
           }
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodDonationMapping xmlns='\(BaseURL)'><FoodDonetId>\(self.dictItemData.value(forKey: "FoodDonetId")!)</FoodDonetId></GetFoodDonationMapping></soap12:Body></soap12:Envelope>"
           WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetFoodDonationMappingResult", responcetype: "GetFoodDonationMappingResponse") { (responce, status) in
               self.loadingPlaceholderView.uncover(animated: false)
               self.refreshControl.endRefreshing()
               if status == "Suceess"{
                   let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                   let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                  if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                   self.aryList = NSMutableArray()
                   self.aryList = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                    self.arySelectedList = NSMutableArray()
                    self.arySelectedList = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                    self.strErrorMessage = ""
                   }else{
                    self.strErrorMessage = apiError
                  }
              }else{
                   self.strErrorMessage = alertSomeError
              }
               self.numberOfRows =  self.aryList.count
               self.numberOfSections = 2
               self.tableView.reloadData()
          }
            }
}
