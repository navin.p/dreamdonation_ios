//
//  ManagerListClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/6/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import TransitionButton

class ManagerListClass: UIViewController {
      // MARK:
        // MARK:- IBOutlet
        
        @IBOutlet weak var viewHold: UIView!
        @IBOutlet weak var btnNext: TransitionButton!
        @IBOutlet weak var btnAdd: TransitionButton!
        @IBOutlet weak var btnAddHeight: NSLayoutConstraint!

        @IBOutlet private weak var tableView: UITableView! {
            didSet {
                tableView.coverableCellsIdentifiers = cellsIdentifiers
                tableView.tableFooterView = UIView()
                tableView.estimatedRowHeight = 200
            }
        }
        var aryList = NSMutableArray()
        var strErrorMessage  = "List Empty!"
        var strComeFrom = ""
        private var numberOfSections = 0
        private var numberOfRows = 0
        private var loadingPlaceholderView = LoadingPlaceholderView(){
            didSet{
                do {
                    loadingPlaceholderView.gradientColor = .white
                    loadingPlaceholderView.backgroundColor = .white
                }
            }
        }
        var refreshControl = UIRefreshControl()
        
        private var cellsIdentifiers = [
            "ManagerCell",
            "ManagerCell","ManagerCell",
            "ManagerCell","ManagerCell",
            "ManagerCell"
        ]
        
        // MARK:
        // MARK:- Life Cycle
        override func viewDidLoad() {
            super.viewDidLoad()
            self.setUpUI()
            refreshControl.attributedTitle = NSAttributedString(string: "Get Manager list...")
            refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
             NotificationCenter.default.addObserver(self, selector: #selector(refresh(sender:)), name: Notification.Name("Dream_ManagerADD"), object: nil)

            if(isInternetAvailable()){
                loadingPlaceholderView.cover(viewHold, animated: true)
                 callgetManagerListAPI()
            }else{
                self.aryList = NSMutableArray()
                self.strErrorMessage = alertInternet
                self.tableView.reloadData()
            }
            tableView.addSubview(refreshControl)
            (strComeFrom == "Navigation" || strComeFrom != "Location") ? btnAddHeight.constant = 0.0 : (btnAddHeight.constant = 44.0)
        }
      
    @objc func refresh(sender:AnyObject) {
            callgetManagerListAPI()
        }
    
        // MARK:
           // MARK:- Extra Function
           func setUpUI() {
               ButtongradientLayer(btnArray: [btnNext])
           }
        // MARK:
        // MARK:- IBAction
        
        @IBAction func actionOnBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
        @IBAction func actionOnNext(_ sender: UIButton) {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardClass") as? DashBoardClass
             self.navigationController?.pushViewController(vc!, animated: true)
        }
        @IBAction func actionOnAdd(_ sender: UIButton) {
            let message = "Invite link:"
            let inviteLink = "\(URL_InviteManager)" + "\(globleLogInData.value(forKey: "DonorVId")!)"
                  if let link = NSURL(string: "\(inviteLink)")
                  {
                    let objectsToShare = [message,link] as [Any]
                      let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                   // activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                    self.present(activityVC, animated: true, completion: nil)
                  }
        }
    }
    // MARK: -
    // MARK: --UITableViewDelegate

    extension ManagerListClass : UITableViewDelegate, UITableViewDataSource{
        func numberOfSections(in tableView: UITableView) -> Int {
            return numberOfSections
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return numberOfRows
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView
                .dequeueReusableCell(withIdentifier: "ManagerCell", for: indexPath as IndexPath) as! ManagerCell
            if(aryList.count != 0){
                
                cell.switchStatus.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                cell.btnAssign.addTarget(self, action: #selector(actionAssignLocation), for: UIControl.Event.touchDown)
                cell.switchStatus.tag = indexPath.row
                cell.btnAssign.tag = indexPath.row
                
                let dict = aryList.object(at: indexPath.row)as! NSDictionary
                cell.lbl_Name.text = "\(dict.value(forKey: "Name")!)"
                cell.lbl_Email.text = "\(dict.value(forKey: "EmailId")!)" == "" ? " " : "\(dict.value(forKey: "EmailId")!)"
                cell.lbl_Mobile.text = "\(dict.value(forKey: "MobileNo")!)"

                let LocationMapping = dict.value(forKey: "LocationMapping")as! NSArray
                var locationName = ""
                for item in LocationMapping {
                    locationName = locationName + "\((item as AnyObject).value(forKey: "LocationName")!), "
                }
                if locationName.count != 0 {
                    locationName = String(locationName.dropLast())
                    cell.lbl_Location.text = "\(locationName)"

                }else{
                   cell.lbl_Location.text = ""
                }
                
                let imgName = "\(dict.value(forKey: "ProfileImage")!)"
                cell.imageManager.contentMode = .center
                cell.imageManager.setImageWith(URL(string: ProfileImageDownload + imgName), placeholderImage: UIImage(named: "add_photo"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    print(url ?? 0)
                    if(image != nil){
                        cell.imageManager.contentMode = .scaleAspectFill
                    }
                }, usingActivityIndicatorStyle: .gray)
                
                let value = "\(dict.value(forKey: "IsActive")!)"
                value == "True" ? cell.switchStatus.isOn = true : (cell.switchStatus.isOn = false)
              
            }
            return cell
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if(aryList.count != 0){
                
                
            }
        }
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
            customView.backgroundColor = UIColor.clear
            
            
            let lbl1 = UILabel(frame: CGRect(x: 0, y: tableView.frame.height / 2, width: tableView.frame.width, height: 30))
            let lbl2 = UILabel(frame: CGRect(x: 0, y: lbl1.frame.maxY, width: tableView.frame.width, height: 45))

          //  let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
            lbl1.text = strErrorMessage
            lbl1.textAlignment = .center
            lbl1.textColor = hexStringToUIColor(hex: primaryColor)
            lbl2.text = "Please add Manager"
            lbl2.textAlignment = .center
            lbl2.textColor = UIColor.lightGray
            lbl2.numberOfLines = 0
    //        button.setTitle("\(strErrorMessage)", for: .normal)
    //        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    //        button.setTitleColor(UIColor.lightGray, for: .normal)
    //        customView.addSubview(button)
               customView.addSubview(lbl1)
            customView.addSubview(lbl2)

            return customView
        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return strErrorMessage == "" ? 0 : tableView.frame.height
        }
        
        @objc func buttonAction(_ sender: UIButton!) {
            print("Button tapped")
        }
        @objc func switchChanged(mySwitch: UISwitch) {
            callActiveInActiveAPI(sender: mySwitch)
        }
        @objc func actionAssignLocation(sender: UIButton) {
          let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssignLocationClass") as? AssignLocationClass
            let dict = aryList.object(at: sender.tag)as! NSDictionary
            let LocationMapping = dict.value(forKey: "LocationMapping")as! NSArray
            vc?.arySelected = LocationMapping.mutableCopy() as! NSMutableArray
            vc?.dictManagerData = dict.mutableCopy() as! NSMutableDictionary
          self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
// MARK: -
// MARK: -ReminderCell
class ManagerCell: UITableViewCell {
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Location: UILabel!
    @IBOutlet weak var lbl_Mobile: UILabel!
    @IBOutlet weak var lbl_Email: UILabel!

    @IBOutlet weak var imageManager: UIImageView! {
          didSet {
              imageManager.layer.cornerRadius = 30
             imageManager.backgroundColor = UIColor.groupTableViewBackground
          }
      }
    @IBOutlet weak var switchStatus: UISwitch!
    @IBOutlet weak var btnAssign: UIButton!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
    // MARK: -
    // MARK: --API Calling

    extension ManagerListClass {
        func callgetManagerListAPI() {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("0", forKey: "DonorVId")
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "ManagerOwnerDonorVId")
        dictJson.setValue("", forKey: "Name")
        dictJson.setValue("", forKey: "Role")
            
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictJson) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetUserRegistration xmlns='\(BaseURL)'><RegistrationSearchMdl>\(jsonString)</RegistrationSearchMdl></GetUserRegistration></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetUserRegistrationResult", responcetype: "GetUserRegistrationResponse") { (responce, status) in
            self.loadingPlaceholderView.uncover(animated: false)
            self.refreshControl.endRefreshing()
            if status == "Suceess"{
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
               if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                self.aryList = NSMutableArray()
                        self.aryList =  NSMutableArray(array: (dictTemp.value(forKey: "DTList")as! NSArray).reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
                 self.strErrorMessage = ""
                deleteAllRecords(strEntity:"Manager")
                saveDataInLocalArray(strEntity: "Manager", strKey: "manager", data: (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray)
                
                }else{
                 self.strErrorMessage = apiError
               }
           }else{
                self.strErrorMessage = alertSomeError
           }
            self.numberOfRows =  self.aryList.count
            self.numberOfSections = 1
            self.tableView.reloadData()
       }
         }
        
        func callActiveInActiveAPI(sender : UISwitch) {
               if !isInternetAvailable() {
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                   
               }else{
                
                let status = sender.isOn == true ? "1" : "0"
                let tag = sender.tag
                   let dict = (self.aryList.object(at: tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                   let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ActiveInActiveUser xmlns='http://aahar.org.in/'><DonorVId>\(dict.value(forKey: "DonorVId")!)</DonorVId><IsActive>\(status)</IsActive></ActiveInActiveUser></soap12:Body></soap12:Envelope>"
                   let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
             
                
                   WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "ActiveInActiveUserResult", responcetype: "ActiveInActiveUserResponse") { (responce, status) in
                    loader.dismiss(animated: false) {
                        if status == "Suceess"{
                             let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                             let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                             
                            if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            
                             
                                dict.setValue("\(sender.isOn == true ? "True" : "False")", forKey: "IsActive")
                                self.aryList.replaceObject(at: tag, with: dict)
                                self.tableView.reloadData()
                             }else{
                                  CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                            }
                        }else{
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                            
                            
                        }
                    }
                    
                }
            }
               
               
               
           }
        
    }
