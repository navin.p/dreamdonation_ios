//
//  AddManagerClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/6/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class AddManagerClass: UIViewController {
    // MARK:
       // MARK:- IBOutlet
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!

    @IBOutlet weak var btnAddNow: TransitionButton!
    @IBOutlet weak var btnSelectLocation: UIButton!
    @IBOutlet weak var lblLocation: UILabel!

    var arySelectedLocation = NSMutableArray()
    var aryLocation = NSMutableArray()

    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI() 
    }
    // MARK:
       // MARK:- IBAction
       @IBAction func actionOnBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
           
       }
    @IBAction func actionOnSelectLocation(_ sender: UIButton) {
        if(aryLocation.count != 0){
           goOnSelectionView(tag: 3, ary: aryLocation)
        }else{
            self.callgetLocationAPI()
        }
        
        }
       @IBAction func actionOnAddNow(_ sender: TransitionButton) {
           self.view.endEditing(true)
           
           UIButton.animate(withDuration: 0.2,
                               animations: {
                                  sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
              }, completion: { finish in
                                  sender.transform = CGAffineTransform.identity
                                  
               
               if(self.validationForAddManager()){
                   // Call API
                   self.callAddManagerAPI()
               }
          })
       }
    // MARK:
      // MARK:- Extra Function
         func setUpUI() {
             ButtongradientLayer(btnArray: [btnAddNow])
            btnSelectLocation.layer.cornerRadius = 12.0
            
    }
    
    func goOnSelectionView(tag : Int , ary : NSMutableArray) {
            self.view.endEditing(true)
            let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
            vc.strTag = tag
            vc.aryList = ary
            vc.aryForSelectedListData = self.arySelectedLocation
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            self.present(vc, animated: false, completion: {})
        }
}

//MARK:-
//MARK:- PopUpDelegate
extension AddManagerClass : PopUpDelegate{
    func getDataFromPopupDelegateArray(aryData: NSArray, tag: Int) {
        
        for item in aryData {
            self.lblLocation.text =  self.lblLocation.text! + "\((item as AnyObject).value(forKey: "LocationName")!),"
        }
        if(self.lblLocation.text?.count != 0){
            self.lblLocation.text = String(self.lblLocation.text!.dropLast())
        }
    }
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
     
    }
}
         // MARK:
         // MARK:- Validation

         extension AddManagerClass {
             
             func validationForAddManager() -> Bool {
                 
                 if(txtName.text?.count == 0){
                     addErrorMessage(textView: txtName, message: alert_ManagerName)
                     return false
                 }
                 else if(txtEmail.text?.count == 0){
                    addErrorMessage(textView: txtEmail, message: alert_Email)
                    return false
                 }
                 else if(txtEmail.text!.count != 0){
                    if((txtEmail.text?.isValidEmailAddress())!){
                        return true
                    }
                    addErrorMessage(textView: txtEmail, message: alert_EmailValid)
                    return false
                }
                if((txtMobile.text!.count != 0)){
                    if(txtMobile.text!.count < 10){
                        addErrorMessage(textView: txtMobile, message: alert_MobileNumberValid)
                        return false
                    }
                }else if(txtUserName.text?.count == 0){
                   addErrorMessage(textView: txtUserName, message: alert_UserName)
                   return false
                }
//               else if(arySelectedLocation.count == 0){
//                 
//                  return false
//               }
//                
                
                
                
                 return true
             }
             func addErrorMessage(textView : ACFloatingTextfield, message : String) {
                 textView.shakeLineWithError = true
                 textView.showErrorWithText(errorText: message)
             }
             
         }

// MARK:
// MARK:- API Calling

extension AddManagerClass{

    func callAddManagerAPI() {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "ManagerOwnerDonorVId")
        
        dictJson.setValue("\(txtName.text!)", forKey: "Name")
        dictJson.setValue("", forKey: "MiddleName")
        dictJson.setValue("", forKey: "LastName")
        dictJson.setValue("\(txtMobile.text!)", forKey: "MobileNo")
        dictJson.setValue("\(txtEmail.text!)", forKey: "EmailId")
        dictJson.setValue("\(txtUserName.text!)", forKey: "UserName")
        dictJson.setValue("\(txtPassword.text!)", forKey: "OTP")
        let aryTemp = NSMutableArray()
        for item in arySelectedLocation {
                   let id = (item as AnyObject).value(forKey: "LocationId")
                   let dict = NSMutableDictionary()
                   dict.setValue(id, forKey: "LocationId")
                   aryTemp.add(dict)
               }
        dictJson.setValue(aryTemp, forKey: "LocationArray")

        
        var json = Data()
           var jsonString = NSString()
           if JSONSerialization.isValidJSONObject(dictJson) {
               // Serialize the dictionary
               json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
               jsonString = String(data: json, encoding: .utf8)! as NSString
               print("UpdateLeadinfo JSON: \(jsonString)")
           }
           
       let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddManager xmlns='\(BaseURL)'><DreamDonationRegistrationMdl>\(jsonString)</DreamDonationRegistrationMdl></AddManager></soap12:Body></soap12:Envelope>"
        self.btnAddNow.startAnimation()
       WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "AddManagerResult", responcetype: "AddManagerResponse") { (responce, status) in
           self.btnAddNow.stopAnimation()
           
           if status == "Suceess"{
               let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
               let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
               
               if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                  NotificationCenter.default.post(name: Notification.Name("Dream_ManagerADD"), object: nil)
                self.navigationController?.popViewController(animated: true)
               }else{
                   CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
               }
           }else{
               
               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
               
           }
       }
         }
     func callgetLocationAPI() {
     let dictJson = NSMutableDictionary()
     dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
     dictJson.setValue("0", forKey: "ManagerDonorVId")
     dictJson.setValue("0", forKey: "LocationId")
     dictJson.setValue("0", forKey: "LocationName")
     
     var json = Data()
     var jsonString = NSString()
     if JSONSerialization.isValidJSONObject(dictJson) {
         // Serialize the dictionary
         json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
         jsonString = String(data: json, encoding: .utf8)! as NSString
         print("UpdateLeadinfo JSON: \(jsonString)")
     }
     let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)

     let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocation xmlns='\(BaseURL)'><LocationSearchMdl>\(jsonString)</LocationSearchMdl></GetLocation></soap12:Body></soap12:Envelope>"
     WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetLocationResult", responcetype: "GetLocationResponse") { (responce, status) in
        loader.dismiss(animated: false, completion: nil)
         if status == "Suceess"{
             let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
            if("\(dictTemp.value(forKey: "Result")!)" == "True"){
             self.aryLocation = NSMutableArray()
             self.aryLocation = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                self.goOnSelectionView(tag: 3, ary: self.aryLocation)

             }else{
             CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
            }
        }else{
           CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
        }
         
    }
      }
}
