//
//  ManagerAssignLocationClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/16/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import TransitionButton
class ManagerAssignLocationClass: UIViewController {
    // MARK:
      // MARK:- IBOutlet
      
      @IBOutlet weak var viewHold: UIView!
      @IBOutlet private weak var tableView: UITableView! {
          didSet {
              tableView.coverableCellsIdentifiers = cellsIdentifiers
              tableView.tableFooterView = UIView()
              tableView.estimatedRowHeight = 200
          }
      }
      var aryManagerList = NSMutableArray() , aryAdminLocation = NSMutableArray()
      var strErrorMessage  = "List Empty!"
      var strComeFrom = ""

      private var numberOfSections = 0
      private var numberOfRows = 0
      private var loadingPlaceholderView = LoadingPlaceholderView(){
          didSet{
              do {
                  loadingPlaceholderView.gradientColor = .white
                  loadingPlaceholderView.backgroundColor = .white
              }
          }
      }
      var refreshControl = UIRefreshControl()
      
      private var cellsIdentifiers = [
          "LocationCell",
          "LocationCell","LocationCell",
          "LocationCell","LocationCell",
          "LocationCell"
      ]
      
        // MARK:
        // MARK:- Life Cycle
        override func viewDidLoad() {
            super.viewDidLoad()
            self.setUpUI()
            refreshControl.attributedTitle = NSAttributedString(string: "Get assigned location list...")
            refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)

            if(isInternetAvailable()){
                loadingPlaceholderView.cover(viewHold, animated: true)
              callgetAdminLocationAPI()
            }else{
                self.aryAdminLocation = NSMutableArray()
                self.strErrorMessage = alertInternet
                self.tableView.reloadData()
            }
            tableView.addSubview(refreshControl)

        }
        @objc func refresh(sender:AnyObject) {
          callgetAdminLocationAPI()
            
        }
        // MARK:
           // MARK:- Extra Function
           func setUpUI() {
//            let aryTemp = NSMutableArray()
//            for item in aryAdminLocation {
//                if(aryManagerList.contains(item)){
//                    aryTemp.add(item)
//                }
//                aryAdminLocation.remove(item)
//            }
//            aryTemp.addingObjects(from: aryAdminLocation as! [Any])
//            let aryFinal = aryTemp
//            aryAdminLocation = NSMutableArray()
//            aryAdminLocation = aryFinal
//            self.tableView.reloadData()
            
    }
    // MARK:
        // MARK:- IBAction
        
        @IBAction func actionOnBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    // MARK: -
    // MARK: --UITableViewDelegate

    extension ManagerAssignLocationClass : UITableViewDelegate, UITableViewDataSource{
        func numberOfSections(in tableView: UITableView) -> Int {
            return numberOfSections
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return numberOfRows
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView
                .dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath as IndexPath) as! LocationCell
            if(aryAdminLocation.count != 0){
                let dict = aryAdminLocation.object(at: indexPath.row)as! NSDictionary
                cell.lbl_Title.text = "\(dict.value(forKey: "LocationName")!)"
                cell.lbl_SubTitle.text = "\(dict.value(forKey: "Address")!)" == "" ? " " : "\(dict.value(forKey: "Address")!)"
                if(self.aryManagerList.contains(dict)){
                    cell.lbl_Assigned.text = "Assigned"
                    cell.contentView.alpha = 1.0
                }else{
                    cell.lbl_Assigned.text = ""
                    cell.contentView.alpha = 0.3
                }
            }
            return cell
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if(aryAdminLocation.count != 0){
                
                
            }
        }
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
            customView.backgroundColor = UIColor.clear
            
            
            let lbl1 = UILabel(frame: CGRect(x: 0, y: tableView.frame.height / 2, width: tableView.frame.width, height: 30))
            let lbl2 = UILabel(frame: CGRect(x: 0, y: lbl1.frame.maxY, width: tableView.frame.width, height: 45))

          //  let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
            lbl1.text = strErrorMessage
            lbl1.textAlignment = .center
            lbl1.textColor = hexStringToUIColor(hex: primaryColor)
            lbl2.text = ""
            lbl2.textAlignment = .center
            lbl2.textColor = UIColor.lightGray
            lbl2.numberOfLines = 0
    //        button.setTitle("\(strErrorMessage)", for: .normal)
    //        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    //        button.setTitleColor(UIColor.lightGray, for: .normal)
    //        customView.addSubview(button)
               customView.addSubview(lbl1)
            customView.addSubview(lbl2)

            return customView
        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return strErrorMessage == "" ? 0 : tableView.frame.height
        }
        
        @objc func buttonAction(_ sender: UIButton!) {
            print("Button tapped")
        }

    }
  

    // MARK: -
    // MARK: --API Calling

    extension ManagerAssignLocationClass {
        func callgetAdminLocationAPI() {
            let dictJson = NSMutableDictionary()
            dictJson.setValue("\(globleLogInData.value(forKey: "ManagerOwnerDonorVId")!)", forKey: "DonorVId")
            dictJson.setValue("0", forKey: "ManagerDonorVId")
            dictJson.setValue("0", forKey: "LocationId")
            dictJson.setValue("0", forKey: "LocationName")
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictJson) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocation xmlns='\(BaseURL)'><LocationSearchMdl>\(jsonString)</LocationSearchMdl></GetLocation></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetLocationResult", responcetype: "GetLocationResponse") { (responce, status) in
            self.loadingPlaceholderView.uncover(animated: true)
            self.refreshControl.endRefreshing()
            if status == "Suceess"{
                self.callgetManagerLocationAPI()
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
               if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                self.aryAdminLocation = NSMutableArray()
                self.aryAdminLocation = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                 self.strErrorMessage = ""
           
                }else{
                 self.strErrorMessage = apiError
               }
           }else{
                self.strErrorMessage = alertSomeError
           }
            self.numberOfRows =  self.aryAdminLocation.count
            self.numberOfSections = 1
            self.tableView.reloadData()
       }
         }
        func callgetManagerLocationAPI() {
                  let dictJson = NSMutableDictionary()
                  dictJson.setValue("0", forKey: "DonorVId")
                  dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "ManagerDonorVId")
                  dictJson.setValue("0", forKey: "LocationId")
                  dictJson.setValue("", forKey: "LocationName")
              var json = Data()
              var jsonString = NSString()
              if JSONSerialization.isValidJSONObject(dictJson) {
                  // Serialize the dictionary
                  json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
                  jsonString = String(data: json, encoding: .utf8)! as NSString
                  print("UpdateLeadinfo JSON: \(jsonString)")
              }
              
              let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocation xmlns='\(BaseURL)'><LocationSearchMdl>\(jsonString)</LocationSearchMdl></GetLocation></soap12:Body></soap12:Envelope>"
              WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetLocationResult", responcetype: "GetLocationResponse") { (responce, status) in
                  self.loadingPlaceholderView.uncover(animated: true)
                  self.refreshControl.endRefreshing()
                  if status == "Suceess"{
                      let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                      let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                      self.aryManagerList = NSMutableArray()

                     if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                      self.aryManagerList = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.setUpUI()
                          self.tableView.reloadData()
                      }else{
                        
                        
                     }
                 }else{
                 }
             }
        }
    }
