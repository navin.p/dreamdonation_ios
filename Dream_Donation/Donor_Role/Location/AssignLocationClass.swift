//
//  AssignLocationClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/13/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications
import LoadingPlaceholderView
import TransitionButton
class AssignLocationClass: UIViewController {
    
    
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var viewHold: UIView!
    @IBOutlet weak var btnAssign: TransitionButton!

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.coverableCellsIdentifiers = cellsIdentifiers
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    var aryList = NSMutableArray()
    var arySelected = NSMutableArray()
    var dictManagerData = NSMutableDictionary()

    var strErrorMessage  = "List Empty!"
    private var numberOfSections = 0
    private var numberOfRows = 0
    private var loadingPlaceholderView = LoadingPlaceholderView(){
        didSet{
            do {
                loadingPlaceholderView.gradientColor = .white
                loadingPlaceholderView.backgroundColor = .white
            }
        }
    }
    var refreshControl = UIRefreshControl()
    
    private var cellsIdentifiers = [
        "LocationCell",
        "LocationCell","LocationCell",
        "LocationCell","LocationCell",
        "LocationCell"
    ]
    
    // MARK:
           // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
              refreshControl.attributedTitle = NSAttributedString(string: "Get Location list...")
              refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
           
              if(isInternetAvailable()){
                  loadingPlaceholderView.cover(viewHold, animated: true)
                   callgetLocationAPI()
              }else{
                  self.aryList = NSMutableArray()
                  self.strErrorMessage = alertInternet
                  self.tableView.reloadData()
              }
              tableView.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
   
    
    // MARK:
       // MARK:- Extra Function
       func setUpUI() {
           ButtongradientLayer(btnArray: [btnAssign])
       }
    @objc func refresh(sender:AnyObject) {
           callgetLocationAPI()
           
       }
    // MARK:
         // MARK:- IBAction
         @IBAction func actionOnBack(_ sender: UIButton) {
             self.navigationController?.popViewController(animated: true)
         }
     @IBAction func actionOnAssign(_ sender: UIButton) {
      if !isInternetAvailable() {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                    
      }else{
        self.callAssignLocationAPI()
        }
          
      }
}
// MARK: -
// MARK: --UITableViewDelegate

extension AssignLocationClass : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath as IndexPath) as! LocationCell
        if(aryList.count != 0){
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            cell.lbl_Title.text = "\(dict.value(forKey: "LocationName")!)"
            cell.lbl_SubTitle.text = "Address: \(dict.value(forKey: "Address")!)"
            
           cell.accessoryType = .none
            for item in arySelected {
                if("\(dict.value(forKey: "LocationId")!)" == "\(((item as AnyObject)as! NSDictionary).value(forKey: "LocationId")!)"){
                     cell.accessoryType = .checkmark
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
         var countTag = 0
            for item in arySelected {
                if("\(dict.value(forKey: "LocationId")!)" == "\(((item as AnyObject)as! NSDictionary).value(forKey: "LocationId")!)"){
                    self.arySelected.remove(item)
                    countTag = 1
                    self.tableView.reloadData()
                }
            }
            if(countTag == 0){
                let dict1 = NSMutableDictionary()
                dict1.setValue("\(dict.value(forKey: "LocationId")!)", forKey: "LocationId")
                dict1.setValue("\(dict.value(forKey: "LocationName")!)", forKey: "LocationName")
                self.arySelected.add(dict1)
                self.tableView.reloadData()
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        customView.backgroundColor = UIColor.clear
        
        
        let lbl1 = UILabel(frame: CGRect(x: 0, y: tableView.frame.height / 2, width: tableView.frame.width, height: 30))
        let lbl2 = UILabel(frame: CGRect(x: 0, y: lbl1.frame.maxY, width: tableView.frame.width, height: 45))

      //  let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height - 100))
        lbl1.text = strErrorMessage
        lbl1.textAlignment = .center
        lbl1.textColor = hexStringToUIColor(hex: primaryColor)
        lbl2.text = "Please add loaction"
        lbl2.textAlignment = .center
        lbl2.textColor = UIColor.lightGray
        lbl2.numberOfLines = 0
//        button.setTitle("\(strErrorMessage)", for: .normal)
//        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//        button.setTitleColor(UIColor.lightGray, for: .normal)
//        customView.addSubview(button)
           customView.addSubview(lbl1)
        customView.addSubview(lbl2)

        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return strErrorMessage == "" ? 0 : tableView.frame.height
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }

}

// MARK: -
// MARK: --API Calling

extension AssignLocationClass  {
    func callgetLocationAPI() {
    let dictJson = NSMutableDictionary()
    dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
    dictJson.setValue("0", forKey: "ManagerDonorVId")
    dictJson.setValue("0", forKey: "LocationId")
    dictJson.setValue("0", forKey: "LocationName")
    
    var json = Data()
    var jsonString = NSString()
    if JSONSerialization.isValidJSONObject(dictJson) {
        // Serialize the dictionary
        json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
        jsonString = String(data: json, encoding: .utf8)! as NSString
        print("UpdateLeadinfo JSON: \(jsonString)")
    }
    
    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocation xmlns='\(BaseURL)'><LocationSearchMdl>\(jsonString)</LocationSearchMdl></GetLocation></soap12:Body></soap12:Envelope>"
    WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetLocationResult", responcetype: "GetLocationResponse") { (responce, status) in
        self.loadingPlaceholderView.uncover(animated: true)
        self.refreshControl.endRefreshing()
        if status == "Suceess"{
            let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
            
           if("\(dictTemp.value(forKey: "Result")!)" == "True"){
            self.aryList = NSMutableArray()
            self.aryList = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray
             self.strErrorMessage = ""
            }else{
             self.strErrorMessage = apiError
           }
       }else{
            self.strErrorMessage = alertSomeError
       }
        self.numberOfRows =  self.aryList.count
        self.numberOfSections = 1
        self.tableView.reloadData()
   }
     }
    
    func callAssignLocationAPI() {
          let aryTemp = NSMutableArray()
        for item in arySelected {
            let dictJson = NSMutableDictionary()
            dictJson.setValue("\(dictManagerData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
            dictJson.setValue("\((item as AnyObject).value(forKey: "LocationId")!)", forKey: "LocationId")
            dictJson.setValue("0", forKey: "LocationDonorMapId")
            aryTemp.add(dictJson)
        }
        
          var json = Data()
             var jsonString = NSString()
             if JSONSerialization.isValidJSONObject(aryTemp) {
                 // Serialize the dictionary
                 json = try! JSONSerialization.data(withJSONObject: aryTemp, options: .prettyPrinted)
                 jsonString = String(data: json, encoding: .utf8)! as NSString
                 print("UpdateLeadinfo JSON: \(jsonString)")
             }
             
         let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AttachManagerToLocation xmlns='\(BaseURL)'><LocationDonorMapMdl>\(jsonString)</LocationDonorMapMdl></AttachManagerToLocation></soap12:Body></soap12:Envelope>"
       let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
        
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "AttachManagerToLocationResult", responcetype: "AttachManagerToLocationResponse") { (responce, status) in
            loader.dismiss(animated: false) {
                
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 3)
                        NotificationCenter.default.post(name: Notification.Name("Dream_ManagerADD"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                    }
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                }
            }
        }
    }
    
    func callAddManagerAPI() {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "ManagerOwnerDonorVId")
          
        dictJson.setValue("\(dictManagerData.value(forKey: "Name")!)", forKey: "Name")
          dictJson.setValue("\(dictManagerData.value(forKey: "MiddleName")!)", forKey: "MiddleName")
          dictJson.setValue("\(dictManagerData.value(forKey: "LastName")!)", forKey: "LastName")
          dictJson.setValue("\(dictManagerData.value(forKey: "MobileNo")!)", forKey: "MobileNo")
          dictJson.setValue("\(dictManagerData.value(forKey: "EmailId")!)", forKey: "EmailId")
          dictJson.setValue("\(dictManagerData.value(forKey: "UserType")!)", forKey: "UserName")
          dictJson.setValue("\(dictManagerData.value(forKey: "OTP")!)", forKey: "OTP")
          let aryTemp = NSMutableArray()
        
          for item in arySelected {
                     let id = (item as AnyObject).value(forKey: "LocationId")
                     let dict = NSMutableDictionary()
                     dict.setValue(id, forKey: "LocationId")
                     aryTemp.add(dict)
                 }
          dictJson.setValue(aryTemp, forKey: "LocationArray")

          
          var json = Data()
             var jsonString = NSString()
             if JSONSerialization.isValidJSONObject(dictJson) {
                 // Serialize the dictionary
                 json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
                 jsonString = String(data: json, encoding: .utf8)! as NSString
                 print("UpdateLeadinfo JSON: \(jsonString)")
             }
             
         let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddManager xmlns='\(BaseURL)'><DreamDonationRegistrationMdl>\(jsonString)</DreamDonationRegistrationMdl></AddManager></soap12:Body></soap12:Envelope>"
          self.btnAssign.startAnimation()
         WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "AddManagerResult", responcetype: "AddManagerResponse") { (responce, status) in
             self.btnAssign.stopAnimation()
             
             if status == "Suceess"{
                 let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                 let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                 
                 if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    NotificationCenter.default.post(name: Notification.Name("Dream_ManagerADD"), object: nil)
                  self.navigationController?.popViewController(animated: true)
                 }else{
                     CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                 }
             }else{
                 
                 CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                 
             }
         }
           }
}
