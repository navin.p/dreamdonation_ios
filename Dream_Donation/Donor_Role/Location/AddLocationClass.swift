//
//  AddLocationClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/12/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications
import CoreLocation
import MapKit
import GooglePlacesSearchController
class AddLocationClass: UIViewController {
    
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var txtLocationName: ACFloatingTextfield!
    @IBOutlet weak var btnAddNow: TransitionButton!
    
    @IBOutlet weak var txtAddress: ACFloatingTextfield!

    // MARK: - ----------- Google Address Code ----------------
    // MARK: -
       @IBOutlet weak var scrollView: UIScrollView!
       @IBOutlet weak var tableView: UITableView!
       private var apiKey: String = GoogleMapsAPIServerKey
       private var placeType: PlaceType = .all
       private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
       private var radius: Double = 0.0
       private var strictBounds: Bool = false
       private var places = [Place]() {
           didSet { tableView.reloadData() }
       }
    
    var strLAt = "" ,strLong = "" , cityName = "" , strStateID = "", strCountryName = ""
  
  
    
    // MARK:
    // MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI() 
        // Do any additional setup after loading the view.
    }
   
    
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actionOnAddNow(_ sender: TransitionButton) {
        self.view.endEditing(true)
        
        UIButton.animate(withDuration: 0.2,
                            animations: {
                               sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
           }, completion: { finish in
                               sender.transform = CGAffineTransform.identity
                               
            
            if(self.validationForAddLocation()){
                // Call API
                self.callAddLocationAPI()
            }
       })
    }

    @IBAction func actionOnAddress(_ sender: UIButton) {
        self.view.endEditing(true)
        
   
        //let placesSearchController = GooglePlacesSearchController(delegate: self, apiKey: GoogleMapsAPIServerKey,placeType: .all )
       // present(placesSearchController, animated: true, completion: nil)
        
//        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
//        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        vc.modalTransitionStyle = .coverVertical
//        vc.delegate = self
//      self.present(vc, animated: true, completion: nil)
      
    }
    // MARK:
    // MARK:- Extra Function
       func setUpUI() {
           ButtongradientLayer(btnArray: [btnAddNow])
      
       }

}
// MARK:
// MARK:- Validation

extension AddLocationClass {
    
    func validationForAddLocation() -> Bool {
        
        if(txtLocationName.text?.count == 0){
            addErrorMessage(textView: txtLocationName, message: alert_LocationName)
            return false
        }
        else if(txtAddress.text?.count == 0){
          showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Address, viewcontrol: self)

            return false
        }
      else if(cityName == ""){
              showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_AddressProper, viewcontrol: self)
                      return false
                  }
        return true
    }
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}


//MARK:-
//MARK:- AddressFromMapScreenDelegate

extension AddLocationClass: AddressFromMapScreenDelegate {
    func GetAddressFromMapScreen(dictData: NSDictionary, tag: Int) {
             //   self.txtAddress.text = (dictData.value(forKey: "address")as! String)
    }
    
    
}
// MARK:
// MARK:- API Calling

extension AddLocationClass{

    func callAddLocationAPI() {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
        dictJson.setValue("0", forKey: "LocationId")
        dictJson.setValue("\(txtLocationName.text!)", forKey: "LocationName")
        dictJson.setValue("\(txtAddress.text!)", forKey: "Address")
        dictJson.setValue("\(strLAt)", forKey: "Lat")
        dictJson.setValue("\(strLong)", forKey: "Long")
        dictJson.setValue("\(strCountryID)", forKey: "country_id")
        dictJson.setValue("\(strStateID)", forKey: "state_id")
        dictJson.setValue("0", forKey: "city_id")
        dictJson.setValue(cityName, forKey: "city_name")

           var json = Data()
           var jsonString = NSString()
           if JSONSerialization.isValidJSONObject(dictJson) {
               // Serialize the dictionary
               json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
               jsonString = String(data: json, encoding: .utf8)! as NSString
               print("UpdateLeadinfo JSON: \(jsonString)")
           }
           
       let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddLocation xmlns='\(BaseURL)'><LocationMdl>\(jsonString)</LocationMdl></AddLocation></soap12:Body></soap12:Envelope>"
        self.btnAddNow.startAnimation()
       WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "AddLocationResult", responcetype: "AddLocationResponse") { (responce, status) in
           self.btnAddNow.stopAnimation()
           
           if status == "Suceess"{
               let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
               let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
               
               if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                  NotificationCenter.default.post(name: Notification.Name("Dream_LocationADD"), object: nil)
                self.navigationController?.popViewController(animated: true)
               }else{
                   CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
               }
           }else{
               
               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
               
           }
       }
         }
}
//extension AddLocationClass: GooglePlacesAutocompleteViewControllerDelegate {
//     func viewController(didAutocompleteWith place: PlaceDetails) {
//        self.dismiss(animated: true, completion: nil)
//          print(place.description)
//          placesSearchController.isActive = false
//            var FormattedAddress  = ""
//            if(place.name != nil){
//                FormattedAddress = place.name! + ","
//            }
//            if(place.streetNumber != nil){
//                FormattedAddress =  FormattedAddress + place.streetNumber! + " "
//            }
//            if(place.route != nil){
//                FormattedAddress =  FormattedAddress + place.route! + ", "
//            }
//            if(place.locality != nil){
//                FormattedAddress =  FormattedAddress + place.locality! + ", "
//            }
//            if(place.administrativeAreaCode != nil){
//                FormattedAddress =  FormattedAddress + place.administrativeAreaCode! + " "
//            }
//            if(place.postalCode != nil){
//                FormattedAddress =  FormattedAddress + place.postalCode! + ", "
//            }
//            if(place.country != nil){
//                FormattedAddress =  FormattedAddress + place.country!
//            }
//        
//        if let country = place.country {
//                        strCountryName = country
//
//            FormattedAddress = FormattedAddress + "\nCountry: \(country)"
//                  }
//                  if let state = place.administrativeArea {
//                    strStateName = state
//
//                      FormattedAddress = FormattedAddress + "\nState: \(state)"
//                  }
//                  if let town = place.locality {
//                    cityName = town
//                      FormattedAddress = FormattedAddress + "\nCity: \(town)"
//                  }
//        self.lblAddress.text = FormattedAddress
//        self.strLAt = "\(place.coordinate?.latitude ?? 0.0)"
//        self.strLong = "\(place.coordinate?.longitude ?? 0.0)"
//        self.stateID = getStateIDFromeJson(strName: strStateName)
//        if(self.stateID == "" || cityName == "" || strCountryName != "United States"){
//            self.lblAddress.text = ""
//            self.cityName = ""
//            self.stateID = "0"
//            self.strCountryName = ""
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLocationCountry, viewcontrol: self)
//            
//        }
//      }
//}

//MARK:-
//MARK:- ---------UITextFieldDelegate

extension AddLocationClass : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
         
         if(textField == txtAddress){
             self.scrollView.isScrollEnabled = false
         }
     }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if(textField == txtAddress){
            if (range.location == 0) && (string == " ")
            {
                return false
            }
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.tableView.removeFromSuperview()
                    return true
                }
            }
            var txtAfterUpdate:NSString = txtAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
               if(textField == txtAddress){

                    let txtAfterUpdate:NSString = txtAddress.text! as NSString
                    if(txtAfterUpdate == ""){
                        places = [];
                        return true
                    }
                    let parameters = getParameters(for: txtAfterUpdate as String)

                    return true
                }
        return true
    }
    
       func textFieldDidEndEditing(_ textField: UITextField) {
           if(textField == txtAddress){
               self.scrollView.isScrollEnabled = true
               self.tableView.removeFromSuperview()
           }
           
       }
       private func getParameters(for text: String) -> [String: String] {
             var params = [
                 "input": text,
                 "types": placeType.rawValue,
                 "key": apiKey
             ]
             
             if CLLocationCoordinate2DIsValid(coordinate) {
                 params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
                 
                 if radius > 0 {
                     params["radius"] = "\(radius)"
                 }
                 
                 if strictBounds {
                     params["strictbounds"] = "true"
                 }
             }
             
             return params
         }
         
}

extension AddLocationClass {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.tableView.removeFromSuperview()
                }
                else
                {
           
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    
                    self.tableView.frame = CGRect(x: self.txtAddress.frame.origin.x, y: 144, width: self.scrollView.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)

                    
                    
                    self.scrollView.addSubview(self.tableView)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension AddLocationClass : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            self.txtAddress.text = value.formattedAddress
            
            if let state = value.administrativeArea {
                self.strStateID = getStateIDFromeJson(strName: "\(state)")
            }
            if let town = value.locality {
                self.cityName  = town
            }
            self.strLAt = "\(value.coordinate?.latitude ?? 0)"
            self.strLong = "\(value.coordinate?.longitude ?? 0)"

            self.places = [];
            if("\(value.countryCode!)" != "US"){
                self.strLAt = "0"
                self.strLong = "0"
                self.strStateID = "0"
                self.txtAddress.text = ""
                self.cityName = ""
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLocationCountry, viewcontrol: self)
            }
        }
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
