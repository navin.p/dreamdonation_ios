//
//  CreatFeedBackVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import TransitionButton

class CreatFeedBackVC: UIViewController {
    @IBOutlet weak var btnSubmit: TransitionButton!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var txtFeedBack: KMPlaceholderTextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnSubmit.layer.cornerRadius = 15.0
        txtFeedBack.layer.borderColor = UIColor.darkGray.cgColor
        txtFeedBack.delegate = self
        lblCount.text =  "0/250"
        txtFeedBack.layer.cornerRadius = 10.0
        ButtongradientLayer(btnArray: [btnSubmit])
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononSubmit(_ sender: UIButton) {
        if(txtFeedBack.text.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter your feedback!", viewcontrol: self)
        }else{
            callAddFeedbackSuggesionAPI(sender: sender)
        }
        
    }
   
    //MARK:- --------------- API CAlling
    //MARK:

    func callAddFeedbackSuggesionAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            
        }
    }
}
extension CreatFeedBackVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        lblCount.text =  "\(newText.count)/250"
        return newText.count < 250
    }

}
