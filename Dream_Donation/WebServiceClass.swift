//
//  WebServiceClass.swift
//  Nimantran
//
//  Created by Navin Patidar on 2/12/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//


import UIKit
import Alamofire
import Alamofire
import SWXMLHash
import StringExtensionHTML
import AEXML

var BaseURL = "http://aahar.org.in/"
var MainURL = "\(BaseURL)DreamDonationWebService.asmx"
var MainURLForStateCity = "http://aahar.org.in/FoodDonationWebService.asmx"

var ProfileImageDownload = "\(BaseURL)ProfileImage/"
var BaseURLUploadProfile :String = "http://aahar.org.in/ProfileImageHandler.ashx"

var URL_aboutapp = "\(BaseURL)" + "aboutapp.aspx"
var URL_helpsupport = "\(BaseURL)" + "helpsupport.aspx"
var URL_termsofuse = "\(BaseURL)" + "termsofuse.aspx"
var URL_InviteManager = "\(BaseURL)" + "AddManager.aspx?donorvid="


class WebServiceClass: NSObject , NSURLConnectionDelegate,XMLParserDelegate {
       
      class func callSOAP_APIBY_POST(soapMessage:String,url:String,resultype:String , responcetype : String, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {

              let url1 = URL(string: url)
              var theRequest = URLRequest(url: url1!)
              let msgLength = soapMessage.count
              theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
              theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
              theRequest.httpMethod = "POST"
              theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
              request(theRequest)
                  .responseString { response in
                      switch(response.result) {
                      case .success(_):
                          if response.result.value != nil
                          {
                              if let xmlString = response.result.value {
                                  let xml = SWXMLHash.parse(xmlString)
                                  let body =  xml["soap:Envelope"]["soap:Body"]
                                  if let helpDeskElement = body[responcetype][resultype].element {
                                      let getResult = helpDeskElement.text
                                      let string = "\(getResult)"
                                      let data = string.data(using: .utf8)!
                                      do {
                                          if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                                          {
                                              let dictdata = NSMutableDictionary()
                                              dictdata.setValue(jsonArray, forKey: "data")
                                              OnResultBlock((dictdata) ,"Suceess")
                                              print(jsonArray) // use the json here
                                          } else {
                                              let dictdata = NSMutableDictionary()
                                              dictdata.setValue(dictdata, forKey: "data")
                                              OnResultBlock((dictdata) ,"failure")
                                          }
                                      } catch let error as NSError {
                                          let dictdata = NSMutableDictionary()
                                          dictdata.setValue(dictdata, forKey: "data")
                                          OnResultBlock((dictdata) ,"failure")
                                          print(error)
                                      }
                                  }
                              }
                          }
                          break
                          
                      case .failure(_):
                          print(response.result.error ?? 0)
                          let dic = NSMutableDictionary.init()
                          dic .setValue("Connection Time Out ", forKey: "message")
                          OnResultBlock(dic,"failure")
                          break
                      }
              }
      }
      
      
      class func callSOAP_APIBY_GETHELP(soapMessage:String,url:String,resultype:String , responcetype : String, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {

              let url1 = URL(string: url)
              var theRequest = URLRequest(url: url1!)
              let msgLength = soapMessage.count
              theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
              theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
              theRequest.httpMethod = "POST"
              theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
              request(theRequest)
                  .responseString { response in
                      switch(response.result) {
                      case .success(_):
                          if response.result.value != nil
                          {
                              if let xmlString = response.result.value {
                                  let xml = SWXMLHash.parse(xmlString)
                                  let body =  xml["soap:Envelope"]["soap:Body"]
                                  if let helpDeskElement = body["\(responcetype)"]["\(resultype)"].element {
                                      let getResult = helpDeskElement.text
                                      let dictdata = NSMutableDictionary()
                                      print(convertJsonStringToDictionary(text:getResult ) ?? 0)
                                   dictdata.setValue((convertJsonStringToDictionary(text:getResult ) ?? 0), forKey: "data")
                                      OnResultBlock((dictdata) ,"Suceess")
                                  }
                              }else{
                                  let dic = NSMutableDictionary.init()
                                  dic .setValue("Connection Time Out ", forKey: "message")
                                  OnResultBlock(dic,"failure")
                              }
                          }
                          break
                          
                      case .failure(_):
                          print(response.result.error as Any)
                          let dic = NSMutableDictionary.init()
                          dic .setValue("Connection Time Out ", forKey: "message")
                          OnResultBlock(dic,"failure")
                          break
                      }
              }
    
          
      }
      class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
          
          
          upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
              
              let imageData = image.jpegData(compressionQuality:0.5)
              multiPartFormData.append(imageData!, withName: withName, fileName: fileName, mimeType: "image/png")
              for (key, value) in parameter {
                  multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
              }
          }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
              switch (encodingResult){
              case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                  upload.responseJSON { response in
                      //                        print(response.request)  // original URL request
                      //                        print(response.response) // URL response
                      //                        print(response.data)     // server data
                      //                        print(response.result)   // result of response serialization
                      let statusCode = response.response?.statusCode
                      if statusCode == 200
                      {
                          let dic = NSMutableDictionary.init()
                          dic.setValue("\(response.result)", forKey: "message")
                          OnResultBlock(dic,"Suceess")
                      }else{
                          let dic = NSMutableDictionary.init()
                          dic.setValue("FAILURE", forKey: "message")
                          OnResultBlock(dic,"failure")
                      }
                  }
                  
              case .failure(let encodingError):
                  print(encodingError)
                  let dic = NSMutableDictionary.init()
                  dic.setValue("Connection Time Out", forKey: "message")
                  OnResultBlock(dic,"failure")
              }
          }
      }
      // MARK: - Convert String To Dictionary Function
      class func convertJsonStringToDictionary(text: String) -> [String: Any]? {
          if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
              do {
                  return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
              } catch {
                  print(error.localizedDescription)
              }
          }
          return nil
      }
      
}


