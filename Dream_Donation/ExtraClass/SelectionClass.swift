//
//  SelectionClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//


// tag 1,2  State , City
// 3 for location
// 4 ,5 unit , manager name
import UIKit
import TransitionButton

class SelectionClass: UIViewController {
    
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    open weak var delegate:PopUpDelegate?
    @IBOutlet weak var heightDone: NSLayoutConstraint!
    
    @IBOutlet weak var btnDone: TransitionButton!
    // MARK: - ----Variable
    
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var aryForSelectedListData = NSMutableArray()
    
    var strTitle = String()
    var strTag = Int()
    // MARK: - ----Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        aryForListData = aryList

        print(aryList)
        print(aryForSelectedListData)
        
        tvlist.tableFooterView = UIView()
        ButtongradientLayer(btnArray: [btnDone])
        if(strTag == 3){
            heightDone.constant = 44.0
        }else{
            heightDone.constant = 0.0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        searchBar.layer.cornerRadius = 10
        searchBar.tintColor = hexStringToUIColor(hex: primaryColor)
    }
    // MARK: - ----IBAction
    
    @IBAction func actionOnBack(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func actionOnSave(_ sender: Any) {
        self.view.endEditing(true)
        self.delegate?.getDataFromPopupDelegateArray(aryData: aryForSelectedListData, tag: self.strTag)
        self.dismiss(animated: false) {}
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SelectionClass : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath as IndexPath) as! SelectionCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        
        if(aryForSelectedListData.contains(dict)){
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
            
        }
        
        if(self.strTag == 1){
            cell.lblTitle.text = "\(dict.value(forKey: "state_name")!)"
        }else if(self.strTag == 2){
            cell.lblTitle.text = "\(dict.value(forKey: "city_name")!)"
        }else if(self.strTag == 3){
            cell.lblTitle.text = "\(dict.value(forKey: "LocationName")!)"
        }else if(self.strTag == 4){
            cell.lblTitle.text = "\(dict.value(forKey: "Unit")!)"
        }
            //Contct Information
        else if(self.strTag == 5){
            let strMobile = "\(dict.value(forKey: "MobileNo")!)"
            
            if("\(dict.value(forKey: "DonorVId")!)" == "\(globleLogInData.value(forKey: "DonorVId")!)"){
                
                strMobile != "" ?  cell.lblTitle.text = "\(dict.value(forKey: "Name")!) - \(strMobile)  (Self)" : (cell.lblTitle.text = "\(dict.value(forKey: "Name")!) (Self)")
                
            }else{
                strMobile != "" ?  cell.lblTitle.text = "\(dict.value(forKey: "Name")!) - \(strMobile)" : (cell.lblTitle.text = "\(dict.value(forKey: "Name")!)")
                
            }
        }
            //Address Information
            
        else if(self.strTag == 6){
             cell.accessoryType = .none
            cell.lblTitle.text = "\(dict.value(forKey: "LocationName")!)\nAddress: \(dict.value(forKey: "Address")!)"
            if(aryForSelectedListData.contains(dict)){
                cell.contentView.alpha = 1.0
            }else{
                cell.contentView.alpha = 0.4
                
            }
            
        }
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let dict = aryList.object(at: indexPath.row)as? NSDictionary
        
        if(strTag == 3){
            if(aryForSelectedListData.contains(dict!)){
                aryForSelectedListData.remove(dict!)
            }else{
                aryForSelectedListData.add(dict!)
            }
            self.tvlist.reloadData()
        }
            else if(strTag == 5){
            
            let active =  "\(dict!.value(forKey: "IsActive")!)"
            let lMapping =  ((dict!.value(forKey: "LocationMapping")!)as! NSArray).mutableCopy()as! NSMutableArray

            if(active != "True" || lMapping.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_AssignedManagerMessage, viewcontrol: self)
            }
            else{
                self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            self.tvlist.reloadData()
        }
        else if(strTag == 6){
            if(aryForSelectedListData.contains(dict!)){
                self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
            }else{
                showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alert_LocationAssignedMessage, viewcontrol: self)
            }
            self.tvlist.reloadData()
        }
            
        else{
            self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
            self.dismiss(animated: false) {}
        }
        
    }
    
    
}
// MARK: - ----------------SlectionCell
// MARK: -
class SelectionCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  SelectionClass : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        let resultPredicate = NSPredicate(format: "state_name contains[c] %@ OR city_name contains[c] %@ OR Unit contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@ OR EventType contains[c] %@", argumentArray: [Searching, Searching , Searching , Searching, Searching , Searching , Searching , Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}

//MARK: -------Protocol

protocol PopUpDelegate : class{
    func getDataFromPopupDelegate(dictData : NSDictionary ,tag : Int)
    func getDataFromPopupDelegateArray(aryData : NSArray ,tag : Int)
    
}
