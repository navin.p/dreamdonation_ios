//
//  ConstantClass.swift
//  AaharFoeUS
//
//  Created by Navin Patidar on 2/5/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//
import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration
import AVKit
import TransitionButton


//MARK: Common use in app
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()
var globleLogInData = NSMutableDictionary()
var globleRoleUser = ""
var globleSubRoleUser = ""
var GoogleMapsAPIServerKey = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"

var primaryG1 = "15cb5a"
var primaryG2 = "1086ca"
var primaryG3 = "13aa8f"

var whiteColor = "F9FFFF"
var primaryColor = "1086ca"


var DarkprimaryG1 = "3A3A3D"
var DarkprimaryG2 = "1C1C1E"



var strLat = "0.0"
var strLong = "0.0"
var strCountryID = 2

var app_Name = "Dream Donation"

var app_Version : String = "1.0.0"
var app_VersionDate : String = ""
var app_VersionSupport : String = "Requires iOS 11.0 or later & Compatible with iPhone."
var application_ID : String = "https://itunes.apple.com/in/app/aahar-daan/id1164477317?mt=8"
var Platform = "iPhone"
var dateFormate = "yyyy-MM-dd'T'HH:mm:ss.SSS"
var dateFormate1 = "yyyy-MM-dd'T'HH:mm:ss.SS"
var dateFormate2 = "yyyy-MM-dd'T'HH:mm:ss"


var onlydateFormate = "yyyy-MM-dd"
var onlyTimeFormate = "HH:mm:ss.SSS"


var alertMessage = "Alert!"
var alertMsg = "Alert"
var alertInfo = "Information!"
var alertPhotoSource = "Photo Source"
var alertChooseSource = "Choose A Source"
var alertPhotoLibrary = "Photo Library"
var alertCamera = "Camera"



var alertInternet = "No Internet Connection, try later!"
var alertDataNotFound = "Sorry , Data is not Available!"
var alertSomeError = "Somthing went wrong please try again!"
var alertCalling = "Your device doesn't support this feature."
var alertLogout = "Are you sure want to logout ?"
var alert_FirstName = "First name is required!"
var alert_UserName = "User name is required!"
var alert_required = "required!"
var alert_ManagerName = "Manager name is required!"
var alert_LocationName = "Location name is required!"

var alertDelete = "Are you sure want to delete ?"
var alertEventRemove = "Are you sure want to remove event ?"

var alert_EmailValid = "Email address is invalid!"
var alert_Email = "Email address is required!"
var alert_MobileNumber = "Mobile number is required!"
var alert_MobileNumberValid = "Mobile number is invalid!"

var alert_Password = "Password is required!"
var alert_CPassword = "Confirm password is required!"
var alert_Password_CPassword = "Confirm password is required!"
var alert_OldPassword = "Old password is required!"
var alert_NewPassword = "New password is required!"
var alert_PasswordNotMatch = "Password not match!"
var alert_operation = "Hours of operation should be minimum 24 hrs!"


var alert_Terms = "Please accept terms and condition."

var alert_State = "State name is required!"
var alert_City = "City name is required!"
var alert_Address = "Address is required!"
var alert_AddressProper = "Please enter proper address (address, city, zipcode, state)."

var alert_FromTime = "From time is required!"
var alert_ToTime = "To time is required!"


var alert_LocationAssignedMessage = "This location is not assigned. Please select another location or contact to admin."
var alertLocationCountry = "Please select another location or contact to admin."
var alert_LocationAssignedManager = "Location is not assigned. Please select another manager."
var alert_AssignedManagerMessage = "This contact person is not active or location not assigned .Please select another contact info or contact to admin."
//MARK:
//MARK: Status
struct Status{
    static let Donate = "Donated"
    static let Booked = "Booked"
  
}
//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
}


//MARK:
//MARK: Color

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


//MARK:
//MARK: GradientLayer
func getgradientLayerImage(bounds : CGRect ) -> UIImage {
    let gradientLayer = CAGradientLayer()
    
    gradientLayer.frame = bounds
    gradientLayer.colors = [hexStringToUIColor(hex: primaryG1).cgColor, hexStringToUIColor(hex: primaryG2).cgColor] // start color and end color
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0) // Horizontal gradient start
    gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0) // Horizontal gradient end
    UIGraphicsBeginImageContext(gradientLayer.bounds.size)
    gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
    
    
}
func getgradientLayer(bounds : CGRect) -> CAGradientLayer {
    
    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = bounds
    gradientLayer.colors = [hexStringToUIColor(hex: primaryG1).cgColor, hexStringToUIColor(hex: primaryG3).cgColor]
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
    gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
    gradientLayer.locations = [0.0, 1.0]
    return gradientLayer
}

@IBDesignable
class GradientView: UIView {
    @IBInspectable var startColor:   UIColor = hexStringToUIColor(hex: primaryG1)
        
        { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = hexStringToUIColor(hex: primaryG2)
        { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override public class var layerClass: AnyClass { CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}



//MARK:
//MARK: isInternetAvailable
func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}
func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}


extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}
extension ACFloatingTextfield {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    @IBInspectable var borderWidth: Float = 0.0
    @IBInspectable var borderColor: UIColor = UIColor.gray
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = CGFloat(borderWidth)
    }
    
}
@IBDesignable
class CircleImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    @IBInspectable var borderWidth: Float = 0.0
    @IBInspectable var borderColor: UIColor = UIColor.gray
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = CGFloat(borderWidth)
    }
    
}

func ButtongradientLayer(btnArray:[TransitionButton]) {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        for btn in btnArray {
            // btn.titleLabel?.font = ButtonMainFont
            for item in btn.layer.sublayers! {
                if item is CAGradientLayer {
                    item.removeFromSuperlayer()
                }
            }
            btn.layer.insertSublayer(getgradientLayer(bounds: btn.bounds), at: 0)
        }
        
    }
    
    
}

extension String {
    
    func isValidEmailAddress() -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}"
            + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
            + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
            + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
            + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
            + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
            + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}

func getStateIDFromeJson(strName : String) -> String {
    var stateID = ""
    let statearray = getDataFromCoreDataBase(strEntity: "State", strkey: "state")
    for item in statearray {
        if(strName == "\(((item as AnyObject)as! NSDictionary).value(forKey: "state_name")!)"){
            stateID = "\(((item as AnyObject)as! NSDictionary).value(forKey: "state_id")!)"
        }
    }
    return stateID
}


func getJson(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func convertToDictionary(from text: String) throws -> NSArray {
    guard let data = text.data(using: .utf8) else { return NSArray() }
    let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
    return anyResult as? NSArray ?? NSArray()
}

func getLogInData(key : String) -> NSDictionary {
    if(nsud.value(forKey: key) != nil){
        return nsud.value(forKey: key)as! NSDictionary
    }
    return NSDictionary()
}
func getUniqueString()-> String{
    var strName = "\(Date()).jpg".replacingOccurrences(of: "-", with: "")
    strName = strName.replacingOccurrences(of: " ", with: "")
    strName = strName.replacingOccurrences(of: "+", with: "")
    return  strName.replacingOccurrences(of: ":", with: "")
}


func callingFunction(number : NSString) -> Bool{
    let str = number.replacingOccurrences(of: " ", with:"")
    
    if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    else {
        return false
    }
}


func loader_Show(controller: UIViewController , strMessage : String , title : String ,style : UIAlertController.Style) -> UIAlertController {
    let alert = UIAlertController(title: title, message:strMessage, preferredStyle: style)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    if #available(iOS 13.0, *) {
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
    } else {
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
    }
    loadingIndicator.startAnimating();
    alert.view.addSubview(loadingIndicator)
    
    controller.present(alert, animated: false, completion: nil)
    return alert
}


extension NSMutableDictionary {
    
    
    func removeNullFromDict () -> NSMutableDictionary
    {
        let dic = self;
        
        for (key, value) in self {
            
            let val : NSObject = value as! NSObject;
            if(val.isEqual(NSNull()))
            {
                dic.setValue("", forKey: (key as? String)!)
            }
           
            else
            {
                dic.setValue(value, forKey: key as! String)
            }
            
        }
        
        return dic;
    }
}
extension NSDictionary {
    
    
    func nullKeyRemoval() -> NSDictionary {
        var dict = (self as! Dictionary<String,Any>)
        
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
            ((dict as NSDictionary).mutableCopy()as! NSMutableDictionary).setValue("", forKey: key)
        }
        
        return dict as NSDictionary
    }
}
func showLocationAccessAlertAction(viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
    alert.addAction(UIAlertAction (title: "Go to Setting", style: .default, handler: { (nil) in
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }))
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}





func getDateTime(FormateDate: String , FormateTime : String , strdate: String) -> String {
    let strSendDate = ""
    let dateFormatter = DateFormatter()
    let strDateTimeString = strdate
    //    if(strdate.split(separator: ".").count != 0){
    //        strDateTimeString = String(strdate.split(separator: ".")[0])
    //    }
    
    dateFormatter.dateFormat = "\(dateFormate)"
    if(checkFormaet(formatter: dateFormatter, strDate: strDateTimeString)){
        return getFinalDate(formate: dateFormate, FormateDate: FormateDate, FormateTime: FormateTime, strdate: strDateTimeString)
    }
    
    dateFormatter.dateFormat = "\(dateFormate1)"
    if(checkFormaet(formatter: dateFormatter, strDate: strDateTimeString)){
        return getFinalDate(formate: dateFormate1, FormateDate: FormateDate, FormateTime: FormateTime, strdate: strDateTimeString)
        
    }
    dateFormatter.dateFormat = "\(dateFormate2)"
    if(checkFormaet(formatter: dateFormatter, strDate: strDateTimeString)){
        return getFinalDate(formate: dateFormate2, FormateDate: FormateDate, FormateTime: FormateTime, strdate: strDateTimeString)
    }
    return strSendDate
}
func getFinalDate(formate : String , FormateDate : String, FormateTime : String , strdate: String) -> String{
    let dateFormatter = DateFormatter()
    var strSendDate = ""
    dateFormatter.dateFormat = formate
    let date = dateFormatter.date(from:strdate)!
    dateFormatter.dateFormat = FormateDate
    strSendDate = "\(dateFormatter.string(from: date))"
    dateFormatter.dateFormat = FormateTime
    strSendDate = strSendDate + "," + "\(dateFormatter.string(from: date))"
    return strSendDate
    
}
func checkFormaet(formatter : DateFormatter , strDate : String)-> Bool{
    guard let _ = formatter.date(from: strDate)else {
        
        return false
    }
    return true
    
}

//MARK:
//MARK: CoreData
func saveDataInLocalArray(strEntity: String , strKey : String , data : NSMutableArray)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}
func saveDataInLocalDictionary(strEntity: String , strKey : String , data : NSMutableDictionary)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}
func getDataFromCoreDataBase(strEntity: String ,strkey : String )-> NSMutableArray   {
    let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)
    
    let aryList = NSMutableArray()
    if aryTemp.count > 0 {
        for j in 0 ..< aryTemp.count {
            var obj = NSManagedObject()
            obj = aryTemp[j] as! NSManagedObject
            aryList.add(obj.value(forKey: strkey) ?? 0)
        }
    }
    if aryList.count !=  0{
        return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
        
    }else{
        return NSMutableArray()
    }
}
func getDataFromLocal(strEntity: String , strkey : String )-> NSArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func deleteAllRecords(strEntity: String ) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let context = delegate.persistentContainer.viewContext
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}

func txtFiledValidation(textField : UITextField , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let decimelnumberOnly = NSCharacterSet.init(charactersIn: "0123456789.")
    let strValidStr_Digit = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    let characterOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz")
    
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValidnumber = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strcharacterOnly = characterOnly.isSuperset(of: stringFromTextField as CharacterSet)
    
    let strValidDecimal = decimelnumberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDigitCharacter = strValidStr_Digit.isSuperset(of: stringFromTextField as CharacterSet)
    
    if returnOnly == "NUMBER" {
        if strValidnumber == false{
            return false
        }
    }
    if returnOnly == "CHAR_DIGIT" {
        if strValidDigitCharacter == false{
            return false
        }
    }
    
    if returnOnly == "DECIMEL" {
        if strValidDecimal == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strcharacterOnly == false{
            return false
        }
    }
    
    
    
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        if(returnOnly == "DECIMEL"){
            if(string == "."){
                if(textField.text!.contains(".")){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    if (string == ".") {
        return false;
    }
    return true;
}
func ACFloatingTextValidation(textField : ACFloatingTextfield , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let decimelnumberOnly = NSCharacterSet.init(charactersIn: "0123456789.")
    let strValidStr_Digit = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    let characterOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz")
    
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValidnumber = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strcharacterOnly = characterOnly.isSuperset(of: stringFromTextField as CharacterSet)
    
    let strValidDecimal = decimelnumberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDigitCharacter = strValidStr_Digit.isSuperset(of: stringFromTextField as CharacterSet)
    
    if returnOnly == "NUMBER" {
        if strValidnumber == false{
            return false
        }
    }
    if returnOnly == "CHAR_DIGIT" {
        if strValidDigitCharacter == false{
            return false
        }
    }
    
    if returnOnly == "DECIMEL" {
        if strValidDecimal == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strcharacterOnly == false{
            return false
        }
    }
    
    
    
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        if(returnOnly == "DECIMEL"){
            if(string == "."){
                if(textField.text!.contains(".")){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    if (string == ".") {
        return false;
    }
    return true;
}
func formattedNumber(number: String) -> String {
    let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    let mask = "+X (XXX) XXX-XXXX"
    
    var result = ""
    var index = cleanPhoneNumber.startIndex
    for ch in mask where index < cleanPhoneNumber.endIndex {
        if ch == "X" {
            result.append(cleanPhoneNumber[index])
            index = cleanPhoneNumber.index(after: index)
        } else {
            result.append(ch)
        }
    }
    return result
}


func removeUSformate(number: String) -> String {
    var str = ""
    str = number.replacingOccurrences(of: "-", with: "")
    str = str.replacingOccurrences(of: "(", with: "")
    str = str.replacingOccurrences(of: ")", with: "")
    str = str.replacingOccurrences(of: "+", with: "")
    str = str.replacingOccurrences(of: " ", with: "")
    
    
    return str
}


//------For Address
public enum PlaceType: String {
    case all = ""
    case geocode
    case address
    case establishment
    case regions = "(regions)"
    case cities = "(cities)"
}

open class Place: NSObject {
    public let id: String
    public let mainAddress: String
    public let secondaryAddress: String
    
    override open var description: String {
        get { return "\(mainAddress), \(secondaryAddress)" }
    }
    
    public init(id: String, mainAddress: String, secondaryAddress: String) {
        self.id = id
        self.mainAddress = mainAddress
        self.secondaryAddress = secondaryAddress
    }
    
    convenience public init(prediction: [String: Any]) {
        let structuredFormatting = prediction["structured_formatting"] as? [String: Any]
        
        self.init(
            id: prediction["place_id"] as? String ?? "",
            mainAddress: structuredFormatting?["main_text"] as? String ?? "",
            secondaryAddress: structuredFormatting?["secondary_text"] as? String ?? ""
        )
    }
}

open class PlaceDetails: CustomStringConvertible {
    public let formattedAddress: String
    open var name: String? = nil
    
    open var streetNumber: String? = nil
    open var route: String? = nil
    open var postalCode: String? = nil
    open var country: String? = nil
    open var countryCode: String? = nil
    
    open var locality: String? = nil
    open var subLocality: String? = nil
    open var administrativeArea: String? = nil
    open var administrativeAreaCode: String? = nil
    open var subAdministrativeArea: String? = nil
    
    open var coordinate: CLLocationCoordinate2D? = nil
    
    init?(json: [String: Any]) {
        guard let result = json["result"] as? [String: Any],
            let formattedAddress = result["formatted_address"] as? String
            else { return nil }
        
        self.formattedAddress = formattedAddress
        self.name = result["name"] as? String
        
        if let addressComponents = result["address_components"] as? [[String: Any]] {
            streetNumber = get("street_number", from: addressComponents, ofType: .short)
            route = get("route", from: addressComponents, ofType: .short)
            postalCode = get("postal_code", from: addressComponents, ofType: .long)
            country = get("country", from: addressComponents, ofType: .long)
            countryCode = get("country", from: addressComponents, ofType: .short)
            
            locality = get("locality", from: addressComponents, ofType: .long)
            subLocality = get("sublocality", from: addressComponents, ofType: .long)
            administrativeArea = get("administrative_area_level_1", from: addressComponents, ofType: .long)
            administrativeAreaCode = get("administrative_area_level_1", from: addressComponents, ofType: .short)
            subAdministrativeArea = get("administrative_area_level_2", from: addressComponents, ofType: .long)
        }
        
        if let geometry = result["geometry"] as? [String: Any],
            let location = geometry["location"] as? [String: Any],
            let latitude = location["lat"] as? CLLocationDegrees,
            let longitude = location["lng"] as? CLLocationDegrees {
            coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
    
    open var description: String {
        return "\nAddress: \(formattedAddress)\ncoordinate: (\(coordinate?.latitude ?? 0), \(coordinate?.longitude ?? 0))\n"
    }
}

private extension PlaceDetails {
    
    enum ComponentType: String {
        case short = "short_name"
        case long = "long_name"
    }
    func get(_ component: String, from array: [[String: Any]], ofType: ComponentType) -> String? {
        return (array.first { ($0["types"] as? [String])?.contains(component) == true })?[ofType.rawValue] as? String
    }
}
func getAddress(from dataString: String) ->  NSMutableDictionary {
    let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
    let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
    
    let resultsArray = NSMutableArray()
    // put matches into array of Strings
    for match in matches {
        if match.resultType == .address,
            let components = match.addressComponents {
            resultsArray.add(components)
        } else {
            print("no components found")
        }
    }
    
    let tempArrayOfKeys = NSMutableArray()
    let tempDictData = NSMutableDictionary()
    
    for k in 0 ..< resultsArray.count {
        
        if resultsArray[k] is NSDictionary {
            
            let tempDict = resultsArray[k] as! NSDictionary
            
            tempArrayOfKeys.addObjects(from: tempDict.allKeys)
            tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])
            
        }
        
    }
    var strCityName = "" , strZipCode = "" , strStateId = "" , strAddressLine1 = ""
    
    if tempArrayOfKeys.contains("City") {
        
        strCityName = "\(tempDictData.value(forKey: "City")!)"
        
    }
    if tempArrayOfKeys.contains("ZIP") {
        
        strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"
        
    }
    if tempArrayOfKeys.contains("State") {
        strStateId = getStateIDFromeJson(strName: "\(tempDictData.value(forKey: "State")!)")
    }
    if tempArrayOfKeys.contains("Street") {
        
        strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"
        
    }
    
    let dictAdrres = NSMutableDictionary()
    dictAdrres.setValue(strCityName, forKey: "CityName")
    dictAdrres.setValue(strZipCode, forKey: "ZipCode")
    dictAdrres.setValue(strStateId, forKey: "StateId")
    dictAdrres.setValue(strAddressLine1, forKey: "AddressLine1")
    
    return dictAdrres
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
