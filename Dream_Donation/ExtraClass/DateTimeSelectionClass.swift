//
//  DateTimeSelectionClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/3/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class DateTimeSelectionClass: UIViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!

    // MARK: - ----Variable
    open weak var delegateDateTime:DateTimeDelegate?
    var strMode = String()
    var strTag = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
       // datePicker.minimumDate = Date()
        if(strMode == "Time"){
            datePicker.datePickerMode = UIDatePicker.Mode.time
        }else if(strMode == "Date"){
            datePicker.datePickerMode = UIDatePicker.Mode.date
        }else if(strMode == "DateTime"){
            datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        }
       
    }
    

    // MARK: - ----IBAction
    
    @IBAction func actionOnDone(_ sender: Any) {
        
        delegateDateTime?.getDataFromDelegate(time: datePicker.date, tag: strTag)
        
        self.view.endEditing(true)
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func actionOnClose(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
}
//MARK: -------Protocol

protocol DateTimeDelegate : class{
    func getDataFromDelegate(time : Date ,tag : Int)
}
