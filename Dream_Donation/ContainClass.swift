//
//  ContainClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 5/1/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class ContainClass: UIViewController {
    // MARK: -
       // MARK: - IBOutlet
       @IBOutlet weak var txtContain: UITextView!
          @IBOutlet weak var lblTitle: UILabel!
         var dictContainData = NSMutableDictionary()
    
    // MARK: -
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let htmlContain = "\(dictContainData.value(forKey: "HtmlContent")!)"
        lblTitle.text = "\(dictContainData.value(forKey: "ContentTitle")!)"
       // txtContain.attributedText = htmlContain.htmlToAttributedString
        txtContain.attributedText = htmlContain.convertHtmlToAttributedStringWithCSS(font: UIFont(name: "\(String(describing: txtContain.font?.familyName))", size: 22), csscolor: "black", lineheight: 5, csstextalign: "center")

    }
    // MARK:
            // MARK:- IBAction
     @IBAction func actionOnBack(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
            
        }



}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
        
        
    }
}
extension String {
    private var convertHtmlToNSAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data,options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public func convertHtmlToAttributedStringWithCSS(font: UIFont? , csscolor: String , lineheight: Int, csstextalign: String) -> NSAttributedString? {
        guard let font = font else {
            return convertHtmlToNSAttributedString
        }
        let modifiedString = "<style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px; color: \(csscolor); line-height: \(lineheight)px; text-align: \(csstextalign); }</style>\(self)";
        guard let data = modifiedString.data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error)
            return nil
        }
    }
}
