//
//  ChangePasswordClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/20/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//
import UIKit
import TransitionButton
import CRNotifications

class ChangePasswordClass: UIViewController {
  
    // MARK:
     // MARK:- IBOutlet
      @IBOutlet weak var txtOldPass: ACFloatingTextfield!
      @IBOutlet weak var txtNewPass: ACFloatingTextfield!
      @IBOutlet weak var txtConfirmNewPass: ACFloatingTextfield!

      @IBOutlet weak var btnSubmit: TransitionButton!
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    
    // MARK:
        // MARK:- Extra Function
        func setUpUI() {
            ButtongradientLayer(btnArray: [btnSubmit])
        }
    // MARK:
     // MARK:- IBAction
     @IBAction func actionOnSubmit(_ sender: TransitionButton) {
         self.view.endEditing(true)
         UIButton.animate(withDuration: 0.2,
                             animations: {
                                sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
            }, completion: { finish in
                                sender.transform = CGAffineTransform.identity
               if !(self.validationForChanePassword()){
                   if(isInternetAvailable()){
                     CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
                   }else{
                       self.callChangePasswordAPI()
                   }
               }
            })
     }
     @IBAction func actionOnBack(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
          
      }
}
// MARK:
// MARK:- Validation

extension ChangePasswordClass {
    func validationForChanePassword() -> Bool {
        if(txtOldPass.text?.count == 0){
            addErrorMessage(textView: txtOldPass, message: alert_OldPassword)
            return false
        }else if(txtNewPass.text!.count == 0){
            addErrorMessage(textView: txtNewPass, message: alert_NewPassword)
            return false
        }else if(txtConfirmNewPass.text!.count == 0){
            addErrorMessage(textView: txtConfirmNewPass, message: alert_Password_CPassword)
            return false
        }else if(txtNewPass.text! != txtConfirmNewPass.text!){
            addErrorMessage(textView: txtConfirmNewPass, message: alert_PasswordNotMatch)
            return false
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension ChangePasswordClass : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
// MARK:
// MARK:- API Calling

extension ChangePasswordClass{
    func callChangePasswordAPI() {
  
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ChangePassword xmlns='\(BaseURL)'><DonorVId>\(globleLogInData.value(forKey: "DonorVId")!)</DonorVId><OldPassword>\(txtOldPass.text!)</OldPassword><NewPassword>\(txtNewPass.text!)</NewPassword></ChangePassword></soap12:Body></soap12:Envelope>"
           self.btnSubmit.startAnimation()
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "ChangePasswordResult", responcetype: "ChangePasswordResponse") { (responce, status) in
                self.btnSubmit.stopAnimation()
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                           
                           alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            
                            self.navigationController?.popViewController(animated: true)
                           }))
                           self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: "\(dictTemp.value(forKey: "Msg")!)", dismissDelay: 2)
                    }
                }else{
                    
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 2)
                }
            }
        }
}
