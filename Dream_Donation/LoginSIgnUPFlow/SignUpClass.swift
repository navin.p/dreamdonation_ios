//
//  SignUpClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/6/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications
import CoreLocation
import MapKit
import GooglePlacesSearchController
import IQKeyboardManager

class SignUpClass: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var txtFirstName: ACFloatingTextfield!
    @IBOutlet weak var txtLastName: ACFloatingTextfield!
    @IBOutlet weak var txtMiddleName: ACFloatingTextfield!
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: ACFloatingTextfield!

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSignUP: TransitionButton!
    @IBOutlet weak var btnBGProfile: TransitionButton!
    var imagePicker = UIImagePickerController()
    var strImageName = "" , strLAt = "" , strLong = "" , strCityNAme = "" , strStateID = "" 
    
    var aryState = NSMutableArray()
    var aryCity = NSMutableArray()
   
    
    // MARK: - ----------- Google Address Code ----------------
    // MARK: -
       @IBOutlet weak var scrollView: UIScrollView!
       @IBOutlet weak var tableView: UITableView!
       private var apiKey: String = GoogleMapsAPIServerKey
       private var placeType: PlaceType = .all
       private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
       private var radius: Double = 0.0
       private var strictBounds: Bool = false
       private var places = [Place]() {
           didSet { tableView.reloadData() }
       }
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        ButtongradientLayer(btnArray: [btnSignUP,btnBGProfile])

        self.btnBGProfile.layer.cornerRadius = 15.0
        self.imgProfile.layer.borderWidth = 2
        self.imgProfile.layer.borderColor = UIColor.white.cgColor
        self.imgProfile.layer.masksToBounds = false
        self.imgProfile.layer.cornerRadius = 15
        self.imgProfile.clipsToBounds = true
        self.imgProfile.contentMode = .center
        
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
        textField.text = trimmedText
        if(txtMobile == textField){
            txtMobile.text = formattedNumber(number: textField.text!)
        }
    }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnSignUP(_ sender: TransitionButton) {
        self.view.endEditing(true)
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                               sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
           }, completion: { finish in
                               sender.transform = CGAffineTransform.identity
            if(self.validationForSignUP()){
                let dictData = NSMutableDictionary()
                dictData.setValue("\(self.txtFirstName.text!)", forKey: "Name")
                dictData.setValue("\(self.txtMiddleName.text!)", forKey: "MiddleName")
                dictData.setValue("\(self.txtLastName.text!)", forKey: "LastName")
                dictData.setValue(removeUSformate(number: self.txtMobile.text!), forKey: "MobileNo")
                dictData.setValue("\(self.txtEmail.text!)", forKey: "EmailId")
                dictData.setValue("\(self.txtUserName.text!)", forKey: "UserName")
                dictData.setValue("\(self.txtPassword.text!)", forKey: "Password")
           
                dictData.setValue("\(self.strImageName)", forKey: "ProfileImage")
                dictData.setValue(self.imgProfile.image, forKey: "ProfileUIImage")
           
                
               
                dictData.setValue("\(self.strStateID)", forKey: "state_id")
                dictData.setValue("\(self.strCityNAme)", forKey: "city_name")
                dictData.setValue("\(self.txtAddress.text!)", forKey: "Address")
                dictData.setValue("\(self.strLAt)", forKey: "Lat")
                dictData.setValue("\(self.strLong)", forKey: "Long")

                
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "RoleSelectionClass") as? RoleSelectionClass
                vc?.dictSignUpData = dictData
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        })
    }

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actionOnEditProfile(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController.init(title: alertPhotoSource, message: alertChooseSource, preferredStyle: .actionSheet)
        let actioncamera = UIAlertAction(title: alertCamera, style: .default) { (action: UIAlertAction!) in
            
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
                
            }
        }
        actioncamera.setValue(UIImage(named: "camera-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        
        alertController.addAction(actioncamera)
        
        
        let actionGallery = UIAlertAction(title: alertPhotoLibrary, style: .default) { (action: UIAlertAction!) in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
                
            }
        }
        actionGallery.setValue(UIImage(named: "gallery-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        alertController.addAction(actionGallery)
        let actionDismiss = UIAlertAction(title: "Dismiss", style: .destructive) { (action: UIAlertAction!) in
            
        }
        alertController.addAction(actionDismiss)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
// MARK:
// MARK:- Validation

extension SignUpClass {
    
    func validationForSignUP() -> Bool {
        
        if(txtFirstName.text?.count == 0){
            // addErrorMessage(textView: txtFirstName, message: alert_UserName)
            addErrorMessage(textView: txtFirstName, message: alert_required)
            return false
        }
//        else if(txtMiddleName.text?.count == 0){
//            addErrorMessage(textView: txtMiddleName, message: alert_required)
//            return false
//        }
//        else if(txtLastName.text?.count == 0){
//            addErrorMessage(textView: txtLastName, message: alert_required)
//            return false
//        }
        else if(txtUserName.text?.count == 0){
            addErrorMessage(textView: txtUserName, message: alert_UserName)
            return false
        }
        else if(txtPassword.text?.count == 0){
            addErrorMessage(textView: txtPassword, message: alert_Password)
            return false
        }
        
        else if(txtMobile.text?.count == 0){
            addErrorMessage(textView: txtMobile, message: alert_MobileNumber)
            return false
        }
     else if(txtMobile.text!.count < 10){
                         addErrorMessage(textView: txtMobile, message: alert_MobileNumberValid)
                         return false
    }
      else if(txtEmail.text?.count != 0){
        if((txtEmail.text?.isValidEmailAddress())!){
            return true
        }
        addErrorMessage(textView: txtEmail, message: alert_EmailValid)
        return false
        }
        
        
        if(txtAddress.text?.count == 0){
            addErrorMessage(textView: txtAddress, message: alert_Address)
                       return false
        }
      
        if(strCityNAme == "" || self.strStateID == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_AddressProper, viewcontrol: self)
            return false
        }
        return true
    }
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
}
// MARK: -
// MARK: -UINavigationControllerDelegate
extension SignUpClass : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.imgProfile.contentMode = .scaleAspectFill
        self.strImageName = "\(getUniqueString())"
        self.imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
    }
}

//MARK:-
//MARK:- ---------UITextFieldDelegate

extension SignUpClass : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
         
         if(textField == txtAddress){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
               self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 500), animated: false)
                         self.scrollView.isScrollEnabled = false
            }
          

         }
     }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

      if(textField == txtMobile){
                return ACFloatingTextValidation(textField: txtMobile, string: string, returnOnly: "NUMBER", limitValue: 20)
             }
     
        if(textField == txtMiddleName){
                      return ACFloatingTextValidation(textField: txtMiddleName, string: string, returnOnly: "", limitValue: 2)
                   }
        if(textField == txtAddress){
            if (range.location == 0) && (string == " ")
            {
                return false
            }
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.tableView.removeFromSuperview()
                    return true
                }
            }
            var txtAfterUpdate:NSString = txtAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
               if(textField == txtAddress){

                    let txtAfterUpdate:NSString = txtAddress.text! as NSString
                    if(txtAfterUpdate == ""){
                        places = [];
                        return true
                    }
                    let parameters = getParameters(for: txtAfterUpdate as String)

                    return true
                }
        return true
    }
    
       func textFieldDidEndEditing(_ textField: UITextField) {
           if(textField == txtAddress){
               self.scrollView.isScrollEnabled = true
               self.tableView.removeFromSuperview()
           }
           
       }
       private func getParameters(for text: String) -> [String: String] {
             var params = [
                 "input": text,
                 "types": placeType.rawValue,
                 "key": apiKey
             ]
             
             if CLLocationCoordinate2DIsValid(coordinate) {
                 params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
                 
                 if radius > 0 {
                     params["radius"] = "\(radius)"
                 }
                 
                 if strictBounds {
                     params["strictbounds"] = "true"
                 }
             }
             
             return params
         }
         
}
//MARK:-
//MARK:- ---------doRequest
extension SignUpClass {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   /* for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                 
                    self.tableView.layer.cornerRadius = 8.0
                    
                    self.tableView.frame = CGRect(x: Int(self.txtAddress.frame.origin.x), y: 575, width: Int(self.txtAddress.frame.width), height:200)

                    
                    
                    self.scrollView.addSubview(self.tableView)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension SignUpClass : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            self.txtAddress.text = value.formattedAddress
            
             if let state = value.administrativeArea {
                self.strStateID = getStateIDFromeJson(strName: "\(state)")
             }
            if let town = value.locality {
                         self.strCityNAme  = town
                     }
            self.strLAt = "\(value.coordinate?.latitude ?? 0.0)"
            self.strLong = "\(value.coordinate?.longitude ?? 0.0)"
            if("\(value.countryCode!)" != "US"){
                        self.strLAt = "0"
                        self.strLong = "0"
                        self.strStateID = "0"
                        self.txtAddress.text = ""
                        self.strCityNAme = ""
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLocationCountry, viewcontrol: self)
                    }
            self.places = [];
        }
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
// MARK: -
// MARK: -addressCell
class addressCell: UITableViewCell {
    @IBOutlet weak var lbl_Address: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
