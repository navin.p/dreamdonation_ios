//
//  TutorialVC.swift
//  AaharFoeUS
//
//  Created by Navin Patidar on 2/5/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import CRNotifications

class TutorialVC: UIViewController {
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var collectionForWelcome: UICollectionView!
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var viewLoader: UIView!

    //MARK:
    //MARK:- CustomeVariable
    let  ary_CollectionData = [["title":"HowToWork ","subtitle" :"Designed with the users in mind, 'Dream donations' is an intuitive,easy-to-use application. Here are the steps to use 'Dream donations' for food procurement and donation.","image":"well_img_1"],["title":"HowToDonate","subtitle" :"Designed with the users in mind, 'Dream donations' is an intuitive,easy-to-use application. Here are the steps to use 'Dream donations' for food procurement and donation.","image":"well_img_1"],["title":"HowToTransport","subtitle" :"<Designed with the users in mind, 'Dream donations' is an intuitive,easy-to-use application. Here are the steps to use 'Dream donations' for food procurement and donation."
,"image":"well_img_1"]]
    
      var aryContainData = NSMutableArray()
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
         aryContainData = getDataFromCoreDataBase(strEntity: "AllContain", strkey: "contain")
        if(aryContainData.count == 0){
           viewLoader.isHidden = false
            if (isInternetAvailable()){
                callGetAllContainAPI()
                 viewLoader.isHidden = false
            }else{
               CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 3)
            }
        }else{
            viewLoader.isHidden = true
          aryContainData =  filterArray(ary: aryContainData)
            self.collectionForWelcome.reloadData()

        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    func filterArray(ary: NSMutableArray) -> NSMutableArray {
        let arytemp = NSMutableArray()
        for item in ary {
            if((item as AnyObject).value(forKey: "TutorialType") as! String == "Tutorial"){
                arytemp.add(item)
            }
        }
        return arytemp
        }
  
    
    // MARK:
    // MARK:- IBAction
    
    @IBAction func actionOnSkip(_ sender: UIButton) {
        
        if(nsud.value(forKey: "DreamDonation_Permission") == nil){
            nsud.setValue(true, forKey: "DreamDonation_Permission")
            UIButton.animate(withDuration: 0.2,
                animations: {
                sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.96)
                },
                completion: { finish in
                sender.transform = CGAffineTransform.identity
                    let controller = SPPermissions.list([.photoLibrary, .camera , .notification])
                         //  controller.headerText = "Header Text"
                         // By default using project texts and icons.
                         controller.dataSource = self
                         controller.delegate = self
                         
                         // Always use this method for present
                         // self.present(controller, animated: true, completion: nil)
                         controller.present(on: self)
                   
                })
               
        }else{
               let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginClass")as! LoginClass
                              self.navigationController?.pushViewController(testController, animated: true)
        }
        
  
        
   
//
     }
}
// MARK:
// MARK:- UICollectionView
extension TutorialVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryContainData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "welcomeCell", for: indexPath as IndexPath) as! welcomeCell
        let dict = self.aryContainData[indexPath.row]as! NSDictionary
        cell.lbl_Title.text = app_Name
        cell.lbl_SubTitle.text = dict["title"]as? String
        
        cell.lbl_Title.text = dict["ContentTitle"]as? String
        cell.lbl_SubTitle.text = (dict["HtmlContent"]as? String)?.withoutHtml
        cell.welcome_lbl_Title.text = app_Name
        // cell.welcome_lbl_SubTitle.text = dict["title"]as? String
         cell.welcome_Image.image = UIImage(named: "well_img_1")
        
       // cell.welcome_Image.image = UIImage(named: dict["ContentImages"]as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionForWelcome.frame.size.width) , height:(self.collectionForWelcome.frame.size.height))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let indexPath = collectionForWelcome.indexPathsForVisibleItems.first {
            self.pager.currentPage = indexPath.row
        }
    }
}
class welcomeCell: UICollectionViewCell {
    @IBOutlet weak var welcome_Image: UIImageView!
    @IBOutlet weak var welcome_lbl_Title: UILabel!
    @IBOutlet weak var welcome_lbl_SubTitle: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_SubTitle: UILabel!
    
}

// MARK:-
// MARK:- SPPermissions Data Source & Delegate

extension TutorialVC: SPPermissionsDataSource, SPPermissionsDelegate {
    
    
    /**
     Configure permission cell here.
     You can return permission if want use default values.
     
     - parameter cell: Cell for configure. You can change all data.
     - parameter permission: Configure cell for it permission.
     */
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        
        
        // Titles
//            cell.permissionTitleLabel.text = "Notifications"
//                 cell.permissionDescriptionLabel.text = "Remind about payment to your bank"
//                 cell.button.allowTitle = "Allow"
//                 cell.button.allowedTitle = "Allowed"
        
        //    Colors
        cell.iconView.color = hexStringToUIColor(hex: primaryColor)
        cell.button.allowedBackgroundColor = hexStringToUIColor(hex: primaryColor)
        cell.button.allowTitleColor = hexStringToUIColor(hex: primaryColor)
        
        // If you want set custom image.
        // cell.set(UIImage(named: "IMAGE-NAME")!)
        return cell
    }
    
    /**
     Call when controller closed.
     
     - parameter ids: Permissions ids, which using this controller.
     */
    func didHide(permissions ids: [Int]) {
        let permissions = ids.map { SPPermission(rawValue: $0)! }
        print("Did hide with permissions: ", permissions.map { $0.name })
        
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginClass")as!LoginClass
        self.navigationController?.pushViewController(testController, animated: true)
        
        
    }
    
    /**
     Call when permission allowed.
     Also call if you try request allowed permission.
     
     - parameter permission: Permission which allowed.
     */
    func didAllow(permission: SPPermission) {
        print("Did allow: ", permission.name)
    }
    
    /**
     Call when permission denied.
     Also call if you try request denied permission.
     
     - parameter permission: Permission which denied.
     */
    func didDenied(permission: SPPermission) {
        print("Did denied: ", permission.name)
        _  = deniedData(for: permission)
    }
    
    /**
     Alert if permission denied. For disable alert return `nil`.
     If this method not implement, alert will be show with default titles.
     
     - parameter permission: Denied alert data for this permission.
     */
    func deniedData(for permission: SPPermission) -> SPPermissionDeniedAlertData? {
        //   if permission == .notification {
        let data = SPPermissionDeniedAlertData()
        data.alertOpenSettingsDeniedPermissionTitle = "Permission denied"
        data.alertOpenSettingsDeniedPermissionDescription = "Please, go to Settings and allow \(permission.name)."
        data.alertOpenSettingsDeniedPermissionButtonTitle = "Settings"
        data.alertOpenSettingsDeniedPermissionCancelTitle = "Cancel"
        return data
        //        } else {
        //            // If returned nil, alert will not show.
        //            print("Alert for \(permission.name) not show, becouse in datasource returned nil for configure data. If you need alert, configure this.")
        //            return nil
        //        }
    }
}
// MARK:
// MARK:- API Calling

extension TutorialVC{
    func callGetAllContainAPI() {
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetContent xmlns='\(BaseURL)'><country_id>2</country_id><ContentType></ContentType></GetContent></soap12:Body></soap12:Envelope>"
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetContentResult", responcetype: "GetContentResponse") { (responce, status) in
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.aryContainData = NSMutableArray()
                        self.aryContainData = (dictTemp.value(forKey: "DTList")as! NSArray).mutableCopy()as! NSMutableArray

                        deleteAllRecords(strEntity:"AllContain")
                        saveDataInLocalArray(strEntity: "AllContain", strKey: "contain", data: self.aryContainData)
                        self.aryContainData =  self.filterArray(ary: self.aryContainData)

                        self.viewLoader.isHidden = true
                        self.collectionForWelcome.reloadData()
                    }else{
                          CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    }
                }else{
                 CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    
                }
            }
        }
}
extension String {
    public var withoutHtml: String {
        guard let data = self.data(using: .utf8) else {
            return self
        }

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }

        return attributedString.string
    }
}
