//
//  ThankyouClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/6/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class ThankyouClass: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var btnSubmit: TransitionButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    var data = NSMutableDictionary()
    var strMessgae = ""
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        // Do any additional setup after loading the view.
    }
    
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        ButtongradientLayer(btnArray: [btnSubmit])
        lblTitle.textColor = hexStringToUIColor(hex: primaryColor)
        if(data.count != 0){
            lblSubTitle.text = strMessgae
            //lblSubTitle.text = "Thank you \(data.value(forKey: "Name")!) for creating an account as \(data.value(forKey: "Role")!).\nYou're ready to go!"
        }
    }
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnSubmit(_ sender: TransitionButton) {
        self.view.endEditing(true)
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
        }, completion: { finish in
            sender.transform = CGAffineTransform.identity
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginClass") as! LoginClass
            vc.strUserName = "\(self.data.value(forKey: "UserName")!)"
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        
    }
    
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
}
