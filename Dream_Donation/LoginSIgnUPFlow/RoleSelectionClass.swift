//
//  RoleSelectionClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/6/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class RoleSelectionClass: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    @IBOutlet weak var btnDonorCheckBox: UIButton!
    @IBOutlet weak var btnDonorIndivisualCheckBox: UIButton!
    @IBOutlet weak var btnDonorCorporateCheckBox: UIButton!
    @IBOutlet weak var txtDonor_OrganizationName: ACFloatingTextfield!
    @IBOutlet weak var heightDonorView: NSLayoutConstraint!

    @IBOutlet weak var btnTransportCheckBox: UIButton!
    @IBOutlet weak var btnReciverCheckBox: UIButton!
    @IBOutlet weak var txtReciver_OrganizationNAme: ACFloatingTextfield!
    @IBOutlet weak var txtReciver_WebSite: ACFloatingTextfield!
    @IBOutlet weak var txtReciver_HrsOfOpration: ACFloatingTextfield!
      
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var height_ReciverView: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: TransitionButton!
    

    var slectedIndex = 0
    var slectedDonorSubIndex = 0
    var dictSignUpData = NSMutableDictionary()
    var imageDocument = UIImage()
    var imagePicker = UIImagePickerController()

    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
      
    }
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        ButtongradientLayer(btnArray: [btnSubmit])
        heightDonorView.constant = 0.0
        height_ReciverView.constant = 0.0
    }
    
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnDonor(_ sender: UIButton){
        self.view.endEditing(true)
        self.slectedIndex = 1
        self.slectedDonorSubIndex = 1
        
        heightDonorView.constant = 70.0
        btnDonorCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
        btnDonorCorporateCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        btnDonorIndivisualCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
        
        
        btnTransportCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        btnReciverCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        height_ReciverView.constant = 0.0


    }
    @IBAction func actionOnDonor_Indivisual(_ sender: UIButton){
           self.view.endEditing(true)

           self.slectedIndex = 1
           self.slectedDonorSubIndex = 1
           
           heightDonorView.constant = 70.0
           btnDonorCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
           btnDonorCorporateCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
           btnDonorIndivisualCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
           
           
           
           btnTransportCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
           btnReciverCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
           height_ReciverView.constant = 0.0


       }
    @IBAction func actionOnDonor_Corporate(_ sender: UIButton){
           self.view.endEditing(true)

           self.slectedIndex = 1
           self.slectedDonorSubIndex = 2
           
           heightDonorView.constant = 125.0
           btnDonorCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
           btnDonorCorporateCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
           btnDonorIndivisualCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
           
           
           
           btnTransportCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
           btnReciverCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
           height_ReciverView.constant = 0.0


       }
    
    
    
    @IBAction func actionOnTransport(_ sender: UIButton){
        self.view.endEditing(true)

        self.slectedIndex = 2
        self.slectedDonorSubIndex = 0
        btnTransportCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
        
        
        heightDonorView.constant = 0.0
        btnDonorCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        btnDonorCorporateCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        btnDonorIndivisualCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        
        btnReciverCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        height_ReciverView.constant = 0.0

     }
    @IBAction func actionOnReciver(_ sender: UIButton){
        self.view.endEditing(true)

        self.slectedIndex = 3
        self.slectedDonorSubIndex = 0
        
        btnReciverCheckBox.setImage(UIImage(named: "redio_button"), for: .normal)
        height_ReciverView.constant = 245.0
        
        btnTransportCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        
        heightDonorView.constant = 0.0
        btnDonorCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        btnDonorCorporateCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        btnDonorIndivisualCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
        
        
    }
    
    @IBAction func actionOnSubmit(_ sender: TransitionButton) {
        self.view.endEditing(true)
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
        }, completion: { finish in
            sender.transform = CGAffineTransform.identity
            if(self.validation()){
                self.APICalling()
            }
        })
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
          
      }
    @IBAction func actionOnBrowse(_ sender: UIButton) {
             
                   let alertController = UIAlertController.init(title: alertPhotoSource, message: alertChooseSource, preferredStyle: .actionSheet)
                   let actioncamera = UIAlertAction(title: alertCamera, style: .default) { (action: UIAlertAction!) in
                       
                       self.imagePicker = UIImagePickerController()
                       
                       if UIImagePickerController.isSourceTypeAvailable(.camera){
                           print("Button capture")
                           
                           self.imagePicker.delegate = self
                           self.imagePicker.sourceType = .camera
                           self.imagePicker.allowsEditing = false
                           self.present(self.imagePicker, animated: true, completion: nil)
                       }else{
                           CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
                           
                       }
                   }
                   actioncamera.setValue(UIImage(named: "camera-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
                   
                   alertController.addAction(actioncamera)
                   
                   
                   let actionGallery = UIAlertAction(title: alertPhotoLibrary, style: .default) { (action: UIAlertAction!) in
                       self.imagePicker = UIImagePickerController()
                       if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                           print("Button capture")
                           self.imagePicker.delegate = self
                           self.imagePicker.sourceType = .savedPhotosAlbum
                           self.imagePicker.allowsEditing = false
                           self.present(self.imagePicker, animated: true, completion: nil)
                       }else{
                           CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
                           
                       }
                   }
                   actionGallery.setValue(UIImage(named: "gallery-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
                   alertController.addAction(actionGallery)
                   let actionDismiss = UIAlertAction(title: "Dismiss", style: .destructive) { (action: UIAlertAction!) in
                       
                   }
                   alertController.addAction(actionDismiss)
                   self.present(alertController, animated: true, completion: nil)
                   
               }
    
    
    func validation() -> Bool {
        if(self.slectedIndex == 1 && self.slectedDonorSubIndex == 2){
            //require o name
            addErrorMessage(textView: txtDonor_OrganizationName, message: alert_required)
            return false
        }
        else if(self.slectedIndex == 3 ){
            if(txtReciver_OrganizationNAme.text?.count == 0){
                addErrorMessage(textView: txtReciver_OrganizationNAme, message: alert_required)
                           return false
            }else if (txtReciver_WebSite.text?.count == 0){
                addErrorMessage(textView: txtReciver_WebSite, message: alert_required)
                                       return false
            }else if (txtReciver_HrsOfOpration.text?.count == 0){
                addErrorMessage(textView: txtReciver_HrsOfOpration, message: alert_required)
                return false
            }
            else if (Int(txtReciver_HrsOfOpration.text!)! > 24){
              addErrorMessage(textView: txtReciver_HrsOfOpration, message: alert_operation)
              return false
          }
            else if (lblDocumentName.text?.count == 0){
                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Document is required!", viewcontrol: self)
                          return false
              }
            
        }
        return true
    }
    
    func APICalling()  {
        if !(isInternetAvailable()){
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }else{

            if(self.dictSignUpData.value(forKey: "ProfileImage")as! String != ""){
                let imgProfile = self.dictSignUpData.value(forKey: "ProfileUIImage")as! UIImage
                self.uploadProfileImage(strImageNAme: self.dictSignUpData.value(forKey: "ProfileImage")as! String, image: imgProfile)
            }
           else if (lblDocumentName.text?.count != 0){
                          self.uploadDocumentImage(strImageNAme: self.lblDocumentName.text!, image: self.imageDocument)

                      }
            else{
                self.btnSubmit.startAnimation()
                 self.callSignUpAPI()
            }
        }
    }
   
      func addErrorMessage(textView : ACFloatingTextfield, message : String) {
          textView.shakeLineWithError = true
          textView.showErrorWithText(errorText: message)
      }
  
}





// MARK:
// MARK:- UITextFieldDelegate

extension RoleSelectionClass : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtReciver_HrsOfOpration){
            return ACFloatingTextValidation(textField: txtReciver_HrsOfOpration, string: string, returnOnly: "NUMBER", limitValue: 1)
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
// MARK:
// MARK:- API Calling

extension RoleSelectionClass{
    func callSignUpAPI() {

         dictSignUpData.removeObject(forKey: "ProfileUIImage")
        let strSubType = slectedDonorSubIndex == 1 ? "Individual" : "Corporate"
        
        var strRole = ""
        if(self.slectedIndex == 1){
            strRole = "Donor"
        }else if (self.slectedIndex == 2){
            strRole = "Transport"
        }else if (self.slectedIndex == 3){
            strRole = "Receiver"
        }
        dictSignUpData.setValue("\(strSubType)", forKey: "UserType")
        dictSignUpData.setValue("True", forKey: "IsActive")
        
        dictSignUpData.setValue("\(strRole)", forKey: "Role")
        dictSignUpData.setValue("\(strCountryID)", forKey: "country_id")
       
        dictSignUpData.setValue("", forKey: "Website")
        dictSignUpData.setValue("", forKey: "HoursOfOperation")
        dictSignUpData.setValue("", forKey: "AttachDocument")
        dictSignUpData.setValue("", forKey: "InstitutionName")
     
        if (self.slectedIndex == 1 && self.slectedDonorSubIndex == 2){
            dictSignUpData.setValue("\(self.txtDonor_OrganizationName.text!)", forKey: "InstitutionName")
        }
        if (self.slectedIndex == 3){
           
            dictSignUpData.setValue("\(self.txtReciver_OrganizationNAme.text!)", forKey: "InstitutionName")
            
            dictSignUpData.setValue("\(txtReciver_WebSite.text!)", forKey: "Website")
            dictSignUpData.setValue("\(txtReciver_HrsOfOpration.text!)", forKey: "HoursOfOperation")
            dictSignUpData.setValue("\(lblDocumentName.text!)", forKey: "AttachDocument")
            dictSignUpData.setValue("", forKey: "UserType")

        }
        

        
        print(dictSignUpData)
        
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictSignUpData) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: dictSignUpData, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        
    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UserRegistration xmlns='\(BaseURL)'><DreamDonationRegistrationMdl>\(jsonString)</DreamDonationRegistrationMdl></UserRegistration></soap12:Body></soap12:Envelope>"
    WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "UserRegistrationResult", responcetype: "UserRegistrationResponse") { (responce, status) in
        self.btnSubmit.stopAnimation()
        
        if status == "Suceess"{
            let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"

            if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPVerificationClass") as? OTPVerificationClass
                vc!.strComeFrom = "REGISTRATION"
                vc!.strMessage = apiError
                vc!.dictData = self.dictSignUpData
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }else{
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
            }
        }else{
            
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
            
        }
    }
      }
    func uploadProfileImage(strImageNAme : String , image : UIImage)  {
        btnSubmit.startAnimation()
            WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProfile, image: image, fileName: strImageNAme, withName: "userfile") { (responce, status) in
                if(status == "Suceess"){
                    if (self.lblDocumentName.text?.count != 0){
                        self.uploadDocumentImage(strImageNAme: self.lblDocumentName.text!, image: self.imageDocument)
                        
                    }
                    else{
                        self.callSignUpAPI()
                    }
                    
                }else{
                    self.btnSubmit.stopAnimation()
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                }
            }
    }
    func uploadDocumentImage(strImageNAme : String , image : UIImage)  {
        btnSubmit.startAnimation()
            WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProfile, image: image, fileName: strImageNAme, withName: "userfile") { (responce, status) in
                if(status == "Suceess"){
                    self.callSignUpAPI()
                }else{
                    self.btnSubmit.stopAnimation()
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                }
            }
    }
}
// MARK: -
// MARK: -UINavigationControllerDelegate
extension RoleSelectionClass : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.lblDocumentName.text = "Document\(getUniqueString())"
        self.imageDocument = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
    }
}
