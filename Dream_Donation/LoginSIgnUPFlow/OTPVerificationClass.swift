//
//  OTPVerificationClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 4/10/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class OTPVerificationClass: UIViewController {
    // MARK:
    // MARK:- IBOutlet
     @IBOutlet weak var txtOTP: ACFloatingTextfield!
     @IBOutlet weak var btnResend: TransitionButton!
     @IBOutlet weak var btnVerify: TransitionButton!
     @IBOutlet weak var lblMEssage: UILabel!
    @IBOutlet weak var lblCount: UILabel!

    var timer : Timer!
    var second = 30
    
    var strComeFrom = ""
    var strMessage = ""

    var dictData = NSMutableDictionary()
     // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    
    // MARK:
        // MARK:- Extra Function
        func setUpUI() {
            ButtongradientLayer(btnArray: [btnResend,btnVerify])
            let mobileNumer = formattedNumber(number: "\(dictData.value(forKey: "MobileNo")!)")

            guard mobileNumer.count > 5 else {
                fatalError("The phone number is not complete")
            }

            let intLetters = mobileNumer.prefix(0)
            let endLetters = mobileNumer.suffix(3)

            let stars = String(repeating: "*", count: mobileNumer.count - 3)

            let result = intLetters + stars + endLetters
            
            lblMEssage.text = "A One time passcode has been sent to \(result).\n\nPlease enter the OTP below to verify your account."
            btnResend.isHidden = true

            timer = Timer()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.calculateSeconds), userInfo: nil, repeats: true)
        }
  
    @objc func calculateSeconds() {
         second -= 1
         whatever()
    }
    func whatever() {
         if second < 30 && second > 0 {
            lblCount.text = "00:\(second)"
            btnResend.isHidden = true
         }
         else {
            lblCount.text = ""
            btnResend.isHidden = false
            second = 30
            timer.invalidate()
             timer = nil
        }
        
    }
    
    func CheckConditionAndRedirct(dict : NSMutableDictionary) {
        let strRole = "\(dict.value(forKey: "Role")!)"
            
                    if(self.dictData.value(forKey: "IsRemember") as! Bool){
                        
                        nsud.set(true, forKey: "Dream_Donation_KeepLogin")
                        nsud.set("\(dict.value(forKey: "UserName")!)", forKey: "Dream_Donation_UserName")
                        nsud.set("\(dictData.value(forKey: "Password")!)", forKey: "Dream_Donation_Password")
                    }else{
                        nsud.set(false, forKey: "Dream_Donation_KeepLogin")
                        nsud.set("", forKey: "Dream_Donation_UserName")
                        nsud.set("", forKey: "Dream_Donation_Password")
                    }
                    nsud.set(dict.removeNullFromDict(), forKey: "Dream_Donation_LoginData")
                    nsud.synchronize()
                    globleLogInData = NSMutableDictionary()
                    globleLogInData = dict
                  
        //For Role Check and Redirect
        
                        if(strRole == "Donor" || strRole == "Transport" || strRole == "Manager"){ // For Donor and Transport
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardClass") as? DashBoardClass
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }else if (strRole == "Receiver"){ // For Receiver
                            
                        }
               
        
    }
    
    // MARK:
          // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func actionOnResend(_ sender: UIButton) {
        self.view.endEditing(true)
        txtOTP.text = ""
        self.callOTPResendAPI()
        
    }
    @IBAction func actionOnVerify(_ sender: UIButton) {
            self.view.endEditing(true)
        if(txtOTP.text!.count >= 4){
            self.callOTPVerifyAPI()
        }else{
            
        }
           
       }
    
}
// MARK:
// MARK:- API Calling

extension OTPVerificationClass{
    func callOTPVerifyAPI() {
        let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)

        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><OTPVerification xmlns='\(BaseURL)'><UserName>\(dictData.value(forKey: "UserName")!)</UserName><OTP>\(txtOTP.text!)</OTP></OTPVerification></soap12:Body></soap12:Envelope>"
 
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "OTPVerificationResult", responcetype: "OTPVerificationResponse") { (responce, status) in
        
            loader.dismiss(animated: false) {
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        if(self.strComeFrom == "LOGIN"){
                            let dictLoginData = ((dictTemp.value(forKey: "DTList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            
                            self.CheckConditionAndRedirct(dict: dictLoginData)
                        }else{
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ThankyouClass") as? ThankyouClass
                            vc!.strMessgae = self.strMessage
                            vc!.data = ((dictTemp.value(forKey: "DTList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }
                        
                        
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                       }
                   }else{
                       
                       CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                   }
            }
    }
}
    
    
      func callOTPResendAPI() {
        let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)

       let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ResendOTP xmlns='\(BaseURL)'><UserName>\(dictData.value(forKey: "UserName")!)</UserName></ResendOTP></soap12:Body></soap12:Envelope>"
    
           WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "ResendOTPResult", responcetype: "ResendOTPResponse") { (responce, status) in
            loader.dismiss(animated: false) {
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                    
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        
                        
                        self.timer = Timer()
                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.calculateSeconds), userInfo: nil, repeats: true)
//                        CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: apiError, dismissDelay: 3, completion: {
//                            
//                            
//                        })
                        
                    }else{
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                    }
                }else{
                    
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                    
                }
                
            }
            
        }
    }
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension OTPVerificationClass : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtOTP){
            return ACFloatingTextValidation(textField: txtOTP, string: string, returnOnly: "All", limitValue: 3)
        }
        
        return true
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
