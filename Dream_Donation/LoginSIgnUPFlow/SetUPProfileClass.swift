//
//  SetUPProfileClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/6/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications
import CoreLocation
import MapKit
import GooglePlacesSearchController

class SetUPProfileClass: UIViewController, UIScrollViewDelegate {
    
    var kTableHeaderHeight:CGFloat = 145
    var headerView: UIView!
    var imagePicker = UIImagePickerController()
    
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var viewHeader: GradientView!
    @IBOutlet weak var imgTopProfile: UIImageView!
    @IBOutlet weak var Width_imgTopProfile: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: TransitionButton!
    
    
    @IBOutlet weak var txtFirstName: ACFloatingTextfield!
    @IBOutlet weak var txtLastName: ACFloatingTextfield!
    @IBOutlet weak var txtMiddleName: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    
    
    @IBOutlet weak var txtUserNAme: ACFloatingTextfield!
    @IBOutlet weak var txtWebsite: ACFloatingTextfield!
    @IBOutlet weak var txthoursOFOpration: ACFloatingTextfield!
    @IBOutlet weak var txtOrganizationName: ACFloatingTextfield!
    @IBOutlet weak var heightOtherInfo: NSLayoutConstraint!

    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSignUP: TransitionButton!
    @IBOutlet weak var btnBGProfile: TransitionButton!
    
    @IBOutlet weak var txtAddress: ACFloatingTextfield!
    @IBOutlet weak var lblTitle: UILabel!

    var strCountry = "" , strState = "" , strCity = ""
    var IDState = "0" , IDCity = "0" , strLat = "0.0" , strLong = "0.0"
    
    var strProfileImageName = "" , strDocumentImageName = ""
    var aryState = NSMutableArray()
    var aryCity = NSMutableArray()
    
    var strComeFrom = ""
    var strTitle = ""
     // MARK: - ----------- Google Address Code ----------------
      // MARK: -
         @IBOutlet weak var scrollView: UIScrollView!
         @IBOutlet weak var tableView: UITableView!
         private var apiKey: String = GoogleMapsAPIServerKey
         private var placeType: PlaceType = .all
         private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
         private var radius: Double = 0.0
         private var strictBounds: Bool = false
         private var places = [Place]() {
             didSet { tableView.reloadData() }
         }
    
    // MARK:
    // MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll.delegate = self
        self.setUpUI()
    }
    
    
    // MARK:
    // MARK:- IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        if(self.strComeFrom == "Profile"){
            self.navigationController?.popViewController(animated: true)
        }else{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: LoginClass.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validationForSetUpProfile()){
            self.APICalling()
        }
        
        
    }
    @IBAction func actionOnEditProfile(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController.init(title: alertPhotoSource, message: alertChooseSource, preferredStyle: .actionSheet)
        let actioncamera = UIAlertAction(title: alertCamera, style: .default) { (action: UIAlertAction!) in
            
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
                
            }
        }
        actioncamera.setValue(UIImage(named: "camera-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        
        alertController.addAction(actioncamera)
        
        
        let actionGallery = UIAlertAction(title: alertPhotoLibrary, style: .default) { (action: UIAlertAction!) in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                CRNotifications.showNotification(type: CRNotifications.info, title: "Error", message: alertCalling, dismissDelay: 3)
                
            }
        }
        actionGallery.setValue(UIImage(named: "gallery-1")!.imageWithSize(scaledToSize: CGSize(width: 32, height: 32)), forKey: "image")
        alertController.addAction(actionGallery)
        let actionDismiss = UIAlertAction(title: "Dismiss", style: .destructive) { (action: UIAlertAction!) in
            
        }
        alertController.addAction(actionDismiss)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
  
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        ButtongradientLayer(btnArray: [btnSubmit, btnBGProfile])
   
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.updateHeaderView()
            self.strComeFrom == "Profile" ? self.lblTitle.text = "Profile" :  (self.lblTitle.text = "Setup Profile")
            self.strComeFrom == "Profile" ? self.btnSubmit.setTitle("Update Profile", for: .normal) :  self.btnSubmit.setTitle("Next", for: .normal)
            
            self.btnBGProfile.layer.cornerRadius = 15.0
            self.imgProfile.layer.borderWidth = 2
            self.imgProfile.layer.borderColor = UIColor.white.cgColor
            self.imgProfile.layer.masksToBounds = false
            self.imgProfile.layer.cornerRadius = 15
            self.imgProfile.clipsToBounds = true
            self.imgTopProfile.layer.masksToBounds = false
            self.imgTopProfile.layer.cornerRadius = 22
            self.imgTopProfile.clipsToBounds = true
        }
        // Data Put On View
        txtUserNAme.text = "\(globleLogInData.value(forKey: "UserName")!)"
        txtOrganizationName.text = "\(globleLogInData.value(forKey: "InstitutionName")!)"

        txtFirstName.text = "\(globleLogInData.value(forKey: "Name")!)"
        txtMiddleName.text = "\(globleLogInData.value(forKey: "MiddleName")!)"
        txtLastName.text = "\(globleLogInData.value(forKey: "LastName")!)"
        txtEmail.text = "\(globleLogInData.value(forKey: "EmailId")!)"
        txtMobile.text = "\(globleLogInData.value(forKey: "MobileNo")!)"
        txtAddress.text = "\(globleLogInData.value(forKey: "Address")!)"
        self.strCity = "\(globleLogInData.value(forKey: "city_name")!)"
        strLat = "\(globleLogInData.value(forKey: "Lat")!)"
        strLong = "\(globleLogInData.value(forKey: "Long")!)"
        self.IDCity = "\(globleLogInData.value(forKey: "city_id")!)" == "" ? "0" : "\(globleLogInData.value(forKey: "city_id")!)"
        self.IDState = "\(globleLogInData.value(forKey: "state_id")!)" == "" ? "0" : "\(globleLogInData.value(forKey: "state_id")!)"
        
        let imgName = "\(globleLogInData.value(forKey: "ProfileImage")!)"
        self.imgProfile.contentMode = .center
        self.imgProfile.setImageWith(URL(string: ProfileImageDownload + imgName), placeholderImage: UIImage(named: "add_photo"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
            if(image != nil){
                self.imgProfile.contentMode = .scaleAspectFill
            }
        }, usingActivityIndicatorStyle: .gray)
        self.strProfileImageName = imgName
        txtMobile.isUserInteractionEnabled = false
        
        
        if("\(globleLogInData.value(forKey: "Role")!)" == "Receiver"){
            heightOtherInfo.constant = 0.0
            txtWebsite.text = "\(globleLogInData.value(forKey: "Website")!)"
            txthoursOFOpration.text = "\(globleLogInData.value(forKey: "HoursOfOperation")!)"
            strDocumentImageName = "\(globleLogInData.value(forKey: "AttachDocument")!)"
            
            
        }else if("\(globleLogInData.value(forKey: "Role")!)" == "Donor") && ("\(globleLogInData.value(forKey: "UserType")!)" == "Corporate"){
            heightOtherInfo.constant = 55.0
        }else{
            heightOtherInfo.constant = 0.0
        }
        
        
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
        textField.text = trimmedText
        if(txtMobile == textField){
                        txtMobile.text = formattedNumber(number: textField.text!)

        }
    }
    
    
    // MARK:
    // MARK:- scrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func updateHeaderView() {
        
        print(scroll.contentOffset.y)
        if(scroll.contentOffset.y > 142){
            if(self.imgTopProfile.alpha != 1){
                self.imgTopProfile.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
                self.imgTopProfile.alpha = 0
                UIView.animate(withDuration:0.2,
                               delay: 0,
                               usingSpringWithDamping: 0.5,
                               initialSpringVelocity: 3.0,
                               options: .allowUserInteraction,
                               animations: { [weak self] in
                                self?.imgTopProfile.transform = .identity
                    }, completion: { _ in
                        self.imgTopProfile.alpha = 1
                        self.Width_imgTopProfile.constant = 44.0
                })
            }
        }else{
            self.imgTopProfile.alpha = 0.0
            self.Width_imgTopProfile.constant = 0.0
            
        }
    }
}



// MARK: -
// MARK: -UINavigationControllerDelegate
extension SetUPProfileClass : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.imgProfile.contentMode = .scaleAspectFill
        self.strProfileImageName = "\(getUniqueString())"
    }
}

extension UIImage {
    func imageWithSize(scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

//MARK:-
//MARK:- AddressFromMapScreenDelegate

extension SetUPProfileClass: AddressFromMapScreenDelegate {
    func GetAddressFromMapScreen(dictData: NSDictionary, tag: Int) {
        // self.txtAddress.text = (dictData.value(forKey: "address")as! String)
    }
    
    
}

// MARK:
// MARK:- API Calling

extension SetUPProfileClass{
    func APICalling()  {
        if !(isInternetAvailable()){
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
        }else{
            
            if(globleLogInData.value(forKey: "ProfileImage")as! String != self.strProfileImageName){
                
                self.uploadProfileImage(strImageNAme: self.strProfileImageName, image: imgProfile.image!)
            }else{
                self.btnSubmit.startAnimation()
                self.callUpdateAPI()
            }
        }
    }
    func callUpdateAPI() {
        let dictJson = NSMutableDictionary()
        dictJson.setValue("\(globleLogInData.value(forKey: "DonorVId")!)", forKey: "DonorVId")
        dictJson.setValue("\(txtFirstName.text!)", forKey: "Name")
        dictJson.setValue("\(txtMiddleName.text!)", forKey: "MiddleName")
        dictJson.setValue("\(txtLastName.text!)", forKey: "LastName")
        dictJson.setValue("\(removeUSformate(number: txtMobile.text!))", forKey: "MobileNo")
        dictJson.setValue("\(txtEmail.text!)", forKey: "EmailId")
        dictJson.setValue("\(txtAddress.text!)", forKey: "Address")

        dictJson.setValue("\(self.strLat)", forKey: "Lat")
        dictJson.setValue("\(self.strLong)", forKey: "Long")

        dictJson.setValue("\(strCountryID)", forKey: "country_id")
        dictJson.setValue("\(IDState)", forKey: "state_id")
        dictJson.setValue("\(IDCity)", forKey: "city_id")
        dictJson.setValue("\(strCity)", forKey: "city_name")
        
        dictJson.setValue("\(strProfileImageName)", forKey: "ProfileImage")
        dictJson.setValue("True", forKey: "IsActive")
        
        
        dictJson.setValue("\(txtWebsite.text!)", forKey: "Website")
        dictJson.setValue("\(txtOrganizationName.text!)", forKey: "InstitutionName")
        dictJson.setValue("\(txthoursOFOpration.text!)", forKey: "HoursOfOperation")
        dictJson.setValue("\(strDocumentImageName)", forKey: "AttachDocument")
        
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictJson) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: dictJson, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UpdateProfile xmlns='\(BaseURL)'><DreamDonationUpdateProfileMdl>\(jsonString)</DreamDonationUpdateProfileMdl></UpdateProfile></soap12:Body></soap12:Envelope>"
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "UpdateProfileResult", responcetype: "UpdateProfileResponse") { (responce, status) in
            self.btnSubmit.stopAnimation()
            
            if status == "Suceess"{
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    
                    let dictLoginData = ((dictTemp.value(forKey: "DTList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    nsud.set(dictLoginData.removeNullFromDict(), forKey: "Dream_Donation_LoginData")
                    nsud.synchronize()
                    globleLogInData = NSMutableDictionary()
                    globleLogInData = dictLoginData
                        CRNotifications.showNotification(type: CRNotifications.success, title: "Success", message: apiError, dismissDelay: 3)
                        self.navigationController?.popViewController(animated: true)
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                }
            }else{
                
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                
            }
        }
    }
    func uploadProfileImage(strImageNAme : String , image : UIImage)  {
        btnSubmit.startAnimation()
        WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProfile, image: image, fileName: strImageNAme, withName: "userfile") { (responce, status) in
            if(status == "Suceess"){
                self.callUpdateAPI()
            }else{
                self.btnSubmit.stopAnimation()
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
            }
        }
    }
    
}
// MARK:
// MARK:- Validation

extension SetUPProfileClass {
    
    func validationForSetUpProfile() -> Bool {
        
        if(txtFirstName.text?.count == 0){
            // addErrorMessage(textView: txtFirstName, message: alert_UserName)
            addErrorMessage(textView: txtFirstName, message: alert_required)
            return false
        }
       
            
        else if((txtMobile.text!.count == 0)){
            addErrorMessage(textView: txtMobile, message: alert_MobileNumber)
            return false
        }
            
        else if((txtMobile.text!.count < 10)){
            addErrorMessage(textView: txtMobile, message: alert_MobileNumberValid)
            return false
        }
            
            
        else if(txtEmail.text!.count != 0){
            if((txtEmail.text?.isValidEmailAddress())!){
                return true
            }
            addErrorMessage(textView: txtEmail, message: alert_EmailValid)
            return false
        }
           if(txtAddress.text?.count == 0){
                addErrorMessage(textView: txtAddress, message: alert_Address)
                           return false
            }
          
            if(strCity == "" || self.IDState == ""){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_AddressProper, viewcontrol: self)
                return false
            }
        
        
        
        //------------For Donor Corporate Validation---------------//

        if("\(globleLogInData.value(forKey: "Role")!)" == "Donor") && ("\(globleLogInData.value(forKey: "UserType")!)" == "Corporate"){
            if(txtOrganizationName.text?.count == 0){
                addErrorMessage(textView: txtOrganizationName, message: alert_required)
                return false
            }
        }
        //------------For Reciver Validation---------------//
   if("\(globleLogInData.value(forKey: "Role")!)" == "Receiver"){                   if(txtOrganizationName.text?.count == 0){
                       addErrorMessage(textView: txtOrganizationName, message: alert_required)
                                  return false
                   }else if (txtWebsite.text?.count == 0){
                       addErrorMessage(textView: txtWebsite, message: alert_required)
                                              return false
                   }else if (txthoursOFOpration.text?.count == 0){
                       addErrorMessage(textView: txthoursOFOpration, message: alert_required)
                       return false
                   }
                   else if (Int(txthoursOFOpration.text!)! > 24){
                     addErrorMessage(textView: txthoursOFOpration, message: alert_operation)
                     return false
                 }
                   else if (strDocumentImageName.count == 0){
                              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Document is required!", viewcontrol: self)
                                 return false
                     }
                   
               }
        
        
        return true
    }
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension SetUPProfileClass : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
         
         if(textField == txtAddress){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
               self.scrollView.setContentOffset(CGPoint(x: 0.0, y: 340), animated: false)
                         self.scrollView.isScrollEnabled = false
            }
         }
     }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txthoursOFOpration){
                   return ACFloatingTextValidation(textField: txthoursOFOpration, string: string, returnOnly: "NUMBER", limitValue: 1)
               }
      if(textField == txtMobile){
                return ACFloatingTextValidation(textField: txtMobile, string: string, returnOnly: "NUMBER", limitValue: 20)
             }
      
        if(textField == txtMiddleName){
                      return ACFloatingTextValidation(textField: txtMiddleName, string: string, returnOnly: "", limitValue: 2)
                   }
        if(textField == txtAddress){
            if (range.location == 0) && (string == " ")
            {
                return false
            }
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.tableView.removeFromSuperview()
                    return true
                }
            }
            var txtAfterUpdate:NSString = txtAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
               if(textField == txtAddress){

                    let txtAfterUpdate:NSString = txtAddress.text! as NSString
                    if(txtAfterUpdate == ""){
                        places = [];
                        return true
                    }
                    let parameters = getParameters(for: txtAfterUpdate as String)

                    return true
                }
        self.view.endEditing(true)
        return true
    }
    
       func textFieldDidEndEditing(_ textField: UITextField) {
           if(textField == txtAddress){
               self.scrollView.isScrollEnabled = true
               self.tableView.removeFromSuperview()
           }
           
       }
       private func getParameters(for text: String) -> [String: String] {
             var params = [
                 "input": text,
                 "types": placeType.rawValue,
                 "key": apiKey
             ]
             
             if CLLocationCoordinate2DIsValid(coordinate) {
                 params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
                 
                 if radius > 0 {
                     params["radius"] = "\(radius)"
                 }
                 
                 if strictBounds {
                     params["strictbounds"] = "true"
                 }
             }
             
             return params
         }
         
}
//MARK:-
//MARK:- ---------doRequest
extension SetUPProfileClass {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.tableView.removeFromSuperview()
                }
                else
                {
                  
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                 
                    self.tableView.layer.cornerRadius = 8.0
                    
                    self.tableView.frame = CGRect(x: Int(self.txtAddress.frame.origin.x), y: 423, width: Int(self.txtAddress.frame.width), height:200)

                    
                    
                    self.scrollView.addSubview(self.tableView)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension SetUPProfileClass : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            self.txtAddress.text = value.formattedAddress
            
             if let state = value.administrativeArea {
                self.IDState = getStateIDFromeJson(strName: "\(state)")
             }
            if let town = value.locality {
                         self.strCity  = town
                     }
            self.strLat = "\(value.coordinate?.latitude ?? 0.0)"
            self.strLong = "\(value.coordinate?.longitude ?? 0.0)"
            if("\(value.countryCode!)" != "US"){
                        self.strLat = "0"
                        self.strLong = "0"
                        self.IDState = "0"
                        self.txtAddress.text = ""
                        self.strCity = ""
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLocationCountry, viewcontrol: self)
                    }
            self.places = [];
        }
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
