//
//  ForgotPasswordClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/16/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class ForgotPasswordClass: UIViewController {
   // MARK:
   // MARK:- IBOutlet
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var btnProceed: TransitionButton!
    
     // MARK:
      // MARK:- Life Cycle
      override func viewDidLoad() {
          super.viewDidLoad()
          self.setUpUI()
      }
      // MARK:
      // MARK:- Extra Function
      func setUpUI() {
          ButtongradientLayer(btnArray: [btnProceed])
      }

 // MARK:
  // MARK:- IBAction
  @IBAction func actionOnProceed(_ sender: TransitionButton) {
      self.view.endEditing(true)
      UIButton.animate(withDuration: 0.2,
                          animations: {
                             sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
         }, completion: { finish in
                             sender.transform = CGAffineTransform.identity
            if(self.validationForForgot()){
                if !(isInternetAvailable()){
                  CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
                }else{
                    self.callForgotPasswordAPI()
                }
            }
         })
  }
  
  @IBAction func actionOnBack(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
      
  }

}
// MARK:
// MARK:- Validation

extension ForgotPasswordClass {
    func validationForForgot() -> Bool {
        if(txtEmail.text?.count == 0){
                   addErrorMessage(textView: txtEmail, message: alert_UserName)
                   return false
               }
               
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension ForgotPasswordClass : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
// MARK:
// MARK:- API Calling

extension ForgotPasswordClass{
    func callForgotPasswordAPI() {
    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ForgetPassword xmlns='\(BaseURL)'><UserName>\(txtEmail.text!)</UserName></ForgetPassword></soap12:Body></soap12:Envelope>"
             self.btnProceed.startAnimation()
    WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "ForgetPasswordResult", responcetype: "ForgetPasswordResponse") { (responce, status) in
        self.btnProceed.stopAnimation()
        
        if status == "Suceess"{
            let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"

            if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                var ary = apiError.split(separator: ",")
                if(ary.count > 1){
                    CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: String(ary[0]), dismissDelay: 3, completion: {
                                      self.navigationController?.popViewController(animated: true)
                                  })
                }else{
                    CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: apiError, dismissDelay: 3, completion: {
                                                        self.navigationController?.popViewController(animated: true)
                                                    })
                }
              
                
            }else{
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
            }
        }else{
            
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
            
        }
    }
      }
}
