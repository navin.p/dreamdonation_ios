//
//  LoginClass.swift
//  Dream_Donation
//
//  Created by Navin Patidar on 3/5/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit
import TransitionButton
import CRNotifications

class LoginClass: UIViewController {
    // MARK:
    // MARK:- IBOutlet
    
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var btnLogin: TransitionButton!
    @IBOutlet weak var btnKeepMelogin: UIButton!
    @IBOutlet weak var btnForgot: UIButton!
    @IBOutlet weak var btnNewUser: UIButton!
    @IBOutlet weak var btnShowHide: UIButton!
    var strUserName = ""
    var buildDate: Date {
        if let infoPath = Bundle.main.path(forResource: "Dream_Donation", ofType: "plist"),
            let infoAttr = try? FileManager.default.attributesOfItem(atPath: infoPath),
            let infoDate = infoAttr[.modificationDate] as? Date {
            return infoDate
        }
        return Date()
    }
    // MARK:
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        
        
    print(buildDate)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(getDataFromCoreDataBase(strEntity: "State", strkey: "state").count == 0){
            callstateAPI(sender: UIButton())
        }
        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "\(buildDate)", viewcontrol: self)
    }
    // MARK:
    // MARK:- Extra Function
    func setUpUI() {
        ButtongradientLayer(btnArray: [btnLogin])
        btnShowHide.setTitle("Show", for: .normal)
        txtPassword.isSecureTextEntry = true
        btnKeepMelogin.setImage(UIImage(named: "uncheck"), for: .normal)
        txtUserName.text = strUserName
        if (nsud.value(forKey: "Dream_Donation_KeepLogin") != nil) {
            if(nsud.value(forKey: "Dream_Donation_KeepLogin")as! Bool)
            {
                txtUserName.text = "\(nsud.value(forKey: "Dream_Donation_UserName")!)"
                txtPassword.text = "\(nsud.value(forKey: "Dream_Donation_Password")!)"
                btnKeepMelogin.setImage(UIImage(named: "redio_button"), for: .normal)
                
                self.callLogInAPI()
            }
        }
    }
    
    func CheckConditionAndRedirct(dict : NSMutableDictionary) {
        let strRole = "\(dict.value(forKey: "Role")!)"
        let strIsOTPVerify = "\(dict.value(forKey: "IsVerifyOTP")!)"
        if(strIsOTPVerify == "1"){
            CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message: "LogIn successfully.", dismissDelay: 1, completion: {
                   
                if(self.btnKeepMelogin.currentImage == UIImage(named: "uncheck")){
                        nsud.set(false, forKey: "Dream_Donation_KeepLogin")
                        nsud.set("", forKey: "Dream_Donation_UserName")
                        nsud.set("", forKey: "Dream_Donation_Password")
                        
                    }else{
                        nsud.set(true, forKey: "Dream_Donation_KeepLogin")
                        nsud.set("\(self.txtUserName.text!)", forKey: "Dream_Donation_UserName")
                        nsud.set("\(self.txtPassword.text!)", forKey: "Dream_Donation_Password")
                    }
                    nsud.set(dict.removeNullFromDict(), forKey: "Dream_Donation_LoginData")
                    nsud.synchronize()
                    globleLogInData = NSMutableDictionary()
                    globleLogInData = dict
                  
                        if(strRole == "Donor" || strRole == "Transport" || strRole == "Manager"){ // For Donor and Transport
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardClass") as? DashBoardClass
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }else if (strRole == "Receiver"){ // For Receiver
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardClass") as? DashBoardClass
                                                      self.navigationController?.pushViewController(vc!, animated: true)
                        }
                 
            })
            
            
        }else{
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPVerificationClass") as? OTPVerificationClass
            
            vc?.strComeFrom = "LOGIN"
            dict.setValue(txtPassword.text, forKey: "Password")
            dict.setValue(btnKeepMelogin.currentImage == UIImage(named: "uncheck") ? false : true, forKey: "IsRemember")

            vc?.dictData = dict
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        
        
    }
    
    // MARK:
    // MARK:- IBAction
    @IBAction func ChangeButtionTitle(sender : UIButton)  {
        sender.titleLabel?.text == "Show" ? sender.setTitle("Hide", for: .normal) : sender.setTitle("Show", for: .normal)
        sender.titleLabel?.text == "Show" ? txtPassword.isSecureTextEntry = false : (txtPassword.isSecureTextEntry = true)
        
    }
    @IBAction func actionOnLogin(_ sender: TransitionButton) {
        self.view.endEditing(true)
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.850, y: 0.96)
        }, completion: { finish in
            sender.transform = CGAffineTransform.identity
            if(self.validationForSignIN()){
                if !(isInternetAvailable()){
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertInternet, dismissDelay: 2)
                }else{
                    self.callLogInAPI()
                }
            }
        })
    }
    @IBAction func actionOnKeepMelogin(_ sender: UIButton) {
        self.view.endEditing(true)
        if(btnKeepMelogin.currentImage == UIImage(named: "uncheck")){
            btnKeepMelogin.setImage(UIImage(named: "redio_button"), for: .normal)
        }else{
            btnKeepMelogin.setImage(UIImage(named: "uncheck"), for: .normal)
        }
    }
    @IBAction func actionOnNewAccount(_ sender: TransitionButton) {
        self.view.endEditing(true)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SignUpClass") as? SignUpClass
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func actionOnForgot(_ sender: TransitionButton) {
        self.view.endEditing(true)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordClass") as? ForgotPasswordClass
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
// MARK:
// MARK:- Validation

extension LoginClass {
    func validationForSignIN() -> Bool {
        if(txtUserName.text?.count == 0){
            addErrorMessage(textView: txtUserName, message: alert_UserName)
            
            return false
        }else if(txtPassword.text!.count == 0){
            addErrorMessage(textView: txtPassword, message: alert_Password)
            return false
        }
        return true
    }
    
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- UITextFieldDelegate

extension LoginClass : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
// MARK:
// MARK:- API Calling

extension LoginClass{
    func callLogInAPI() {
//        txtUserName.text = "nvnios3"
//        txtPassword.text = "123456"

        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetUserLogin xmlns='\(BaseURL)'><UserName>\(txtUserName.text!)</UserName><Password>\(txtPassword.text!)</Password></GetUserLogin></soap12:Body></soap12:Envelope>"
        self.btnLogin.startAnimation()
        WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: MainURL, resultype: "GetUserLoginResult", responcetype: "GetUserLoginResponse") { (responce, status) in
            self.btnLogin.stopAnimation()
            
            if status == "Suceess"{
                let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let apiError = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Success")!)"
                
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    
                    let dictLoginData = ((dictTemp.value(forKey: "DTList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    
                    
                    self.CheckConditionAndRedirct(dict: dictLoginData)
                    
                    
                }else{
                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: apiError, dismissDelay: 3)
                }
            }else{
                
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                
            }
        }
    }
    func callstateAPI(sender : UIButton) {
             if !isInternetAvailable() {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                 
             }else{
              let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetStateListByCountry xmlns='http://aahar.org.in/'><country_id>\(strCountryID)</country_id></GetStateListByCountry></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
              
              WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: MainURLForStateCity, resultype: "GetStateListByCountryResult", responcetype: "GetStateListByCountryResponse") { (responce, status) in
                  loader.dismiss(animated: false) {
                      if status == "Suceess"{
                        deleteAllRecords(strEntity:"State")
                        saveDataInLocalArray(strEntity: "State", strKey: "state", data: (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray)
                      }else{
                          
                       //   CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: alertSomeError, dismissDelay: 3)
                          
                          
                      }
                  }
                  
              }
             }
         }
}
var buildDate: Date {
    if let infoPath = Bundle.main.path(forResource: "Dream_Donation", ofType: "plist"),
        let infoAttr = try? FileManager.default.attributesOfItem(atPath: infoPath),
        let infoDate = infoAttr[.modificationDate] as? Date {
        return infoDate
    }
    return Date()
}
